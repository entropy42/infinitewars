package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface SelectionChangedEventHandler extends EventHandler {

	void onSelectionChanged(SelectionChangedEvent event);
}