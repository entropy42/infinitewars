package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface FinishAsteroidFieldEventHandler extends EventHandler {

	void onFinish(FinishAsteroidFieldEvent event);
}