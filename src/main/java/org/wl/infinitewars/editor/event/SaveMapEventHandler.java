package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface SaveMapEventHandler extends EventHandler {

	void onSaveMap(SaveMapEvent event);
}