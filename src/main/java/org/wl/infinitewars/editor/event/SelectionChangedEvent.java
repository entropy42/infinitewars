package org.wl.infinitewars.editor.event;

import java.util.Map;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.json.WorldObjectData;

import com.jme3.scene.Node;

public class SelectionChangedEvent extends Event<SelectionChangedEventHandler> {

	public static final EventType<SelectionChangedEventHandler> TYPE = new EventType<>();

	private final Map<Node, WorldObjectData> selectedObjects;
	private final boolean fromHud;

	public SelectionChangedEvent(Map<Node, WorldObjectData> selectedObjects, boolean fromHud) {
		this.selectedObjects = selectedObjects;
		this.fromHud = fromHud;
	}

	@Override
	public EventType<SelectionChangedEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(SelectionChangedEventHandler handler) {
		handler.onSelectionChanged(this);
	}

	public Map<Node, WorldObjectData> getSelectedObjects() {
		return selectedObjects;
	}

	public boolean isFromHud() {
		return fromHud;
	}
}