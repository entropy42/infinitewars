package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class GenerateAsteroidsEvent extends Event<GenerateAsteroidsEventHandler> {

	public static final EventType<GenerateAsteroidsEventHandler> TYPE = new EventType<>();

	private final int count;
	private final int minSize;
	private final int maxSize;

	public GenerateAsteroidsEvent(int count, int minSize, int maxSize) {
		this.count = count;
		this.minSize = minSize;
		this.maxSize = maxSize;
	}

	@Override
	public EventType<GenerateAsteroidsEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(GenerateAsteroidsEventHandler handler) {
		handler.onGenerate(this);
	}

	public int getCount() {
		return count;
	}

	public int getMinSize() {
		return minSize;
	}

	public int getMaxSize() {
		return maxSize;
	}
}