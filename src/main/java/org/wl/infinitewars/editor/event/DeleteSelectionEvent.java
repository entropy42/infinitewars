package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class DeleteSelectionEvent extends Event<DeleteSelectionEventHandler> {

	public static final EventType<DeleteSelectionEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<DeleteSelectionEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(DeleteSelectionEventHandler handler) {
		handler.onDeleteSelection(this);
	}
}