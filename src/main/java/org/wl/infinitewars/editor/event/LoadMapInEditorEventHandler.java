package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface LoadMapInEditorEventHandler extends EventHandler {

	void onLoad(LoadMapInEditorEvent event);
}