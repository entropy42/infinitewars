package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface DeleteSelectionEventHandler extends EventHandler {

	void onDeleteSelection(DeleteSelectionEvent event);
}