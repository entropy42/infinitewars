package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface GenerateAsteroidsEventHandler extends EventHandler {

	void onGenerate(GenerateAsteroidsEvent event);
}