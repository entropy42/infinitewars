package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class TransformSelectionEvent extends Event<TransformSelectionEventHandler> {

	public static final EventType<TransformSelectionEventHandler> TYPE = new EventType<>();

	private final boolean move;
	private final boolean rotate;
	private final boolean scale;

	public TransformSelectionEvent(boolean move, boolean rotate, boolean scale) {
		this.move = move;
		this.rotate = rotate;
		this.scale = scale;
	}

	@Override
	public EventType<TransformSelectionEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(TransformSelectionEventHandler handler) {
		handler.onMoveSelection(this);
	}

	public boolean isMove() {
		return move;
	}

	public boolean isRotate() {
		return rotate;
	}

	public boolean isScale() {
		return scale;
	}
}