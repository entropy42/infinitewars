package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class FinishAsteroidFieldEvent extends Event<FinishAsteroidFieldEventHandler> {

	public static final EventType<FinishAsteroidFieldEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<FinishAsteroidFieldEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(FinishAsteroidFieldEventHandler handler) {
		handler.onFinish(this);
	}
}