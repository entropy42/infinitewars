package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class AddAsteroidFieldEvent extends Event<AddAsteroidFieldEventHandler> {

	public static final EventType<AddAsteroidFieldEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<AddAsteroidFieldEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(AddAsteroidFieldEventHandler handler) {
		handler.onAddAsteroidField(this);
	}
}