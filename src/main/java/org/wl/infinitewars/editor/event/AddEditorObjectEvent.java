package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.json.WorldObjectData;

public class AddEditorObjectEvent extends Event<AddEditorObjectEventHandler> {

	public static final EventType<AddEditorObjectEventHandler> TYPE = new EventType<>();

	private final WorldObjectData data;

	public AddEditorObjectEvent(WorldObjectData data) {
		this.data = data;
	}

	@Override
	public EventType<AddEditorObjectEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(AddEditorObjectEventHandler handler) {
		handler.onAddEditorObject(this);
	}

	public WorldObjectData getData() {
		return data;
	}
}