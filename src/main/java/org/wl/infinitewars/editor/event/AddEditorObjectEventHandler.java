package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface AddEditorObjectEventHandler extends EventHandler {

	void onAddEditorObject(AddEditorObjectEvent event);
}