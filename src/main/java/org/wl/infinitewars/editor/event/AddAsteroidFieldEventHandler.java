package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface AddAsteroidFieldEventHandler extends EventHandler {

	void onAddAsteroidField(AddAsteroidFieldEvent event);
}