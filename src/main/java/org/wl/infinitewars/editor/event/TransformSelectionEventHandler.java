package org.wl.infinitewars.editor.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface TransformSelectionEventHandler extends EventHandler {

	void onMoveSelection(TransformSelectionEvent event);
}