package org.wl.infinitewars.editor.entity;

import org.wl.infinitewars.shared.entity.EntityAttacher;
import org.wl.infinitewars.shared.gameplay.IngameState;
import org.wl.infinitewars.shared.gameplay.PhysicsWrapper;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

import com.google.inject.Inject;
import com.jme3.scene.Spatial;

public class EditorEntityAttacher extends EntityAttacher {

	@Inject
	public EditorEntityAttacher(IngameState ingameState, AbstractSyncState syncState, PhysicsWrapper physicsWrapper) {
		super(ingameState, syncState, physicsWrapper);
	}

	@Override
	public void attachEntity(Spatial entity) {
		entity.removeFromParent();
		super.attachEntity(entity);
	}
}