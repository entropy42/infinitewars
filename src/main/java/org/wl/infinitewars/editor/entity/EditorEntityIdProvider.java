package org.wl.infinitewars.editor.entity;

import org.wl.infinitewars.shared.entity.EntityIdProvider;

public class EditorEntityIdProvider implements EntityIdProvider {

	@Override
	public long getNextId() {
		return -1L;
	}
}