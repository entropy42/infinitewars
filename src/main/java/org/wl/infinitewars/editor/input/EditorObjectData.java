package org.wl.infinitewars.editor.input;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.math3.util.FastMath;
import org.wl.infinitewars.editor.AsteroidGenerator;
import org.wl.infinitewars.editor.event.GenerateAsteroidsEvent;
import org.wl.infinitewars.editor.event.GenerateAsteroidsEventHandler;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityFactory;
import org.wl.infinitewars.shared.event.EventHandlersContainer;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.json.Position;
import org.wl.infinitewars.shared.json.Rotation;
import org.wl.infinitewars.shared.json.WorldObjectData;
import org.wl.infinitewars.shared.util.FileConstants;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingSphere;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.debug.WireBox;

@Singleton
public class EditorObjectData {

	public static final String ASTEROID_FIELD_NODE = "Asteroid field node";

	private final EventHandlersContainer eventHandlersContainer = new EventHandlersContainer();

	private final Map<Node, WorldObjectData> nodeToWorldObjectMap = new HashMap<>();

	private int asteroidModelCount;

	private Collection<Node> generatedAsteroids = new HashSet<>();
	private Node asteroidFieldNode;
	private WireBox asteroidFieldBox;

	private final AssetManager assetManager;
	private final EntityFactory entityFactory;

	private MapData mapData;
	private Node rootNode;
	private String mapName;

	@Inject
	public EditorObjectData(AssetManager assetManager, EntityFactory entityFactory) {
		this.assetManager = assetManager;
		this.entityFactory = entityFactory;
	}

	public void init(MapData mapData, Node rootNode, String mapName) {
		this.mapData = mapData;
		this.rootNode = rootNode;
		this.mapName = mapName;
		List<WorldObjectData> objects = mapData.getObjects();

		Map<Vector3f, WorldObjectData> positionToObjectMap = new HashMap<>(objects.size());

		for (WorldObjectData worldObjectData : objects) {
			positionToObjectMap.put(worldObjectData.getLocation().toVector(), worldObjectData);
		}

		List<Spatial> children = rootNode.getChildren();

		for (Spatial spatial : children) {
			if (spatial instanceof Node && !spatial.getName().contains("Sky")) {
				Node node = (Node)spatial;
				WorldObjectData worldObject = positionToObjectMap.get(node.getLocalTranslation());

				if (worldObject != null) {
					nodeToWorldObjectMap.put(node, worldObject);
				}
			}
		}

		eventHandlersContainer.addHandler(GenerateAsteroidsEvent.TYPE, new GenerateAsteroidsEventHandler() {
			@Override
			public void onGenerate(GenerateAsteroidsEvent event) {
				generateAsteroids(event);
			}
		});

		File modelsPath = new File("src/main/resources" + FileConstants.MODELS_PATH_COMPLETE);
		File[] modelFolders = modelsPath.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory() && pathname.getName().startsWith("asteroid");
			}
		});

		asteroidModelCount = modelFolders.length;
	}

	public Node addObject(WorldObjectData worldObject) throws Exception {
		Node object = entityFactory.createObject(worldObject);

		rootNode.attachChild(object);
		mapData.getObjects().add(worldObject);

		nodeToWorldObjectMap.put(object, worldObject);
		return object;
	}

	public Node addAsteroidField() {
		if (asteroidFieldNode != null) {
			asteroidFieldNode.removeFromParent();
			clearUnsavedAsteroids();
		}

		asteroidFieldBox = new WireBox(50f, 50f, 50f);

		Geometry asteroidFieldGeom = new Geometry("Asteroid field geom", asteroidFieldBox);
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.getAdditionalRenderState().setWireframe(true);
		mat.setColor("Color", ColorRGBA.Yellow);
		asteroidFieldGeom.setMaterial(mat);

		asteroidFieldNode = new Node(ASTEROID_FIELD_NODE);
		asteroidFieldNode.attachChild(asteroidFieldGeom);
		asteroidFieldNode.setModelBound(new BoundingSphere());
		asteroidFieldNode.updateModelBound();
		rootNode.attachChild(asteroidFieldNode);

		return asteroidFieldNode;
	}

	public void generateAsteroids(GenerateAsteroidsEvent event) {
		try {
			clearUnsavedAsteroids();

			generatedAsteroids = new HashSet<>();

			Vector3f size = asteroidFieldNode.getLocalScale().mult(100f);
			AsteroidGenerator generator = new AsteroidGenerator(asteroidModelCount, asteroidFieldNode.getLocalTranslation(), size, event);
			List<WorldObjectData> asteroids = generator.generate();

			for (WorldObjectData worldObjectData : asteroids) {
				Node object = addObject(worldObjectData);
				generatedAsteroids.add(object);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void finishAsteroidField() {
		asteroidFieldNode.removeFromParent();
		generatedAsteroids = new HashSet<>();
	}

	private void clearUnsavedAsteroids() {
		for (Node asteroid : generatedAsteroids) {
			asteroid.removeFromParent();

			WorldObjectData objectData = nodeToWorldObjectMap.get(asteroid);
			mapData.getObjects().remove(objectData);
		}
	}

	public WorldObjectData getWorldObjectData(Node node) {
		return nodeToWorldObjectMap.get(node);
	}

	public void saveMap() {
		try {
			List<WorldObjectData> asteroids = new ArrayList<>(nodeToWorldObjectMap.size());
			float minX = Float.MAX_VALUE;
			float maxX = Float.MIN_VALUE;
			float minY = Float.MAX_VALUE;
			float maxY = Float.MIN_VALUE;
			float minZ = Float.MAX_VALUE;
			float maxZ = Float.MIN_VALUE;

			for (Entry<Node, WorldObjectData> entry : nodeToWorldObjectMap.entrySet()) {
				Node node = entry.getKey();
				WorldObjectData obj = entry.getValue();

				obj.setLocation(new Position(node.getLocalTranslation()));
				obj.setScale(new Position(node.getLocalScale()));
				obj.setRotation(new Rotation(node.getLocalRotation()));

				if (minX > obj.getLocation().getX()) {
					minX = obj.getLocation().getX();
				}

				if (maxX < obj.getLocation().getX()) {
					maxX = obj.getLocation().getX();
				}

				if (minY > obj.getLocation().getY()) {
					minY = obj.getLocation().getY();
				}

				if (maxY < obj.getLocation().getY()) {
					maxY = obj.getLocation().getY();
				}

				if (minZ > obj.getLocation().getZ()) {
					minZ = obj.getLocation().getZ();
				}

				if (maxZ < obj.getLocation().getZ()) {
					maxZ = obj.getLocation().getZ();
				}

				if (obj.getName().startsWith("asteroid")) {
					asteroids.add(obj);
				}
			}

			if (asteroids.size() >= 10) {
				float rangeX = maxX - minX;
				float rangeY = maxY - minY;
				float rangeZ = maxZ - minZ;

				float blockSizeX = getBlockSize(rangeX);
				float blockSizeY = getBlockSize(rangeY);
				float blockSizeZ = getBlockSize(rangeZ);

				int blockCountX = (int)FastMath.ceil(rangeX / blockSizeX);
				int blockCountY = (int)FastMath.ceil(rangeY / blockSizeY);
				int blockCountZ = (int)FastMath.ceil(rangeZ / blockSizeZ);

				String[][][] blockMatrix = new String[blockCountX][][];

				for (int x = 0; x < blockCountX; x++) {
					blockMatrix[x] = new String[blockCountY][];

					for (int y = 0; y < blockCountY; y++) {
						blockMatrix[x][y] = new String[blockCountZ];

						for (int z = 0; z < blockCountZ; z++) {
							blockMatrix[x][y][z] = UUID.randomUUID().toString();
						}
					}
				}

				for (WorldObjectData a : asteroids) {
					Position pos = a.getLocation();

					int x = getBlockIndex(minX, blockSizeX, pos.getX());
					int y = getBlockIndex(minY, blockSizeY, pos.getY());
					int z = getBlockIndex(minZ, blockSizeZ, pos.getZ());

					String block = blockMatrix[x][y][z];

					Map<String, Object> data = a.getData();

					if (data == null) {
						data = new HashMap<>(1);
						a.setData(data);
					}

					data.put(EntityConstants.batchId, block);
				}
			}

			JsonHelper.get().writeJsonFile(mapData, "src/main/resources" + FileConstants.MAPS_PATH_COMPLETE + "/" + mapName + FileConstants.MAP_DATA_FILE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected float getBlockSize(float range) {
		float standardBlockSize = 500;
		float maxBlockSize = 600;

		if (range >= standardBlockSize && range <= maxBlockSize) {
			return maxBlockSize;
		}

		return standardBlockSize;
	}

	protected int getBlockIndex(float min, float blockSize, float pos) {
		float diff = pos - min;
		int block = (int)FastMath.floor(diff / blockSize);
		return block;
	}

	public void deleteFromMap(Node spatial) {
		WorldObjectData worldObjectEntry = getWorldObjectData(spatial);
		mapData.getObjects().remove(worldObjectEntry);
	}

	public boolean isUnfinishedAsteroid(Spatial c) {
		return generatedAsteroids.contains(c);
	}

	public void cleanup() {
		eventHandlersContainer.cleanup();
	}
}