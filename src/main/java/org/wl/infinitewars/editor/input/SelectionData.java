package org.wl.infinitewars.editor.input;

import com.jme3.bounding.BoundingBox;
import com.jme3.bounding.BoundingSphere;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Line;

public class SelectionData {

	private final Spatial selection;
	private final Node selectionBoxNode;
	private final Line lineLeft;
	private final Line lineTop;
	private final Line lineRight;
	private final Line lineBottom;
	private final Node transformationLinesNode;

	public SelectionData(Spatial selection, Node selectionBoxNode, Node transformationLinesNode, Material mat, Material movementMaterialX, Material movementMaterialY, Material movementMaterialZ) {
		this.selection = selection;
		this.selectionBoxNode = selectionBoxNode;
		this.transformationLinesNode = transformationLinesNode;

		final Vector3f lineStart = new Vector3f(1f, 1f, 1f);
		final Vector3f lineEnd = new Vector3f(2f, 1f, 1f);
		lineLeft = new Line(lineStart, lineEnd);
		lineTop = new Line(lineStart, lineEnd);
		lineRight = new Line(lineStart, lineEnd);
		lineBottom = new Line(lineStart, lineEnd);

		Geometry geomLineLeft = new Geometry("SelectionLineLeft", lineLeft);
		Geometry geomLineTop = new Geometry("SelectionLineLeft", lineTop);
		Geometry geomLineRight = new Geometry("SelectionLineLeft", lineRight);
		Geometry geomLineBottom = new Geometry("SelectionLineLeft", lineBottom);

		geomLineLeft.setMaterial(mat);
		geomLineTop.setMaterial(mat);
		geomLineRight.setMaterial(mat);
		geomLineBottom.setMaterial(mat);

		selectionBoxNode.attachChild(geomLineLeft);
		selectionBoxNode.attachChild(geomLineTop);
		selectionBoxNode.attachChild(geomLineRight);
		selectionBoxNode.attachChild(geomLineBottom);

		float scaleMultiplier = 4f;
		float scaleX = selection.getLocalScale().x * scaleMultiplier;
		float scaleY = selection.getLocalScale().y * scaleMultiplier;
		float scaleZ = selection.getLocalScale().z * scaleMultiplier;

		addTransformationLine(movementMaterialX, Vector3f.UNIT_X, scaleX);
		addTransformationLine(movementMaterialY, Vector3f.UNIT_Y, scaleY);
		addTransformationLine(movementMaterialZ, Vector3f.UNIT_Z, scaleZ);
	}

	private void addTransformationLine(Material mat, Vector3f direction, float scale) {
		Vector3f min = direction.mult(scale).add(Vector3f.UNIT_XYZ.mult(0.5f));
		Vector3f max = min.negate();
		Box box = new Box(min, max);
		Geometry geom = new Geometry("Transformation line " + direction, box);
		geom.setUserData("DIRECTION", direction);
		geom.setMaterial(mat);
		geom.setModelBound(new BoundingBox());
		geom.updateModelBound();
		transformationLinesNode.attachChild(geom);
	}

	public void update(Camera camera) {
		BoundingSphere sphere = (BoundingSphere)selection.getWorldBound();
		Vector3f position = selection.getLocalTranslation();
		float size = sphere.getRadius();

		Vector3f camLeft = camera.getLeft();
		Vector3f camUp = camera.getUp();
		Vector3f left = camLeft.mult(size);
		Vector3f up = camUp.mult(size);
		Vector3f right = left.negate();
		Vector3f down = up.negate();

		Vector3f topLeft = camera.getScreenCoordinates(position.add(left).add(up));
		Vector3f topRight = camera.getScreenCoordinates(position.add(right).add(up));
		Vector3f bottomLeft = camera.getScreenCoordinates(position.add(left).add(down));
		Vector3f bottomRight = camera.getScreenCoordinates(position.add(right).add(down));

		lineLeft.updatePoints(topLeft, bottomLeft);
		lineTop.updatePoints(topLeft, topRight);
		lineRight.updatePoints(topRight, bottomRight);
		lineBottom.updatePoints(bottomLeft, bottomRight);
	}

	public Node getSelectionBoxNode() {
		return selectionBoxNode;
	}

	public Node getTransformationLinesNode() {
		return transformationLinesNode;
	}
}