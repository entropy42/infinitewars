package org.wl.infinitewars.editor.input;

import java.util.ArrayList;
import java.util.Collection;

import org.wl.infinitewars.editor.event.SaveMapEvent;
import org.wl.infinitewars.editor.event.SaveMapEventHandler;
import org.wl.infinitewars.shared.event.EventHandlersContainer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

@Singleton
public class EditorInputHandler extends AbstractControl implements ActionListener, AnalogListener {

	private final EventHandlersContainer eventHandlersContainer = new EventHandlersContainer();

	private final String MOVE_MOUSE_LEFT = "MOVE_MOUSE_LEFT";
	private final String MOVE_MOUSE_RIGHT = "MOVE_MOUSE_RIGHT";
	private final String MOVE_MOUSE_UP = "MOVE_MOUSE_UP";
	private final String MOVE_MOUSE_DOWN = "MOVE_MOUSE_DOWN";
	private final String MOUSE_WHEEL_DOWN = "MOUSE_WHEEL_DOWN";
	private final String MOUSE_WHEEL_UP = "MOUSE_WHEEL_UP";

	private final String MOVE_UP = "MOVE_UP";
	private final String MOVE_DOWN = "MOVE_DOWN";
	private final String STRAFE_LEFT = "STRAFE_LEFT";
	private final String STRAFE_RIGHT = "STRAFE_RIGHT";

	private final String CLICK_LEFT = "CLICK_LEFT";
	private final String CLICK_MIDDLE = "CLICK_MIDDLE";
	private final String CLICK_RIGHT = "CLICK_RIGHT";

	private final String SET_CAMERA_CENTER = "SET_CAMERA_CENTER";
	private final String SELECTION_MODIFIER = "SELECTION_MODIFIER";
	private final String DELETE_SELECTED_OBJECTS = "DELETE_SELECTED_OBJECTS";
	private final String MOVE_OBJECT = "MOVE_OBJECT";
	private final String ROTATE_OBJECT = "ROTATE_OBJECT";
	private final String SCALE_OBJECT = "SCALE_OBJECT";
	private final String SAVE_MAP = "SAVE_MAP";

	private final InputManager inputManager;
	private final Camera camera;
	private final EditorSelectionManager selectionManager;

	private final Collection<String> mappingNames = new ArrayList<>();

	private final float cameraMoveSpeed = 100.0f;
	private boolean rotateCamera;

	private Vector3f cameraCenter = new Vector3f();

	@Inject
	public EditorInputHandler(InputManager inputManager, Camera camera, EditorSelectionManager selectionManager) {
		this.inputManager = inputManager;
		this.camera = camera;
		this.selectionManager = selectionManager;

		init();
	}

	private void init() {
		addMapping(MOVE_MOUSE_LEFT, new MouseAxisTrigger(MouseInput.AXIS_X, true));
		addMapping(MOVE_MOUSE_RIGHT, new MouseAxisTrigger(MouseInput.AXIS_X, false));
		addMapping(MOVE_MOUSE_UP, new MouseAxisTrigger(MouseInput.AXIS_Y, false));
		addMapping(MOVE_MOUSE_DOWN, new MouseAxisTrigger(MouseInput.AXIS_Y, true));
		addMapping(MOUSE_WHEEL_DOWN, new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true));
		addMapping(MOUSE_WHEEL_UP, new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false));

		addMapping(MOVE_UP, new KeyTrigger(KeyInput.KEY_W));
		addMapping(MOVE_DOWN, new KeyTrigger(KeyInput.KEY_S));
		addMapping(STRAFE_LEFT, new KeyTrigger(KeyInput.KEY_A));
		addMapping(STRAFE_RIGHT, new KeyTrigger(KeyInput.KEY_D));

		addMapping(CLICK_LEFT, new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		addMapping(CLICK_RIGHT, new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
		addMapping(CLICK_MIDDLE, new MouseButtonTrigger(MouseInput.BUTTON_MIDDLE));

		addMapping(SET_CAMERA_CENTER, new KeyTrigger(KeyInput.KEY_SPACE));
		addMapping(SELECTION_MODIFIER, new KeyTrigger(KeyInput.KEY_LCONTROL));
		addMapping(DELETE_SELECTED_OBJECTS, new KeyTrigger(KeyInput.KEY_DELETE));
		addMapping(MOVE_OBJECT, new KeyTrigger(KeyInput.KEY_M));
		addMapping(ROTATE_OBJECT, new KeyTrigger(KeyInput.KEY_R));
		addMapping(SCALE_OBJECT, new KeyTrigger(KeyInput.KEY_T));
		addMapping(SAVE_MAP, new KeyTrigger(KeyInput.KEY_F6));

		String[] mappingNamesArray = mappingNames.toArray(new String[mappingNames.size()]);

		inputManager.removeListener(this);
		inputManager.addListener(this, mappingNamesArray);

		eventHandlersContainer.addHandler(SaveMapEvent.TYPE, new SaveMapEventHandler() {
			@Override
			public void onSaveMap(SaveMapEvent event) {
				selectionManager.saveMap();
			}
		});
	}

	private void addMapping(String name, Trigger... triggers) {
		inputManager.addMapping(name, triggers);
		mappingNames.add(name);
	}

	@Override
	protected void controlUpdate(float tpf) {
		selectionManager.drawSelectionBox();
		selectionManager.drawSelectedObjectsBoxes();
	}

	private void rotate(float value, Vector3f axis) {
		if (rotateCamera) {
			final Vector3f oldLocation = camera.getLocation().clone();
			final float distanceToCenter = oldLocation.distance(cameraCenter);
			camera.setLocation(oldLocation.add(axis.mult(value * distanceToCenter * 2f)));

			Vector3f newCamLocation = camera.getLocation().subtract(cameraCenter).normalizeLocal().multLocal(distanceToCenter).add(cameraCenter);
			camera.setLocation(newCamLocation);
			camera.lookAt(cameraCenter, Vector3f.UNIT_Y);
		}
	}

	private void changeCameraCenter() {
		Vector3f newCameraCenter = selectionManager.getNewCameraCenter();

		if (newCameraCenter != null) {
			cameraCenter = newCameraCenter;
			camera.lookAt(cameraCenter, Vector3f.UNIT_Y);
		}
	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf) {
		switch (name) {
			case CLICK_LEFT:
				selectionManager.handleLeftClick(isPressed);
				break;
			case CLICK_RIGHT:
				rotateCamera = isPressed;
				inputManager.setCursorVisible(!rotateCamera);
				break;
			case SET_CAMERA_CENTER:
				if (isPressed) {
					changeCameraCenter();
				}
				break;
			case SELECTION_MODIFIER:
				selectionManager.setKeepSelecting(isPressed);
				break;
			case DELETE_SELECTED_OBJECTS:
				if (isPressed) {
					selectionManager.deleteSelection();
				}
				break;
			case MOVE_OBJECT:
				if (isPressed) {
					selectionManager.toggleMovementMode();
				}
				break;
			case ROTATE_OBJECT:
				if (isPressed) {
					selectionManager.toggleRotationMode();
				}
				break;
			case SCALE_OBJECT:
				if (isPressed) {
					selectionManager.toggleScaleMode();
				}
				break;
			case SAVE_MAP:
				if (isPressed) {
					selectionManager.saveMap();
				}
				break;
		}
	}

	@Override
	public void onAnalog(String name, float value, float tpf) {
		switch (name) {
			case MOVE_UP:
				moveCamera(camera.getUp(), tpf);
				break;
			case MOVE_DOWN:
				moveCamera(camera.getUp(), -tpf);
				break;
			case STRAFE_LEFT:
				moveCamera(camera.getLeft(), tpf);
				break;
			case STRAFE_RIGHT:
				moveCamera(camera.getLeft(), -tpf);
				break;
			case MOUSE_WHEEL_DOWN:
				moveCamera(camera.getDirection(), -tpf * 300f);
				break;
			case MOUSE_WHEEL_UP:
				moveCamera(camera.getDirection(), tpf * 300f);
				break;
			case MOVE_MOUSE_LEFT:
				rotate(value, camera.getLeft());
				selectionManager.transformObject(new Vector2f(-1f, 0f));
				break;
			case MOVE_MOUSE_RIGHT:
				rotate(-value, camera.getLeft());
				selectionManager.transformObject(new Vector2f(1f, 0f));
				break;
			case MOVE_MOUSE_UP:
				rotate(value, camera.getUp());
				selectionManager.transformObject(new Vector2f(0f, 1f));
				break;
			case MOVE_MOUSE_DOWN:
				rotate(-value, camera.getUp());
				selectionManager.transformObject(new Vector2f(0f, -1f));
				break;
		}
	}

	private void moveCamera(Vector3f direction, float tpf) {
		Vector3f newLocation = camera.getLocation().add(direction.normalize().mult(tpf * cameraMoveSpeed));
		camera.setLocation(newLocation);
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

	public void cleanup() {
		eventHandlersContainer.cleanup();
		selectionManager.cleanup();
	}
}