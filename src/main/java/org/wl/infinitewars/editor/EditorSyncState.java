package org.wl.infinitewars.editor;

import org.wl.infinitewars.shared.loading.EnqueueHelper;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

import com.google.inject.Inject;

public class EditorSyncState extends AbstractSyncState {

	@Inject
	public EditorSyncState(EnqueueHelper enqueueHelper) {
		super(enqueueHelper, 0);
	}

	@Override
	protected void onUpdate(float tpf) {

	}
}