package org.wl.infinitewars.editor;

import org.wl.infinitewars.editor.entity.EditorEntityAttacher;
import org.wl.infinitewars.editor.entity.EditorEntityFactory;
import org.wl.infinitewars.editor.entity.EditorEntityIdProvider;
import org.wl.infinitewars.editor.loading.EditorLoadingStepBuilder;
import org.wl.infinitewars.editor.ui.hud.EditorHudState;
import org.wl.infinitewars.editor.ui.hud.EditorHudStateImpl;
import org.wl.infinitewars.editor.ui.mapselection.EditorMapSelectionState;
import org.wl.infinitewars.editor.ui.mapselection.EditorMapSelectionStateImpl;
import org.wl.infinitewars.shared.AbstractGameApplication;
import org.wl.infinitewars.shared.SharedModule;
import org.wl.infinitewars.shared.entity.EntityAttacher;
import org.wl.infinitewars.shared.entity.EntityFactory;
import org.wl.infinitewars.shared.entity.EntityIdProvider;
import org.wl.infinitewars.shared.gameplay.IngameState;
import org.wl.infinitewars.shared.gameplay.IngameStateImpl;
import org.wl.infinitewars.shared.gameplay.PhysicsWrapper;
import org.wl.infinitewars.shared.gameplay.PhysicsWrapperImpl;
import org.wl.infinitewars.shared.loading.LoadingStepsBuilder;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

public class EditorModule extends SharedModule {

	public EditorModule(AbstractGameApplication app) throws Exception {
		super(app);
	}

	@Override
	protected void configure() {
		super.configure();
		bind(EntityFactory.class).to(EditorEntityFactory.class);
		bind(EntityIdProvider.class).to(EditorEntityIdProvider.class);
		bind(IngameState.class).to(IngameStateImpl.class);
		bind(PhysicsWrapper.class).to(PhysicsWrapperImpl.class);
		bind(AbstractSyncState.class).to(EditorSyncState.class);
		bind(EntityAttacher.class).to(EditorEntityAttacher.class);
		bind(LoadingStepsBuilder.class).to(EditorLoadingStepBuilder.class);

		bind(EditorHudState.class).to(EditorHudStateImpl.class);
		bind(EditorMapSelectionState.class).to(EditorMapSelectionStateImpl.class);
	}
}