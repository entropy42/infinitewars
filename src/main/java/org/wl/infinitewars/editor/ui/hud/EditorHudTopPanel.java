package org.wl.infinitewars.editor.ui.hud;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.wl.infinitewars.editor.event.AddEditorObjectEvent;
import org.wl.infinitewars.shared.json.Faction;
import org.wl.infinitewars.shared.json.FactionConfig;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.json.Position;
import org.wl.infinitewars.shared.json.Rotation;
import org.wl.infinitewars.shared.json.WorldObjectData;
import org.wl.infinitewars.shared.ui.EmptyContainer;
import org.wl.infinitewars.shared.util.FileConstants;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.menuing.Menu;
import tonegod.gui.core.ElementManager;

public abstract class EditorHudTopPanel extends EmptyContainer {

	private final MapData mapData;

	private Menu menuAdd;

	public EditorHudTopPanel(ElementManager screen, MapData mapData) {
		super(screen);
		this.mapData = mapData;

		float w = screen.getWidth();
		float h = screen.getHeight() / 17f;
		setDimensions(w, h);

		setColorMap(FileConstants.UI_PATH + "colors/lightgrey.jpg");

		init();
	}

	protected abstract void save();

	protected abstract void addObject(AddEditorObjectEvent event);

	protected abstract void addAsteroidField();

	private void init() {
		float x = 0f;
		Vector2f pos = new Vector2f(x, 0f);
		Vector2f size = new Vector2f(getHeight(), getHeight());

		ButtonAdapter btnSave = new ButtonAdapter(screen, pos, size) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				save();
			}
		};
		btnSave.setText("SAV");
		addChild(btnSave);

		pos.x += size.x;

		menuAdd = new Menu(screen, false) {
			@Override
			public void onMenuItemClicked(int index, Object value, boolean isToggled) {
				if (value instanceof EditorWorldObjectDataFactory) {
					EditorWorldObjectDataFactory factory = (EditorWorldObjectDataFactory)value;
					WorldObjectData data = factory.create();
					addObject(new AddEditorObjectEvent(data));
				} else if ("Asteroid field".equals(value)) {
					addAsteroidField();
				}
			}
		};
		menuAdd.setPosition(pos.x, pos.y + size.y);

		Collection<Faction> factions = new ArrayList<>(2);
		factions.add(mapData.getFaction1());
		factions.add(mapData.getFaction2());

		Set<String> notCreatableModels = new HashSet<>();

		for (Faction faction : factions) {
			try {
				FactionConfig config = JsonHelper.get().toPOJO(FileConstants.FACTIONS_PATH_COMPLETE + faction.name().toLowerCase() + FileConstants.FACTION_CONFIG_FILE, FactionConfig.class);

				List<String> shipNames = config.getShips().stream().map(o -> o.getShipName()).collect(Collectors.toList());
				notCreatableModels.addAll(shipNames);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		File modelsPath = new File("src/main/resources" + FileConstants.MODELS_PATH_COMPLETE);
		File[] modelFolders = modelsPath.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory() && !notCreatableModels.contains(pathname.getName());
			}
		});

		for (File modelFolder : modelFolders) {
			EditorWorldObjectDataFactory factory = new EditorWorldObjectDataFactory() {
				@Override
				public WorldObjectData create() {
					WorldObjectData objectData = new WorldObjectData();
					objectData.setName(modelFolder.getName());
					objectData.setLocation(new Position());
					objectData.setRotation(new Rotation(0f, 0f, 0f, 1f));
					objectData.setScale(new Position(5f, 5f, 5f));
					return objectData;
				}
			};

			menuAdd.addMenuItem(modelFolder.getName(), factory, null);
		}

		menuAdd.addMenuItem("Asteroid field", "Asteroid field", null);

		ButtonAdapter btnAdd = new ButtonAdapter(screen, pos, size) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				toggleAddMenu();
			}
		};
		btnAdd.setText("ADD");
		addChild(btnAdd);
	}

	private void toggleAddMenu() {
		if (menuAdd.getElementParent() == null) {
			addChild(menuAdd);
		} else {
			removeChild(menuAdd);
		}
	}
}