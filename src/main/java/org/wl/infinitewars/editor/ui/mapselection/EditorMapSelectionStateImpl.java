package org.wl.infinitewars.editor.ui.mapselection;

import java.io.File;

import org.wl.infinitewars.editor.event.LoadMapInEditorEvent;
import org.wl.infinitewars.shared.NodeConstants;
import org.wl.infinitewars.shared.ui.GUIStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.scene.Node;

import tonegod.gui.core.Screen;

@Singleton
public class EditorMapSelectionStateImpl extends GUIStateImpl implements EditorMapSelectionState {

	@Inject
	public EditorMapSelectionStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);

		init();
	}

	@Override
	public void init() {
		MapSelectionPanel mapSelectionPanel = new MapSelectionPanel(screen) {
			@Override
			public void onLoadMap(File mapFolder) {
				fireEvent(new LoadMapInEditorEvent(mapFolder));
			}
		};

		mainContent.addChild(mapSelectionPanel);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);

		mainContent.setIsEnabled(enabled);
	}
}