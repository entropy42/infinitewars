package org.wl.infinitewars.editor.ui.hud;

import org.wl.infinitewars.shared.json.WorldObjectData;

public interface EditorWorldObjectDataFactory {

	WorldObjectData create();
}