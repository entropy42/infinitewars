package org.wl.infinitewars.editor.ui.hud;

import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.ui.GUIState;

public interface EditorHudState extends GUIState {

	void setMapData(MapData mapData);
}