package org.wl.infinitewars.editor.ui.mapselection;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

import org.wl.infinitewars.shared.ui.EmptyContainer;
import org.wl.infinitewars.shared.ui.ScreenUtils;
import org.wl.infinitewars.shared.util.FileConstants;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.lists.ComboBox;
import tonegod.gui.core.ElementManager;

public abstract class MapSelectionPanel extends EmptyContainer {

	private ButtonAdapter btnNewGame;

	public MapSelectionPanel(ElementManager screen) {
		super(screen);

		init();
	}

	private void init() {
		File mapsFolder = new File("src/main/resources" + FileConstants.MAPS_PATH_COMPLETE);

		File[] mapFolders = mapsFolder.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		});

		float padding = ScreenUtils.getPadding(screen);
		float x = padding;
		float widgetWidth = ScreenUtils.getWidgetWidth(screen);
		float widgetHeight = ScreenUtils.getWidgetHeight(screen);
		float y = padding + (widgetHeight * 2) + padding;
		Vector2f widgetSize = ScreenUtils.createWidgetSize(screen);

		final ComboBox cmbMaps = new ComboBox(screen, new Vector2f(x, y), widgetSize) {
			@Override
			public void onChange(int selectedIndex, Object value) {
			}
		};

		Arrays.stream(mapFolders).sorted().forEach(mapFolder -> cmbMaps.addListItem(mapFolder.getName(), mapFolder));

		addChild(cmbMaps);

		x += padding + widgetWidth;

		btnNewGame = new ButtonAdapter(screen, new Vector2f(x, y), widgetSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				File mapFolder = (File)cmbMaps.getSelectedListItem().getValue();
				onLoadMap(mapFolder);
			}
		};
		btnNewGame.setText("Load");
		addChild(btnNewGame);
	}

	public abstract void onLoadMap(File mapFolder);

	@Override
	public void setIsEnabled(boolean isEnabled) {
		super.setIsEnabled(isEnabled);
		btnNewGame.setIsEnabled(isEnabled);
	}
}