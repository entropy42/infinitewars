package org.wl.infinitewars.editor.ui.hud;

import org.wl.infinitewars.editor.event.AddAsteroidFieldEvent;
import org.wl.infinitewars.editor.event.AddEditorObjectEvent;
import org.wl.infinitewars.editor.event.DeleteSelectionEvent;
import org.wl.infinitewars.editor.event.FinishAsteroidFieldEvent;
import org.wl.infinitewars.editor.event.GenerateAsteroidsEvent;
import org.wl.infinitewars.editor.event.SaveMapEvent;
import org.wl.infinitewars.editor.event.SelectionChangedEvent;
import org.wl.infinitewars.editor.event.SelectionChangedEventHandler;
import org.wl.infinitewars.editor.event.TransformSelectionEvent;
import org.wl.infinitewars.shared.NodeConstants;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.ui.GUIStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.scene.Node;

import tonegod.gui.core.Screen;

@Singleton
public class EditorHudStateImpl extends GUIStateImpl implements EditorHudState {

	private EditorHudTopPanel panTop;
	private EditorSelectionPanel panSelection;

	@Inject
	public EditorHudStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);

		initEventHandlers();
	}

	private void initEventHandlers() {
		addEventHandler(SelectionChangedEvent.TYPE, new SelectionChangedEventHandler() {
			@Override
			public void onSelectionChanged(SelectionChangedEvent event) {
				panSelection.selectionChanged(event);
			}
		});
	}

	@Override
	public void init() {

	}

	@Override
	public void setMapData(MapData mapData) {
		panTop = new EditorHudTopPanel(screen, mapData) {
			@Override
			protected void save() {
				fireEvent(new SaveMapEvent());
			}

			@Override
			protected void addObject(AddEditorObjectEvent event) {
				fireEvent(event);
			}

			@Override
			protected void addAsteroidField() {
				fireEvent(new AddAsteroidFieldEvent());
			}
		};
		mainContent.addChild(panTop);

		panSelection = new EditorSelectionPanel(screen, mapData) {
			@Override
			protected void deleteSelection() {
				fireEvent(new DeleteSelectionEvent());
			}

			@Override
			protected void moveSelection() {
				fireEvent(new TransformSelectionEvent(true, false, false));
			}

			@Override
			protected void rotateSelection() {
				fireEvent(new TransformSelectionEvent(false, true, false));
			}

			@Override
			protected void scaleSelection() {
				fireEvent(new TransformSelectionEvent(false, false, true));
			}

			@Override
			protected void changeSelection(SelectionChangedEvent event) {
				fireEvent(event);
			}

			@Override
			protected void generateAsteroids(GenerateAsteroidsEvent event) {
				fireEvent(event);
			}

			@Override
			protected void finishAsteroids(FinishAsteroidFieldEvent event) {
				fireEvent(event);
			}
		};
		mainContent.addChild(panSelection);
	}
}