package org.wl.infinitewars.editor.ui.hud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.wl.infinitewars.editor.event.FinishAsteroidFieldEvent;
import org.wl.infinitewars.editor.event.GenerateAsteroidsEvent;
import org.wl.infinitewars.editor.event.SelectionChangedEvent;
import org.wl.infinitewars.editor.input.EditorObjectData;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.json.GameMode;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.json.WorldObjectData;
import org.wl.infinitewars.shared.ui.EmptyContainer;
import org.wl.infinitewars.shared.ui.SimpleTextField;
import org.wl.infinitewars.shared.util.FileConstants;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.buttons.CheckBox;
import tonegod.gui.controls.lists.ComboBox;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.text.TextField;
import tonegod.gui.core.Element;
import tonegod.gui.core.ElementManager;

public abstract class EditorSelectionPanel extends EmptyContainer {

	private EmptyContainer panButtons;
	private ButtonAdapter btnDelete;
	private ButtonAdapter btnMove;
	private ButtonAdapter btnRotate;
	private ButtonAdapter btnScale;

	private EmptyContainer panMain;

	private final MapData mapData;

	private TextField txtCount;
	private TextField txtMinSize;
	private TextField txtMaxSize;
	private Node node;

	public EditorSelectionPanel(ElementManager screen, MapData mapData) {
		super(screen);
		this.mapData = mapData;

		float width = screen.getWidth() / 3f;
		float height = screen.getHeight() / 3f;
		float x = screen.getWidth() - width;
		float y = screen.getHeight() - height;

		setPosition(x, y);
		setDimensions(width, height);

		setColorMap(FileConstants.UI_PATH + "colors/lightgrey.jpg");

		initButtonPanel();
		initMainPanel();
	}

	protected abstract void changeSelection(SelectionChangedEvent event);

	protected abstract void generateAsteroids(GenerateAsteroidsEvent event);

	protected abstract void finishAsteroids(FinishAsteroidFieldEvent event);

	protected abstract void deleteSelection();

	protected abstract void moveSelection();

	protected abstract void rotateSelection();

	protected abstract void scaleSelection();

	private void initButtonPanel() {
		float width = getWidth();
		float height = getHeight() / 6f;
		float x = 0f;
		float y = 0f;

		panButtons = new EmptyContainer(screen);
		panButtons.setPosition(x, y);
		panButtons.setDimensions(width, height);
		addChild(panButtons);

		Vector2f buttonPos = new Vector2f(0f, 0f);
		final Vector2f buttonSize = new Vector2f(panButtons.getWidth() / 5f, panButtons.getHeight());

		btnDelete = new ButtonAdapter(screen, buttonPos, buttonSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				deleteSelection();
			}
		};
		btnDelete.setIsEnabled(false);
		btnDelete.setText("DEL");
		panButtons.addChild(btnDelete);

		buttonPos = new Vector2f(btnDelete.getWidth(), 0f);

		btnMove = new ButtonAdapter(screen, buttonPos, buttonSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				moveSelection();

				showChangeTranslationPanel(node.getLocalTranslation());
			}
		};
		btnMove.setIsEnabled(false);
		btnMove.setText("MV");
		panButtons.addChild(btnMove);

		buttonPos.x += btnMove.getWidth();

		btnRotate = new ButtonAdapter(screen, buttonPos, buttonSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				rotateSelection();
			}
		};
		btnRotate.setIsEnabled(false);
		btnRotate.setText("ROT");
		panButtons.addChild(btnRotate);

		buttonPos.x += btnRotate.getWidth();

		btnScale = new ButtonAdapter(screen, buttonPos, buttonSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				scaleSelection();

				showChangeScalePanel(node.getLocalScale());
			}
		};
		btnScale.setIsEnabled(false);
		btnScale.setText("SCL");
		panButtons.addChild(btnScale);
	}

	private void initMainPanel() {
		float width = getWidth();
		float height = getHeight() - panButtons.getHeight();
		float x = 0f;
		float y = panButtons.getHeight();

		panMain = new EmptyContainer(screen);
		panMain.setPosition(x, y);
		panMain.setDimensions(width, height);
		addChild(panMain);
	}

	public void selectionChanged(SelectionChangedEvent event) {
		final Map<Node, WorldObjectData> selectedObjects = event.getSelectedObjects();

		panMain.removeAllChildren();

		if (selectedObjects != null) {
			final Vector2f dimensions = new Vector2f(panMain.getWidth() / 2f, panMain.getHeight() / 6f);

			float y = 0f;
			float x = 0f;

			for (final Node s : selectedObjects.keySet()) {
				final Vector2f position = new Vector2f(x, y);

				ButtonAdapter button = new ButtonAdapter(screen, position, dimensions) {
					@Override
					public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
						Map<Node, WorldObjectData> objects = new HashMap<>(1);
						objects.put(s, selectedObjects.get(s));
						changeSelection(new SelectionChangedEvent(objects, true));
					}
				};
				button.setText(s.getName());
				panMain.addChild(button);

				if (x == 0f) {
					x += dimensions.x;
				} else {
					x = 0f;
					y += dimensions.y;
				}
			}

			if (selectedObjects.size() == 1) {
				Entry<Node, WorldObjectData> entry = selectedObjects.entrySet().iterator().next();
				node = entry.getKey();
				WorldObjectData worldObjectData = entry.getValue();

				EmptyContainer panDetails = new EmptyContainer(screen);
				panMain.addChild(panDetails);

				x = 0;
				y += dimensions.y;
				Vector2f pos = new Vector2f(x, y);

				if (worldObjectData != null) {
					Label lblLeft = new Label(screen, pos, dimensions);
					lblLeft.setText("Gamemodes:");
					panDetails.addChild(lblLeft);

					pos.x += dimensions.x;

					Vector2f checkBoxSize = new Vector2f(getWidth() / 8, dimensions.y);

					Label lblTDM = new Label(screen, pos, checkBoxSize);
					lblTDM.setText("TDM:");
					panDetails.addChild(lblTDM);

					pos.x += checkBoxSize.x;

					CheckBox checkTDM = createGameModeCheckBox(pos, checkBoxSize, worldObjectData, GameMode.TEAM_DEATH_MATCH);
					panDetails.addChild(checkTDM);

					pos.x = dimensions.x;
					pos.y += dimensions.y;

					Label lblCQ = new Label(screen, pos, checkBoxSize);
					lblCQ.setText("CQ:");
					panDetails.addChild(lblCQ);

					pos.x += checkBoxSize.x;

					CheckBox chkCQ = createGameModeCheckBox(pos, checkBoxSize, worldObjectData, GameMode.CONQUEST);
					panDetails.addChild(chkCQ);

					pos.x += checkBoxSize.x;

					Label lblMS = new Label(screen, pos, checkBoxSize);
					lblMS.setText("MS:");
					panDetails.addChild(lblMS);

					pos.x += checkBoxSize.x;

					CheckBox chkMS = createGameModeCheckBox(pos, checkBoxSize, worldObjectData, GameMode.MOTHER_SHIP);
					panDetails.addChild(chkMS);

					pos.x = 0f;
					pos.y += dimensions.y;

					if (node.getUserData(EntityConstants.controlPointCapturePoints) != null) {
						Element lblFaction = new Label(screen, pos, dimensions);
						lblFaction.setText("Faction:");
						panDetails.addChild(lblFaction);

						pos.x += dimensions.x;

						ComboBox cmbFaction = new ComboBox(screen, pos, dimensions) {

							@Override
							public void onChange(int selectedIndex, Object value) {
								if (worldObjectData.getData() == null) {
									worldObjectData.setData(new HashMap<>(1));
								}

								if (value.equals("")) {
									value = null;
								}

								worldObjectData.getData().put(EntityConstants.faction, value);
								node.setUserData(EntityConstants.faction, value);
							}
						};

						Object initValue = null;

						if (worldObjectData.getData() != null) {
							initValue = worldObjectData.getData().get(EntityConstants.faction);
						}

						cmbFaction.addListItem("-", "");
						cmbFaction.addListItem(mapData.getFaction1().name(), mapData.getFaction1().name());
						cmbFaction.addListItem(mapData.getFaction2().name(), mapData.getFaction2().name());
						panDetails.addChild(cmbFaction);

						if (initValue != null) {
							cmbFaction.setSelectedByValue(initValue, true);
						}
					}
				} else if (node.getName().equals(EditorObjectData.ASTEROID_FIELD_NODE)) {
					ButtonAdapter btnGenerate = new ButtonAdapter(screen, pos, dimensions) {
						@Override
						public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
							try {
								int count = Integer.valueOf(txtCount.getText().trim());
								int minSize = Integer.valueOf(txtMinSize.getText().trim());
								int maxSize = Integer.valueOf(txtMaxSize.getText().trim());

								generateAsteroids(new GenerateAsteroidsEvent(count, minSize, maxSize));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					};
					btnGenerate.setText("Generate");
					panDetails.addChild(btnGenerate);

					pos.x += dimensions.x;

					ButtonAdapter btnFinish = new ButtonAdapter(screen, pos, dimensions) {
						@Override
						public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
							finishAsteroids(new FinishAsteroidFieldEvent());
						}
					};
					btnFinish.setText("Finish");
					panDetails.addChild(btnFinish);

					pos.x = 0f;
					pos.y += dimensions.y;

					Label lblLeft = new Label(screen, pos, dimensions);
					lblLeft.setText("Count:");
					panDetails.addChild(lblLeft);

					pos.x += dimensions.x;

					txtCount = new SimpleTextField(screen, pos, dimensions);
					txtCount.setText("60");
					panDetails.addChild(txtCount);

					pos.x = 0f;
					pos.y += dimensions.y;

					lblLeft = new Label(screen, pos, dimensions);
					lblLeft.setText("Min size:");
					panDetails.addChild(lblLeft);

					pos.x += dimensions.x;

					txtMinSize = new SimpleTextField(screen, pos, dimensions);
					txtMinSize.setText("5");
					panDetails.addChild(txtMinSize);

					pos.x = 0f;
					pos.y += dimensions.y;

					lblLeft = new Label(screen, pos, dimensions);
					lblLeft.setText("Max size:");
					panDetails.addChild(lblLeft);

					pos.x += dimensions.x;

					txtMaxSize = new SimpleTextField(screen, pos, dimensions);
					txtMaxSize.setText("12");
					panDetails.addChild(txtMaxSize);
				}
			}
		}

		btnDelete.setIsEnabled(selectedObjects != null);
		btnMove.setIsEnabled(selectedObjects != null);
		btnRotate.setIsEnabled(selectedObjects != null);
		btnScale.setIsEnabled(selectedObjects != null);
	}

	private void showChangeTranslationPanel(Vector3f initVector) {
		panMain.removeAllChildren();

		EditorVectorPanel pan = new EditorVectorPanel(screen, panMain.getDimensions(), initVector, "") {
			@Override
			protected void onApply(Vector3f newVector) {
				node.setLocalTranslation(newVector);
				moveSelection();
			}
		};
		panMain.addChild(pan);
	}

	private void showChangeScalePanel(Vector3f initVector) {
		panMain.removeAllChildren();

		EditorVectorPanel pan = new EditorVectorPanel(screen, panMain.getDimensions(), initVector, "") {
			@Override
			protected void onApply(Vector3f newVector) {
				node.setLocalScale(newVector);
				scaleSelection();
			}
		};
		panMain.addChild(pan);
	}

	private CheckBox createGameModeCheckBox(Vector2f pos, Vector2f size, final WorldObjectData data, final GameMode gameMode) {
		CheckBox checkBox = new CheckBox(screen, pos, size) {
			@Override
			public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
				if (data.getGameModes() == null) {
					GameMode[] values = GameMode.values();
					data.setGameModes(new ArrayList<>(values.length));

					for (GameMode gm : values) {
						if (gm != gameMode) {
							data.getGameModes().add(gm);
						}
					}
				}

				if (getIsChecked()) {
					if (!data.getGameModes().contains(gameMode)) {
						data.getGameModes().add(gameMode);
					}
				} else {
					data.getGameModes().remove(gameMode);
				}
			}
		};
		checkBox.setIsChecked(data.getGameModes() == null || data.getGameModes().contains(gameMode));
		return checkBox;
	}
}