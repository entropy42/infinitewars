package org.wl.infinitewars.editor.loading;

import java.util.ArrayList;
import java.util.List;

import org.wl.infinitewars.shared.loading.LoadingStep;
import org.wl.infinitewars.shared.loading.LoadingStepBuildRequest;
import org.wl.infinitewars.shared.loading.LoadingStepsBuilder;

public class EditorLoadingStepBuilder extends LoadingStepsBuilder {

	@Override
	public List<LoadingStep> createLoadingSteps(LoadingStepBuildRequest request) {
		List<LoadingStep> steps = new ArrayList<>();
		addMapObjectsSteps(steps, request);

		steps.add(new LoadingStep(40f, true) {
			@Override
			public void load() throws Exception {
				initSkyBox(request);
			}
		});

		steps.add(new LoadingStep(10f, true) {
			@Override
			public void load() throws Exception {
				initCamera(request);
			}
		});

		steps.add(new LoadingStep(10f, true) {
			@Override
			public void load() throws Exception {
				initLights(request);
			}
		});

		return steps;
	}

	@Override
	protected void addSteps(List<LoadingStep> steps, LoadingStepBuildRequest request) {

	}
}