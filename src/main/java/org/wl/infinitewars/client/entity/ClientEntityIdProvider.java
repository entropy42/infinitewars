package org.wl.infinitewars.client.entity;

import org.wl.infinitewars.shared.entity.EntityIdProvider;

public class ClientEntityIdProvider implements EntityIdProvider {
	
	@Override
	public long getNextId() {
		return -1;
	}
}