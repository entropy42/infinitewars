package org.wl.infinitewars.client.entity;

import java.util.List;

import org.wl.infinitewars.client.gameplay.ClientControlPointControl;
import org.wl.infinitewars.shared.control.RotationConstants;
import org.wl.infinitewars.shared.control.SpeedMeasureControl;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityFactoryImpl;
import org.wl.infinitewars.shared.gameplay.GameInfoProvider;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireCreationData;
import org.wl.infinitewars.shared.json.EntityProperties;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.json.WorldObjectData;
import org.wl.infinitewars.shared.util.FileConstants;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.audio.AudioData.DataType;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.system.JmeContext;

@Singleton
public class ClientEntityFactory extends EntityFactoryImpl {

	@Inject
	private GameInfoProvider gameInfoProvider;

	@Override
	public Node createObject(WorldObjectData object) throws Exception {
		Node node = super.createObject(object);

		String controlPointType = node.getUserData(EntityConstants.controlPointType);

		if (controlPointType != null) {
			node.addControl(new ClientControlPointControl(teamsContainer));
		}

		String name = object.getName();
		String propFilePath = FileConstants.MODELS_PATH_COMPLETE + name + FileConstants.MODEL_PROPERTY_FILE;
		EntityProperties properties = JsonHelper.get().toPOJO(propFilePath, EntityProperties.class);

		if (properties != null) {
			Long id = gameInfoProvider.getGameInfo().getObjectIds().get(node.getLocalTranslation());
			node.setUserData(EntityConstants.id, id);
		}

		return node;
	}

	@Override
	public Node createShip(String fileName) throws Exception {
		Node ship = super.createShip(fileName);

		SpeedMeasureControl control = new SpeedMeasureControl(RotationConstants.FORWARD);
		control.setEnabled(false);
		ship.addControl(control);

		List<Vector3f> nozzleSlots = ship.getUserData(EntityConstants.nozzleSlots);

		if (nozzleSlots != null) {
			ColorRGBA startColor = ColorRGBA.Yellow;
			ColorRGBA endColor = new ColorRGBA(0f, 0f, 0f, 0f);
			Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
			material.setTexture("Texture", assetManager.loadTexture(FileConstants.TEXTURES_PATH + "standardWeaponFire.png"));

			for (Vector3f nozzleLocation : nozzleSlots) {
				ParticleEmitter particleEmitter = new ParticleEmitter("Nozzle particle emitter", Type.Triangle, 200);
				ship.attachChild(particleEmitter);
				particleEmitter.setLocalTranslation(nozzleLocation);
				particleEmitter.setImagesX(4);
				particleEmitter.setImagesY(4);
				particleEmitter.getParticleInfluencer().setInitialVelocity(Vector3f.ZERO);
				particleEmitter.getParticleInfluencer().setVelocityVariation(0.0f);
				particleEmitter.setGravity(0, 0, 0);
				particleEmitter.setParticlesPerSec(200f);

				float size = 0.1f;

				particleEmitter.setMaterial(material);
				particleEmitter.setStartColor(startColor);
				particleEmitter.setEndColor(endColor);
				particleEmitter.setStartSize(size * 2f);
				particleEmitter.setEndSize(size);
				particleEmitter.setLowLife(0.05f);
				particleEmitter.setHighLife(0.1f);
			}
		}

		if (jmeContextType == JmeContext.Type.Display) {
			AudioNode audioNode = new AudioNode(assetManager, "assets/sounds/engine1.wav", DataType.Buffer);
			audioNode.setName(EntityConstants.shipEngineAudioNode);
			audioNode.setLooping(true);
			audioNode.setPositional(true);
			ship.attachChild(audioNode);
			audioNode.play();
		}

		return ship;
	}

	@Override
	public Node createWeaponFire(WeaponFireCreationData data) throws Exception {
		Node weaponFire = super.createWeaponFire(data);
		return weaponFire;
	}
}