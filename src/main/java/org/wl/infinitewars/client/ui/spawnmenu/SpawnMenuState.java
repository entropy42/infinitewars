package org.wl.infinitewars.client.ui.spawnmenu;

import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.ui.GUIState;

public interface SpawnMenuState extends GUIState {

	void init(MapData mapData, Player localPlayer);
}