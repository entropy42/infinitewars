package org.wl.infinitewars.client.ui.menu;

import org.wl.infinitewars.client.event.main.CloseGameEvent;
import org.wl.infinitewars.client.event.main.ConnectToServerEvent;
import org.wl.infinitewars.client.event.main.CreateNewServerEvent;
import org.wl.infinitewars.client.event.main.QuitEvent;
import org.wl.infinitewars.shared.NodeConstants;
import org.wl.infinitewars.shared.ui.GUIStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.scene.Node;

import tonegod.gui.core.Screen;

@Singleton
public class MenuStateImpl extends GUIStateImpl implements MenuState {

	private TopMenuPanel panTop;

	@Inject
	public MenuStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);

		init();
	}

	@Override
	public void init() {
		panTop = new TopMenuPanel(screen) {
			@Override
			protected void newGame() {
				fireEvent(new CreateNewServerEvent());
			}

			@Override
			protected void showOptions() {

			}

			@Override
			protected void quit() {
				fireEvent(new QuitEvent());
			}

			@Override
			protected void closeGame() {
				panTop.showCloseGameButton(false);
				fireEvent(new CloseGameEvent());
			}

			@Override
			protected void continueGame() {

			}

			@Override
			protected void connectToServer() {
				panTop.showCloseGameButton(true);
				// fireEvent(new ConnectToServerEvent("192.168.178.20", 6543));
				fireEvent(new ConnectToServerEvent("localhost", 6543));
			}
		};
		mainContent.addChild(panTop);
	}
}