package org.wl.infinitewars.client.ui.hud;

import org.wl.infinitewars.shared.control.DelayedControl;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.ui.Picture;

public class HUDTargetLeadIndicatorHitControl extends DelayedControl {

	private final float size;

	public HUDTargetLeadIndicatorHitControl(float delayTime, float size) {
		super(delayTime);
		this.size = size;
	}

	@Override
	protected void onUpdate(float elapsedTime) {
		Picture pic = (Picture)spatial;
		pic.setWidth(size);
		pic.setHeight(size);
		spatial.removeControl(this);
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}