package org.wl.infinitewars.client.ui.console;

public abstract class ConsoleCommand {
	
	private final String key;
	private final int arguments;
	
	public ConsoleCommand(String key, int arguments) {
		this.key = key;
		this.arguments = arguments;
	}
	
	protected abstract void execute(String key, String... args) throws Exception;
	
	public void executeCommand(String... args) throws Exception {
		execute(key, args);
	}
	
	public String getKey() {
		return key;
	}
	
	public int getArguments() {
		return arguments;
	}
}