package org.wl.infinitewars.client.ui.spawnmenu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.json.EntityProperties;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.json.Position;
import org.wl.infinitewars.shared.json.WorldObjectData;
import org.wl.infinitewars.shared.ui.EmptyContainer;
import org.wl.infinitewars.shared.ui.ImageButton;
import org.wl.infinitewars.shared.ui.ScreenUtils;
import org.wl.infinitewars.shared.util.FileConstants;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

import tonegod.gui.core.ElementManager;

public abstract class SpawnMap extends EmptyContainer {

	private final MapData mapData;
	private final Map<Vector3f, ImageButton> spawnPoints = new HashMap<>();

	private Vector3f activeSpawnLocation;

	private float xRange;
	private float zRange;
	private float mapWidth;
	private float mapHeight;
	private float minX;
	private float minZ;

	public SpawnMap(ElementManager screen, Vector2f size, MapData mapData) {
		super(screen);
		this.mapData = mapData;

		setDimensions(size);

		init();
	}

	protected abstract void onClickSpawnPoint(Vector3f location);

	private void init() {
		setColorMap(FileConstants.UI_PATH + "colors/lightgrey.jpg");

		List<WorldObjectData> objects = mapData.getObjects();

		minX = Float.MAX_VALUE;
		float maxX = Float.MIN_VALUE;
		minZ = Float.MAX_VALUE;
		float maxZ = Float.MIN_VALUE;
		float minScale = Float.MAX_VALUE;
		float maxScale = Float.MIN_VALUE;

		for (WorldObjectData worldObjectData : objects) {
			Position location = worldObjectData.getLocation();
			float scale = worldObjectData.getScale().toVector().length();

			if (minX > location.getX()) {
				minX = location.getX();
			}

			if (maxX < location.getX()) {
				maxX = location.getX();
			}

			if (minZ > location.getZ()) {
				minZ = location.getZ();
			}

			if (maxZ < location.getZ()) {
				maxZ = location.getZ();
			}

			if (minScale > scale) {
				minScale = scale;
			}

			if (maxScale < scale) {
				maxScale = scale;
			}
		}

		xRange = maxX - minX;
		zRange = maxZ - minZ;
		float scaleRange = maxScale - minScale;

		float minIconSize = screen.getWidth() / 90f;
		float maxIconSize = screen.getWidth() / 30f;
		float iconSizeRange = maxIconSize - minIconSize;

		float padding = ScreenUtils.getPadding(screen);
		mapWidth = getWidth() - (padding * 2f);
		mapHeight = getHeight() - (padding * 2f);

		for (WorldObjectData worldObjectData : objects) {
			String name = worldObjectData.getName();
			Position location = worldObjectData.getLocation();

			String propFilePath = FileConstants.MODELS_PATH_COMPLETE + name + FileConstants.MODEL_PROPERTY_FILE;

			EntityProperties properties = null;

			try {
				properties = JsonHelper.get().toPOJO(propFilePath, EntityProperties.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Boolean isSpawnPoint = properties != null ? properties.getValue(EntityConstants.isSpawnPoint) : false;
			isSpawnPoint = BooleanUtils.isTrue(isSpawnPoint);

			float scale = worldObjectData.getScale().toVector().length();
			float normalizedScale = scale - minScale;
			float scalePercentage = normalizedScale / scaleRange;
			float additionalIconSize = scalePercentage * iconSizeRange;
			float iconSize = minIconSize + additionalIconSize;

			float mapX = getMapCoords(minX, xRange, mapWidth, location.getX());
			float mapY = getMapCoords(minZ, zRange, mapHeight, location.getZ());

			ImageButton icon = new ImageButton(screen, FileConstants.MODELS_PATH + name + "/icon.jpg");
			icon.setDimensions(iconSize, iconSize);
			icon.setPosition(mapX, mapY);
			addChild(icon);

			if (isSpawnPoint) {
				Vector3f vector = location.toVector();
				addSpawnIcon(vector, iconSize, mapX, mapY, FileConstants.UI_PATH + "spawnmap/spawn_point.png");
				updateSpawnPoint(vector, false);
			}
		}
	}

	private float getMapCoords(float min, float range, float mapSize, float value) {
		float normalizedX = value - min;
		float mapXPercentage = normalizedX / range;
		return mapXPercentage * mapSize;
	}

	private void addSpawnIcon(final Vector3f location, float iconSize, float mapX, float mapY, String image) {
		ImageButton spawnicon = new ImageButton(screen, image) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				onClickSpawnPoint(location);
			}
		};
		spawnicon.setDimensions(iconSize, iconSize);
		spawnicon.setPosition(mapX, mapY);
		spawnicon.setIsVisible(false);

		ImageButton old = spawnPoints.put(location, spawnicon);

		if (old != null) {
			removeChild(old);
		}

		addChild(spawnicon);
	}

	public void updateSpawnPoint(Vector3f location, boolean show) {
		ImageButton icon = spawnPoints.get(location);
		icon.setIsVisible(show);
	}

	public void setActiveSpawnLocation(Vector3f location) {
		if (activeSpawnLocation != null) {
			String image = FileConstants.UI_PATH + "spawnmap/spawn_point.png";
			replaceSpawnButton(activeSpawnLocation, image);
		}

		activeSpawnLocation = location;
		String image = FileConstants.UI_PATH + "spawnmap/spawn_point_selected.png";
		replaceSpawnButton(activeSpawnLocation, image);
	}

	private void replaceSpawnButton(Vector3f location, String image) {
		ImageButton imageButton = spawnPoints.get(location);

		float iconSize = imageButton.getWidth();
		float mapX = getMapCoords(minX, xRange, mapWidth, location.getX());
		float mapY = getMapCoords(minZ, zRange, mapHeight, location.getZ());
		addSpawnIcon(location, iconSize, mapX, mapY, image);
	}
}