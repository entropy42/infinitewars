package org.wl.infinitewars.client.ui.hud;

import java.util.ArrayList;
import java.util.List;

import org.wl.infinitewars.client.event.gameplay.UpdateNearestTargetEvent;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.util.FileConstants;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingSphere;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.ui.Picture;

public class HUDMarkerControl extends AbstractShipMarkerControl<Node> {

	private static final String ENEMY_MARKER = FileConstants.HUD_PATH + "enemyMarker.png";
	private static final String TARGET_MARKER = FileConstants.HUD_PATH + "targetMarker.png";
	private static final String TEAM_MARKER = FileConstants.HUD_PATH + "teamMarker.png";

	private final Picture pictureTopRight;
	private final Picture pictureTopLeft;
	private final Picture pictureBottomLeft;
	private final Picture pictureBottomRight;

	private final List<Picture> pictures = new ArrayList<>(4);

	private final float size = camera.getWidth() / 80f;

	public HUDMarkerControl(Node node, Node guiNode, Camera camera, AssetManager assetManager, Player player, Player localPlayer) {
		super(node, guiNode, camera, player, localPlayer, assetManager);

		pictureTopLeft = new Picture("HUD Marker Top Left");
		pictures.add(pictureTopLeft);

		pictureTopRight = new Picture("HUD Marker Top Right");
		pictureTopRight.setLocalRotation(pictureTopRight.getLocalRotation().fromAngleAxis(-FastMath.HALF_PI, Vector3f.UNIT_Z));
		pictures.add(pictureTopRight);

		pictureBottomLeft = new Picture("HUD Marker Bottom Left");
		pictureBottomLeft.setLocalRotation(pictureBottomLeft.getLocalRotation().fromAngleAxis(FastMath.HALF_PI, Vector3f.UNIT_Z));
		pictures.add(pictureBottomLeft);

		pictureBottomRight = new Picture("HUD Marker Bottom Right");
		pictureBottomRight.setLocalRotation(pictureBottomRight.getLocalRotation().fromAngleAxis(FastMath.PI, Vector3f.UNIT_Z));
		pictures.add(pictureBottomRight);

		pictures.forEach(picture -> {
			picture.setWidth(size);
			picture.setHeight(size);

			String image = isSameTeam ? TEAM_MARKER : ENEMY_MARKER;
			picture.setImage(assetManager, image, true);
			pictureSpatial.attachChild(picture);
		});
	}

	@Override
	public void onUpdateNearestTarget(UpdateNearestTargetEvent event) {
		String markerString = isSameTeam ? TEAM_MARKER : ENEMY_MARKER;

		if (event.getTarget() != null && event.getTarget().getShip() == spatial) {
			markerString = TARGET_MARKER;
		}

		final String newMarker = markerString;

		pictures.forEach(picture -> picture.setImage(assetManager, newMarker, true));
	}

	@Override
	protected void update(Vector3f position) {
		float x = screenPos.getX();
		float y = screenPos.getY();

		Node node = (Node)spatial;
		Spatial child = node.getChild(0);
		BoundingSphere sphere = (BoundingSphere)child.getWorldBound();
		float sphereSize = sphere.getRadius();

		Vector3f camLeft = camera.getLeft();

		Vector3f left = camera.getScreenCoordinates(position.add(camLeft.mult(sphereSize)));
		Vector3f right = camera.getScreenCoordinates(position.add(camLeft.mult(-sphereSize)));

		float additionalSize = (right.x - left.x) / 2.5f;

		pictureTopLeft.setPosition(x - size - additionalSize, y + additionalSize);
		pictureTopRight.setPosition(x + additionalSize, y + size + additionalSize);
		pictureBottomLeft.setPosition(x - additionalSize, y - size - additionalSize);
		pictureBottomRight.setPosition(x + size + additionalSize, y - additionalSize);
	}

	@Override
	protected boolean show() {
		return screenPos.getZ() < 1;
	}
}