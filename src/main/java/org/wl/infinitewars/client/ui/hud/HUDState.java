package org.wl.infinitewars.client.ui.hud;

import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.ui.GUIState;

public interface HUDState extends GUIState {

	void init(Player localPlayer);
}