package org.wl.infinitewars.client.ui.hud;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.BooleanUtils;
import org.wl.infinitewars.client.event.gameplay.ChangeCurrentControlPointEvent;
import org.wl.infinitewars.client.event.gameplay.ChangeCurrentControlPointEventHandler;
import org.wl.infinitewars.client.event.gameplay.SpectateNextPlayerEvent;
import org.wl.infinitewars.client.event.gameplay.SpectateNextPlayerHandler;
import org.wl.infinitewars.client.event.gameplay.SpectatedPlayerChangedEvent;
import org.wl.infinitewars.client.event.gameplay.UpdateNearestTargetEvent;
import org.wl.infinitewars.client.event.gameplay.UpdateNearestTargetHandler;
import org.wl.infinitewars.client.event.main.UpdateCursorEvent;
import org.wl.infinitewars.client.gameplay.CameraMovementControl;
import org.wl.infinitewars.client.gameplay.ChaseCameraHelper;
import org.wl.infinitewars.client.gameplay.EngineAudioControl;
import org.wl.infinitewars.client.gameplay.SpaceDustControl;
import org.wl.infinitewars.shared.NodeConstants;
import org.wl.infinitewars.shared.control.MoveControl;
import org.wl.infinitewars.shared.control.MovementInfo;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityHelper;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeEvent;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeHandler;
import org.wl.infinitewars.shared.event.gameplay.RespawnPlayerEvent;
import org.wl.infinitewars.shared.event.gameplay.RespawnPlayerHandler;
import org.wl.infinitewars.shared.event.main.GameTimeUpdateEvent;
import org.wl.infinitewars.shared.event.main.GameTimeUpdateHandler;
import org.wl.infinitewars.shared.gameplay.EntityPropertyChangeCause;
import org.wl.infinitewars.shared.gameplay.GameInfoProvider;
import org.wl.infinitewars.shared.gameplay.WeaponFireCause;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.json.Faction;
import org.wl.infinitewars.shared.loading.EnqueueHelper;
import org.wl.infinitewars.shared.ui.GUIStateImpl;
import org.wl.infinitewars.shared.ui.ScreenUtils;
import org.wl.infinitewars.shared.util.FileConstants;
import org.wl.infinitewars.shared.util.FormatHelper;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.math.Vector2f;
import com.jme3.renderer.Camera;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.ui.Picture;

import tonegod.gui.controls.text.Label;
import tonegod.gui.core.Screen;

@Singleton
public class HUDStateImpl extends GUIStateImpl implements HUDState {

	private final EnqueueHelper enqueueHelper;
	private final Camera camera;
	private final AssetManager assetManager;
	private final GameInfoProvider gameInfoProvider;

	private Player localPlayer;
	private Player spectatedPlayer;
	private Label lblGameTime;
	private ShipStatusPanel panShipStatus;
	private CapturePointPanel panCapturePoints;
	private MotherShipStatusPanel panMotherShipStatus;

	private Node spectatedShip;
	private final CameraNode cameraNode;

	private final List<AbstractShipMarkerControl<?>> shipControls = new ArrayList<>();

	@Inject
	public HUDStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode, EnqueueHelper enqueueHelper, Camera camera, AssetManager assetManager, GameInfoProvider gameInfoProvider) {
		super(screen, appRootNode, appGuiNode);
		this.enqueueHelper = enqueueHelper;
		this.camera = camera;
		this.assetManager = assetManager;
		this.gameInfoProvider = gameInfoProvider;

		cameraNode = new CameraNode("Camera Node For Spectate", camera);

		initEventBus();
	}

	private void initEventBus() {
		addEventHandler(GameTimeUpdateEvent.TYPE, new GameTimeUpdateHandler() {
			@Override
			public void onGameTimeUpdate(GameTimeUpdateEvent event) {
				updateGameTimer(event.getCurrentTime());
			}
		});

		addEventHandler(SpectateNextPlayerEvent.TYPE, new SpectateNextPlayerHandler() {
			@Override
			public void onChange(SpectateNextPlayerEvent event) {
				List<Player> players = localPlayer.getTeam().getPlayers();

				int steps = 0;
				int index = players.indexOf(spectatedPlayer);

				do {
					if (index == players.size() - 1) {
						index = 0;
					} else {
						index++;
					}

					spectatedPlayer = players.get(index);

					steps++;
				} while (!spectatedPlayer.isAlive() && steps <= players.size());

				fireEvent(new SpectatedPlayerChangedEvent(spectatedPlayer));

				for (AbstractShipMarkerControl<?> control : shipControls) {
					control.setSpectatedPlayer(spectatedPlayer);
				}

				updateChaseCamera();
			}
		});

		addEventHandler(RespawnPlayerEvent.TYPE, new RespawnPlayerHandler() {
			@Override
			public void onRespawnPlayer(RespawnPlayerEvent event) {
				respawn(event);
			}
		});

		addEventHandler(EntityPropertyChangeEvent.TYPE, new EntityPropertyChangeHandler() {
			@Override
			public void onChange(EntityPropertyChangeEvent event) {
				changeEntityProperty(event);
			}
		});

		addEventHandler(UpdateNearestTargetEvent.TYPE, new UpdateNearestTargetHandler() {
			@Override
			public void onUpdateNearestTarget(UpdateNearestTargetEvent event) {
				for (AbstractShipMarkerControl<?> control : shipControls) {
					control.onUpdateNearestTarget(event);
				}
			}
		});

		addEventHandler(ChangeCurrentControlPointEvent.TYPE, new ChangeCurrentControlPointEventHandler() {
			@Override
			public void onChangeCurrentControlPoint(ChangeCurrentControlPointEvent event) {
				Spatial controlPoint = event.getControlPoint();

				final long currentCapturePointId = controlPoint != null ? EntityHelper.getEntityId(controlPoint) : 0L;

				enqueueHelper.enqueue(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						panCapturePoints.setCurrentCapturePointId(currentCapturePointId);
						return null;
					}
				});
			}
		});
	}

	@Override
	public void update(float tpf) {
		super.update(tpf);

		Node ship = spectatedPlayer.getShip();

		if (spectatedPlayer.isAlive() && ship != null) {
			MoveControl moveControl = ship.getControl(MoveControl.class);

			if (moveControl != null) {
				MovementInfo moveInfo = moveControl.getMoveInfo();

				if (moveInfo != null) {
					panShipStatus.setSpeedPercentage(moveInfo.getForwardSpeedPercentage());
				}
			}
		}
	}

	private void respawn(RespawnPlayerEvent event) {
		final Player player = event.getPlayer();

		if (player.isLocalPlayer() || spectatedPlayer == null) {
			spectatedPlayer = player;
		}

		if (player == spectatedPlayer) {
			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					setEnabled(true);
					panShipStatus.setShieldsPercentage(1f);
					panShipStatus.setHitPointsPercentage(1f);
					panShipStatus.setSpeedPercentage(0f);
					panShipStatus.setSpecialWeaponEnergyPercentage(1f);
					fireEvent(new UpdateCursorEvent());

					if (spectatedShip != null) {
						spectatedShip.removeControl(CameraMovementControl.class);
					}

					return null;
				}
			});
		} else {
			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					HUDMarkerControl markerControl = getMarkerControl(player);
					markerControl.setEnabled(true);

					HUDArrowControl arrowControl = getArrowControl(player);
					arrowControl.setEnabled(true);

					HUDTargetLeadIndicatorControl targetLeadControl = getTargetLeadIndicatorControl(player);
					targetLeadControl.setEnabled(true);

					return null;
				}
			});
		}
	}

	private void changeEntityProperty(EntityPropertyChangeEvent event) {
		Spatial entity = event.getEntity();
		final Player player = entity.getUserData(EntityConstants.player);
		long entityId = EntityHelper.getEntityId(entity);
		Boolean isMotherShip = entity.getUserData(EntityConstants.isMotherShip);
		String propertyName = event.getPropertyName();
		EntityPropertyChangeCause cause = event.getCause();

		if (player != null) {
			if (player == spectatedPlayer) {
				if (propertyName.equals(EntityConstants.shields)) {
					int maxShields = entity.getUserData(EntityConstants.maxShields);
					int currentShields = (int)event.getPropertyValue();

					final float percentage = currentShields / (float)maxShields;

					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							panShipStatus.setShieldsPercentage(percentage);
							return null;
						}
					});
				} else if (propertyName.equals(EntityConstants.hitPoints)) {
					int maxHitPoints = entity.getUserData(EntityConstants.maxHitPoints);
					int currentHitPoints = (int)event.getPropertyValue();

					final float percentage = currentHitPoints / (float)maxHitPoints;

					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							panShipStatus.setHitPointsPercentage(percentage);
							return null;
						}
					});
				} else if (propertyName.equals(EntityConstants.specialWeaponEnergy)) {
					float maxSpecialWeaponEnergy = entity.getUserData(EntityConstants.maxSpecialWeaponEnergy);
					float specialWeaponEnergy = (float)event.getPropertyValue();

					final float percentage = specialWeaponEnergy / maxSpecialWeaponEnergy;

					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							panShipStatus.setSpecialWeaponEnergyPercentage(percentage);
							return null;
						}
					});
				} else if (propertyName.equals(EntityConstants.boostEnergy)) {
					float maxBoostEnergy = entity.getUserData(EntityConstants.maxBoostEnergy);
					float boostEnergy = (float)event.getPropertyValue();

					final float percentage = boostEnergy / maxBoostEnergy;

					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							panShipStatus.setBoostEnergyPercentage(percentage);
							return null;
						}
					});
				}
			} else if (propertyName.equals(EntityConstants.hitPoints)) {
				int currentHitPoints = (int)event.getPropertyValue();

				if (currentHitPoints <= 0l) {
					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							HUDMarkerControl markerControl = getMarkerControl(player);
							markerControl.setEnabled(false);

							HUDArrowControl arrowControl = getArrowControl(player);
							arrowControl.setEnabled(false);

							HUDTargetLeadIndicatorControl targetLeadControl = getTargetLeadIndicatorControl(player);
							targetLeadControl.setEnabled(false);

							return null;
						}
					});
				}

				if (cause != null && cause instanceof WeaponFireCause) {
					HUDTargetLeadIndicatorControl targetLeadControl = getTargetLeadIndicatorControl(player);

					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							targetLeadControl.registerHit();
							return null;
						}
					});
				}
			} else if (propertyName.equals(EntityConstants.shields)) {
				if (cause != null && cause instanceof WeaponFireCause) {
					HUDTargetLeadIndicatorControl targetLeadControl = getTargetLeadIndicatorControl(player);

					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							targetLeadControl.registerHit();
							return null;
						}
					});
				}
			}
		} else if (panCapturePoints != null && entityId == panCapturePoints.getCurrentCapturePointId()) {
			final Float capturePoints1 = entity.getUserData(EntityConstants.controlPointCapturePointsTeam1);
			final Float capturePoints2 = entity.getUserData(EntityConstants.controlPointCapturePointsTeam2);
			final int maxCapturePoints = entity.getUserData(EntityConstants.controlPointCapturePoints);

			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					if (capturePoints1 != null) {
						panCapturePoints.updatePercentage1(capturePoints1 / maxCapturePoints);
					}

					if (capturePoints2 != null) {
						panCapturePoints.updatePercentage2(capturePoints2 / maxCapturePoints);
					}

					return null;
				}
			});
		} else if (panMotherShipStatus != null && BooleanUtils.isTrue(isMotherShip)) {
			Faction faction = Faction.valueOf(entity.getUserData(EntityConstants.faction));
			final boolean isEnemy = faction != spectatedPlayer.getTeam().getFaction();

			if (propertyName.equals(EntityConstants.shields)) {
				int maxShields = entity.getUserData(EntityConstants.maxShields);
				int currentShields = (int)event.getPropertyValue();

				final float percentage = currentShields / (float)maxShields;

				enqueueHelper.enqueue(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						if (isEnemy) {
							panMotherShipStatus.setEnemyTeamShieldsPercentage(percentage);
						} else {
							panMotherShipStatus.setTeamShieldsPercentage(percentage);
						}

						return null;
					}
				});
			} else if (propertyName.equals(EntityConstants.hitPoints)) {
				int maxHitPoints = entity.getUserData(EntityConstants.maxHitPoints);
				int currentHitPoints = (int)event.getPropertyValue();

				final float percentage = currentHitPoints / (float)maxHitPoints;

				enqueueHelper.enqueue(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						if (isEnemy) {
							panMotherShipStatus.setEnemyTeamHitPointsPercentage(percentage);
						} else {
							panMotherShipStatus.setTeamHitPointsPercentage(percentage);
						}

						return null;
					}
				});
			}
		}
	}

	@Override
	public void init() {

	}

	@Override
	public void init(Player localPlayer) {
		this.localPlayer = localPlayer;
		spectatedPlayer = localPlayer;

		float screenWidth = screen.getWidth();
		float screenHeight = screen.getHeight();

		float widgetHeight = ScreenUtils.getWidgetHeight(screen);
		float widgetWidth = ScreenUtils.getWidgetWidth(screen);

		float statusWidth = widgetWidth * 3f;
		float statusHeight = widgetHeight * 3f;
		panShipStatus = new ShipStatusPanel(screen, statusWidth, statusHeight);
		panShipStatus.setPosition((screenWidth / 2) - (statusWidth / 2), screenHeight - statusHeight);
		mainContent.addChild(panShipStatus);

		float padding = ScreenUtils.getPadding(screen);
		Vector2f widgetSize = ScreenUtils.createWidgetSize(screen);

		float x = screenWidth - (padding * 3);
		float y = screenHeight / 2;

		Vector2f pos = new Vector2f(x, y);
		lblGameTime = new Label(screen, pos, widgetSize);
		mainContent.addChild(lblGameTime);

		lblGameTime.setText("0:00");

		panCapturePoints = new CapturePointPanel(screen);
		panCapturePoints.setY(padding);
		mainContent.addChild(panCapturePoints);
		panCapturePoints.setIsVisible(false);

		switch (gameInfoProvider.getGameInfo().getGameMode()) {
			case TEAM_DEATH_MATCH:
				break;
			case CONQUEST:
				break;
			case MOTHER_SHIP:
				float width = screen.getWidth() / 5f * 3f;
				float height = widgetHeight;
				panMotherShipStatus = new MotherShipStatusPanel(screen, width, height);
				panMotherShipStatus.setX((screen.getWidth() / 2f) - (width / 2f));
				mainContent.addChild(panMotherShipStatus);
				break;
		}
	}

	private void updateGameTimer(float time) {
		if (lblGameTime != null) {
			final String timeString = FormatHelper.formatTime(time);

			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					lblGameTime.setText(timeString);
					return null;
				}
			});
		}
	}

	private HUDMarkerControl createMarkerControl(Player player) {
		Node picture = new Node("HUD Marker Node");
		HUDMarkerControl control = new HUDMarkerControl(picture, guiNode, camera, assetManager, player, spectatedPlayer);
		shipControls.add(control);
		return control;
	}

	private HUDMarkerControl getMarkerControl(Player player) {
		Node ship = player.getShip();
		HUDMarkerControl control = ship.getControl(HUDMarkerControl.class);

		if (control == null) {
			control = createMarkerControl(player);
			ship.addControl(control);
		}

		return control;
	}

	private HUDArrowControl createArrowControl(Player player) {
		Picture picture = new Picture("HUD Arrow");
		picture.setImage(assetManager, FileConstants.HUD_PATH + "enemyArrow.png", true);
		HUDArrowControl control = new HUDArrowControl(picture, guiNode, camera, player, spectatedPlayer, assetManager);
		shipControls.add(control);
		return control;
	}

	private HUDArrowControl getArrowControl(Player player) {
		Node ship = player.getShip();
		HUDArrowControl control = ship.getControl(HUDArrowControl.class);

		if (control == null) {
			control = createArrowControl(player);
			ship.addControl(control);
		}

		return control;
	}

	private HUDTargetLeadIndicatorControl createTargetLeadIndicationControl(Player player) {
		Node pictureNode = new Node();
		HUDTargetLeadIndicatorControl control = new HUDTargetLeadIndicatorControl(pictureNode, guiNode, camera, player, spectatedPlayer, assetManager);
		shipControls.add(control);
		return control;
	}

	private HUDTargetLeadIndicatorControl getTargetLeadIndicatorControl(Player player) {
		Node ship = player.getShip();
		HUDTargetLeadIndicatorControl control = ship.getControl(HUDTargetLeadIndicatorControl.class);

		if (control == null) {
			control = createTargetLeadIndicationControl(player);
			ship.addControl(control);
		}

		return control;
	}

	private void updateChaseCamera() {
		if (spectatedShip != null) {
			spectatedShip.removeControl(CameraMovementControl.class);
			spectatedShip.removeControl(SpaceDustControl.class);
			spectatedShip.removeControl(EngineAudioControl.class);
			spectatedShip.detachChild(cameraNode);

			AudioNode audioNode = (AudioNode)spectatedShip.getChild(EntityConstants.shipEngineAudioNode);

			if (audioNode != null) {
				audioNode.setPitch(1f);
				audioNode.setPositional(true);
			}
		}

		spectatedShip = spectatedPlayer.getShip();

		if (spectatedShip != null) {
			ChaseCameraHelper.setupChaseCamera(spectatedShip, cameraNode, assetManager);
		}
	}

	@Override
	public void cleanup() {
		super.cleanup();

		for (AbstractShipMarkerControl<?> control : shipControls) {
			control.setEnabled(false);
			Spatial spatial = control.getSpatial();

			if (spatial != null) {
				spatial.removeControl(control);
			}
		}

		shipControls.clear();
	}
}