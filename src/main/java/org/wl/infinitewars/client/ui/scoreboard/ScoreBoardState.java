package org.wl.infinitewars.client.ui.scoreboard;

import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.ui.GUIState;

public interface ScoreBoardState extends GUIState {

	void init(Team team1, Team team2);
}