package org.wl.infinitewars.client.ui.hud;

import org.wl.infinitewars.shared.loading.LoadingBar;
import org.wl.infinitewars.shared.ui.EmptyContainer;

import tonegod.gui.core.ElementManager;

public class MotherShipStatusPanel extends EmptyContainer {

	private LoadingBar barShieldsTeam;
	private LoadingBar barHealthTeam;
	private LoadingBar barShieldsEnemyTeam;
	private LoadingBar barHealthEnemyTeam;

	public MotherShipStatusPanel(ElementManager screen, float width, float height) {
		super(screen);

		setDimensions(width, height);

		init();
	}

	private void init() {
		float widgetWidth = getWidth() / 2f;
		float widgetHeight = getHeight() / 2f;
		float x = 0f;
		float barY = getHeight() - widgetHeight;

		barShieldsTeam = new LoadingBar(screen, widgetWidth, widgetHeight, "blueBar.png");
		barShieldsTeam.setPosition(x, barY);
		barShieldsTeam.updateProgress(1f);
		addChild(barShieldsTeam);

		barY -= widgetHeight;

		barHealthTeam = new LoadingBar(screen, widgetWidth, widgetHeight, "greenBar.png");
		barHealthTeam.setPosition(x, barY);
		barHealthTeam.updateProgress(1f);
		addChild(barHealthTeam);

		barY = getHeight() - widgetHeight;
		x += widgetWidth;

		barShieldsEnemyTeam = new LoadingBar(screen, widgetWidth, widgetHeight, "blueBar.png");
		barShieldsEnemyTeam.setPosition(x, barY);
		barShieldsEnemyTeam.updateProgress(1f);
		addChild(barShieldsEnemyTeam);

		barY -= widgetHeight;

		barHealthEnemyTeam = new LoadingBar(screen, widgetWidth, widgetHeight, "redBar.png");
		barHealthEnemyTeam.setPosition(x, barY);
		barHealthEnemyTeam.updateProgress(1f);
		addChild(barHealthEnemyTeam);
	}

	public void setTeamShieldsPercentage(float percentage) {
		barShieldsTeam.updateProgress(percentage);
	}

	public void setTeamHitPointsPercentage(float percentage) {
		barHealthTeam.updateProgress(percentage);
	}

	public void setEnemyTeamShieldsPercentage(float percentage) {
		barShieldsEnemyTeam.updateProgress(percentage);
	}

	public void setEnemyTeamHitPointsPercentage(float percentage) {
		barHealthEnemyTeam.updateProgress(percentage);
	}
}