package org.wl.infinitewars.client.ui.scoreboard;

import javax.inject.Singleton;

import org.wl.infinitewars.shared.NodeConstants;
import org.wl.infinitewars.shared.event.gameplay.PlayerPropertyChangeEvent;
import org.wl.infinitewars.shared.event.gameplay.PlayerPropertyChangeHandler;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.ui.GUIStateImpl;
import org.wl.infinitewars.shared.ui.ScreenUtils;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

import tonegod.gui.core.Screen;

@Singleton
public class ScoreBoardStateImpl extends GUIStateImpl implements ScoreBoardState {

	@Inject
	public ScoreBoardStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);
	}

	@Override
	public void init() {

	}

	@Override
	public void init(Team team1, Team team2) {
		float width = screen.getWidth();
		float height = screen.getHeight();
		float padding = ScreenUtils.getPadding(screen);

		float tableWidth = (width / 2f) - (padding / 2);
		float tableHeight = height - (padding * 2f);

		Vector2f dimensions = new Vector2f(tableWidth, tableHeight);
		Vector2f pos1 = new Vector2f(padding, padding);
		Vector2f pos2 = new Vector2f((padding * 2f) + tableWidth, padding);

		addScoreTable(team1, pos1, dimensions);
		addScoreTable(team2, pos2, dimensions);
	}

	private void addScoreTable(Team team, Vector2f pos, Vector2f dimensions) {
		final ScoreTable table = new ScoreTable(screen, pos, dimensions, team);
		addEventHandler(PlayerPropertyChangeEvent.TYPE, new PlayerPropertyChangeHandler() {
			@Override
			public void onChange(PlayerPropertyChangeEvent event) {
				table.onPlayerPropertyChange(event);
			}
		});
		mainContent.addChild(table);
	}
}