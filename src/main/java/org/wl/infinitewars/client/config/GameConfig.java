package org.wl.infinitewars.client.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

import org.apache.commons.lang3.SystemUtils;
import org.wl.infinitewars.client.event.ClientConfigChangedEvent;
import org.wl.infinitewars.client.event.config.UpdateDebugOptionEvent;
import org.wl.infinitewars.client.event.main.QuitEvent;
import org.wl.infinitewars.client.ui.console.ConsoleCommand;
import org.wl.infinitewars.shared.config.DebugConfigConstants;
import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.util.FileConstants;

import nu.studer.java.util.OrderedProperties;
import nu.studer.java.util.OrderedProperties.OrderedPropertiesBuilder;

public class GameConfig {

	private static final EventBus eventBus = EventBus.get();

	private static OrderedProperties properties;

	public static String getGameConfigFileDirectoryLocation() {
		return SystemUtils.getUserHome() + "/" + FileConstants.GAME_CONFIG_FILE_DIR;
	}

	public static String getClientConfigFileLocation() {
		return getGameConfigFileDirectoryLocation() + FileConstants.CLIENT_CONFIG_FILE;
	}

	public static void loadFromFile() throws FileNotFoundException, IOException {
		OrderedPropertiesBuilder builder = new OrderedPropertiesBuilder();
		builder.withOrdering(String.CASE_INSENSITIVE_ORDER);
		builder.withSuppressDateInComment(true);

		properties = builder.build();

		try (InputStream inputStream = new FileInputStream(new File(getClientConfigFileLocation()))) {
			properties.load(inputStream);
		}
	}

	public static Collection<String> getBindingNames() {
		Collection<String> bindingNames = new ArrayList<>();
		Enumeration<String> propertyNames = properties.propertyNames();

		while (propertyNames.hasMoreElements()) {
			String commandName = propertyNames.nextElement();

			if (commandName.startsWith("bind_")) {
				bindingNames.add(commandName);
			}
		}

		return bindingNames;
	}

	public static Collection<ConsoleCommand> getAvailableCommands() {
		Collection<ConsoleCommand> commands = new ArrayList<>();
		Enumeration<String> propertyNames = properties.propertyNames();

		while (propertyNames.hasMoreElements()) {
			String commandName = propertyNames.nextElement();

			commands.add(new ConsoleCommand(commandName, 1) {
				@Override
				public void execute(String key, String... args) throws Exception {
					changeProperty(key, args[0]);
				}
			});
		}

		commands.add(new ConsoleCommand("quit", 0) {
			@Override
			protected void execute(String key, String... args) throws Exception {
				eventBus.fireEvent(new QuitEvent());
			}
		});

		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_damage_collision, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_damage_weapons, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_show_bounding_volumes, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_show_weaponfire_bounding_volumes, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_bot_targeting, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_bot_movement, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_show_weapon_paths, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_weapon_fire_movement, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_switch_team, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_log_player_infos, 2);
		addDebugCommand(commands, DebugConfigConstants.dbg_change_entity_property, 2);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_bot_spectating_data, 0);
		addDebugCommand(commands, DebugConfigConstants.dbg_toggle_draw_collision_bounds, 0);

		return commands;
	}

	protected static void addDebugCommand(Collection<ConsoleCommand> commands, String commandKey, int argumentCount) {
		commands.add(new ConsoleCommand(commandKey, argumentCount) {
			@Override
			protected void execute(String key, String... args) throws Exception {
				eventBus.fireEvent(new UpdateDebugOptionEvent(commandKey, args));
			}
		});
	}

	public static String getString(String key) {
		return properties.getProperty(key);
	}

	public static int getInt(String key) {
		return Integer.valueOf(properties.getProperty(key));
	}

	public static float getFloat(String key) {
		return Float.valueOf(properties.getProperty(key));
	}

	public static boolean getBoolean(String key) {
		return Boolean.valueOf(properties.getProperty(key));
	}

	public static void changeProperty(String key, String value) throws IOException {
		properties.setProperty(key, value);
		save();
		eventBus.fireEvent(new ClientConfigChangedEvent(key, value));
	}

	public static void setUsername(String username) throws IOException {
		properties.setProperty(ClientConfigConstants.c_username, username);
		save();
	}

	public static void save() throws IOException {
		try (Writer writer = new FileWriter(getClientConfigFileLocation())) {
			properties.store(writer, "");
		}
	}
}