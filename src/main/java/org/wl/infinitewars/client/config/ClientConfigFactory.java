package org.wl.infinitewars.client.config;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Properties;

import com.jme3.system.JmeContext;

public class ClientConfigFactory {

	public void initDefaultConfig(JmeContext.Type jmeContextType) throws Exception {
		DisplayMode bestMode = new DisplayMode(1024, 768, 32, 30);

		if (jmeContextType == JmeContext.Type.Display) {
			GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
			bestMode = device.getDisplayMode();
		}

		Properties properties = new Properties();

		String resolution = bestMode.getWidth() + "x" + bestMode.getHeight();

		properties.put(ClientConfigConstants.r_resolution, resolution);
		properties.put(ClientConfigConstants.r_fullscreen, true + "");
		properties.put(ClientConfigConstants.r_bits_per_pixel, bestMode.getBitDepth() + "");
		properties.put(ClientConfigConstants.r_frequency, bestMode.getRefreshRate() + "");

		properties.put(ClientConfigConstants.c_username, "PilotOne");

		properties.put(ClientConfigConstants.bind_forward, "KEY_W");
		properties.put(ClientConfigConstants.bind_backward, "KEY_S");
		properties.put(ClientConfigConstants.bind_roll_left, "KEY_Q");
		properties.put(ClientConfigConstants.bind_roll_right, "KEY_E");
		properties.put(ClientConfigConstants.bind_strafe_left, "KEY_A");
		properties.put(ClientConfigConstants.bind_strafe_right, "KEY_D");
		properties.put(ClientConfigConstants.bind_boost, "KEY_LSHIFT");
		properties.put(ClientConfigConstants.bind_nearest_enemy_target, "KEY_R");
		properties.put(ClientConfigConstants.bind_toggle_score_board, "KEY_TAB");
		properties.put(ClientConfigConstants.bind_toggle_spawn_menu, "KEY_RETURN");
		properties.put(ClientConfigConstants.bind_free_mouse, "KEY_LMENU");
		properties.put(ClientConfigConstants.bind_shoot_standard_weapons, "BUTTON_LEFT");
		properties.put(ClientConfigConstants.bind_shoot_special_weapons, "BUTTON_RIGHT");

		File dir = new File(GameConfig.getGameConfigFileDirectoryLocation());

		if (!dir.exists()) {
			dir.mkdir();
		}

		File file = new File(GameConfig.getClientConfigFileLocation());

		if (!file.exists()) {
			file.createNewFile();
		}

		try (Writer writer = new FileWriter(file)) {
			properties.store(writer, "");
		}
	}
}