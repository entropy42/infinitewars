package org.wl.infinitewars.client.config;

public interface ClientConfigConstants {

	String r_resolution = "r_resolution";
	String r_fullscreen = "r_fullscreen";
	String r_bits_per_pixel = "r_bits_per_pixel";
	String r_frequency = "r_frequency";

	String c_username = "c_username";

	String bind_shoot_standard_weapons = "bind_shoot_standard_weapons";
	String bind_shoot_special_weapons = "bind_shoot_special_weapons";
	String bind_free_mouse = "bind_free_mouse";
	String bind_forward = "bind_forward";
	String bind_backward = "bind_backward";
	String bind_roll_left = "bind_roll_left";
	String bind_roll_right = "bind_roll_right";
	String bind_strafe_left = "bind_strafe_left";
	String bind_strafe_right = "bind_strafe_right";
	String bind_boost = "bind_boost";
	String bind_nearest_enemy_target = "bind_nearest_enemy_target";
	String bind_toggle_score_board = "bind_toggle_score_board";
	String bind_toggle_spawn_menu = "bind_toggle_spawn_menu";
}