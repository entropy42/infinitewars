package org.wl.infinitewars.client;

import org.wl.infinitewars.client.entity.ClientEntityFactory;
import org.wl.infinitewars.client.entity.ClientEntityIdProvider;
import org.wl.infinitewars.client.gameplay.ClientIngameState;
import org.wl.infinitewars.client.gameplay.ClientIngameStateImpl;
import org.wl.infinitewars.client.gameplay.weapon.ClientGatlingWeaponFireFactory;
import org.wl.infinitewars.client.gameplay.weapon.ClientLightningWeaponFireFactory;
import org.wl.infinitewars.client.gameplay.weapon.ClientRailGunWeaponFireFactory;
import org.wl.infinitewars.client.gameplay.weapon.ClientRocketLauncherWeaponFireFactory;
import org.wl.infinitewars.client.gameplay.weapon.ClientStandardWeaponFireFactory;
import org.wl.infinitewars.client.loading.ClientLoadingStepsBuilder;
import org.wl.infinitewars.client.network.ClientFactory;
import org.wl.infinitewars.client.network.ClientFactoryImpl;
import org.wl.infinitewars.client.network.ClientState;
import org.wl.infinitewars.client.network.ClientStateImpl;
import org.wl.infinitewars.client.network.ClientSyncState;
import org.wl.infinitewars.client.network.messagehandler.ClientMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.DebugConfigChangedMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.EndTimerUpdateMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.EntityPropertyChangeMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.GameInfoInitMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.GameStateResponseMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.GameTimeUpdateMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.PlayerNameRequestMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.PlayerPropertyChangeMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.PlayerShipChangeMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.RemoveWeaponFireMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.RespawnPlayerMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.RestartGameMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.ServerRestartedMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.SpectatedPlayerUpdateMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.WeaponFireCreationMessageHandler;
import org.wl.infinitewars.client.network.messagehandler.WinConditionReachedMessageHandler;
import org.wl.infinitewars.client.ui.console.ConsoleState;
import org.wl.infinitewars.client.ui.console.ConsoleStateImpl;
import org.wl.infinitewars.client.ui.end.EndState;
import org.wl.infinitewars.client.ui.end.EndStateImpl;
import org.wl.infinitewars.client.ui.hud.HUDState;
import org.wl.infinitewars.client.ui.hud.HUDStateImpl;
import org.wl.infinitewars.client.ui.menu.MenuState;
import org.wl.infinitewars.client.ui.menu.MenuStateImpl;
import org.wl.infinitewars.client.ui.profileselection.UsernameInputState;
import org.wl.infinitewars.client.ui.profileselection.UsernameInputStateImpl;
import org.wl.infinitewars.client.ui.scoreboard.ScoreBoardState;
import org.wl.infinitewars.client.ui.scoreboard.ScoreBoardStateImpl;
import org.wl.infinitewars.client.ui.spawnmenu.SpawnMenuState;
import org.wl.infinitewars.client.ui.spawnmenu.SpawnMenuStateImpl;
import org.wl.infinitewars.shared.AbstractGameApplication;
import org.wl.infinitewars.shared.SharedModule;
import org.wl.infinitewars.shared.entity.EntityFactory;
import org.wl.infinitewars.shared.entity.EntityIdProvider;
import org.wl.infinitewars.shared.gameplay.IngameState;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireFactory;
import org.wl.infinitewars.shared.loading.LoadingStepsBuilder;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;

public class ClientModule extends SharedModule {

	private final ClientGraphicsUpdater graphicsUpdater;

	public ClientModule(AbstractGameApplication app, ClientGraphicsUpdater graphicsUpdater) {
		super(app);
		this.graphicsUpdater = graphicsUpdater;
	}

	@Override
	protected void configure() {
		super.configure();

		bind(IngameState.class).to(ClientIngameStateImpl.class);
		bind(ClientIngameState.class).to(ClientIngameStateImpl.class);
		bind(ClientFactory.class).to(ClientFactoryImpl.class);
		bind(ClientState.class).to(ClientStateImpl.class);
		bind(EntityFactory.class).to(ClientEntityFactory.class);
		bind(EntityIdProvider.class).to(ClientEntityIdProvider.class);
		bind(AbstractSyncState.class).to(ClientSyncState.class);
		bind(LoadingStepsBuilder.class).to(ClientLoadingStepsBuilder.class);
		bind(UsernameInputState.class).to(UsernameInputStateImpl.class);
		bind(ConsoleState.class).to(ConsoleStateImpl.class);
		bind(MenuState.class).to(MenuStateImpl.class);
		bind(HUDState.class).to(HUDStateImpl.class);
		bind(SpawnMenuState.class).to(SpawnMenuStateImpl.class);
		bind(ScoreBoardState.class).to(ScoreBoardStateImpl.class);
		bind(EndState.class).to(EndStateImpl.class);

		bind(ClientGraphicsUpdater.class).toInstance(graphicsUpdater);

		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(DebugConfigChangedMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(EndTimerUpdateMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(EntityPropertyChangeMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(GameInfoInitMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(GameStateResponseMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(GameTimeUpdateMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(PlayerNameRequestMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(PlayerPropertyChangeMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(PlayerShipChangeMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(RemoveWeaponFireMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(RespawnPlayerMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(RestartGameMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(ServerRestartedMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(SpectatedPlayerUpdateMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(WeaponFireCreationMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(WinConditionReachedMessageHandler.class).in(Singleton.class);

		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientStandardWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientLightningWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientGatlingWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientRailGunWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientRocketLauncherWeaponFireFactory.class).in(Singleton.class);
	}
}