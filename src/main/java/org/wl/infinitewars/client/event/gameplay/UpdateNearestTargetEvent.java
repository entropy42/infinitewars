package org.wl.infinitewars.client.event.gameplay;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.gameplay.player.Player;

public class UpdateNearestTargetEvent extends Event<UpdateNearestTargetHandler> {

	public static final EventType<UpdateNearestTargetHandler> TYPE = new EventType<>();

	private final Player target;

	public UpdateNearestTargetEvent(Player target) {
		this.target = target;
	}

	@Override
	public EventType<UpdateNearestTargetHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(UpdateNearestTargetHandler handler) {
		handler.onUpdateNearestTarget(this);
	}

	public Player getTarget() {
		return target;
	}
}