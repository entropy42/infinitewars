package org.wl.infinitewars.client.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface UpdateNearestTargetHandler extends EventHandler {
	
	void onUpdateNearestTarget(UpdateNearestTargetEvent event);
}