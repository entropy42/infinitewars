package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface LoadGameHandler extends EventHandler {
	
	void onLoadGame(LoadGameEvent event);
}