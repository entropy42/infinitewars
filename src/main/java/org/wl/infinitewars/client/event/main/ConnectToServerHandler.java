package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ConnectToServerHandler extends EventHandler {
	
	void onConnect(ConnectToServerEvent event);
}