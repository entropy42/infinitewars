package org.wl.infinitewars.client.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class ToggleSpawnMenuEvent extends Event<ToggleSpawnMenuHandler> {
	
	public static final EventType<ToggleSpawnMenuHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<ToggleSpawnMenuHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ToggleSpawnMenuHandler handler) {
		handler.onChange(this);
	}
}