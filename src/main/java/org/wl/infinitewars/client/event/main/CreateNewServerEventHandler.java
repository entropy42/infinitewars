package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface CreateNewServerEventHandler extends EventHandler {

	void onCreateNewServer(CreateNewServerEvent event);
}