package org.wl.infinitewars.client.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class ToggleMainMenuEvent extends Event<ToggleMainMenuHandler> {
	
	public static final EventType<ToggleMainMenuHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<ToggleMainMenuHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ToggleMainMenuHandler handler) {
		handler.onChange(this);
	}
}