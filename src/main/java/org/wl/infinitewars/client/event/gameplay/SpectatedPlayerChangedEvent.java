package org.wl.infinitewars.client.event.gameplay;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.gameplay.player.Player;

public class SpectatedPlayerChangedEvent extends Event<SpectatedPlayerChangedHandler> {

	public static final EventType<SpectatedPlayerChangedHandler> TYPE = new EventType<>();

	private final Player player;

	public SpectatedPlayerChangedEvent(Player player) {
		this.player = player;
	}

	@Override
	public EventType<SpectatedPlayerChangedHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(SpectatedPlayerChangedHandler handler) {
		handler.onChange(this);
	}

	public Player getPlayer() {
		return player;
	}
}