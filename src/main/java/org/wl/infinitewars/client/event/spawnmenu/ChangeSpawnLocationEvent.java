package org.wl.infinitewars.client.event.spawnmenu;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

import com.jme3.math.Vector3f;

public class ChangeSpawnLocationEvent extends Event<ChangeSpawnLocationEventHandler> {

	public static final EventType<ChangeSpawnLocationEventHandler> TYPE = new EventType<>();

	private final Vector3f requestedSpawnLocation;

	public ChangeSpawnLocationEvent(Vector3f requestedSpawnLocation) {
		this.requestedSpawnLocation = requestedSpawnLocation;
	}

	@Override
	public EventType<ChangeSpawnLocationEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(ChangeSpawnLocationEventHandler handler) {
		handler.onChangeSpawnLocation(this);
	}

	public Vector3f getRequestedSpawnLocation() {
		return requestedSpawnLocation;
	}
}