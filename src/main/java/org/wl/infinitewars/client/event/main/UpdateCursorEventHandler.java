package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface UpdateCursorEventHandler extends EventHandler {

	void onUpdateCursor(UpdateCursorEvent event);
}