package org.wl.infinitewars.client.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface SpectatedPlayerChangedHandler extends EventHandler {

	void onChange(SpectatedPlayerChangedEvent event);
}