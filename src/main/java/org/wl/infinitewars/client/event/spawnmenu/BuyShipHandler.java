package org.wl.infinitewars.client.event.spawnmenu;

import org.wl.infinitewars.shared.event.EventHandler;

public interface BuyShipHandler extends EventHandler {
	
	void onBuyShip(BuyShipEvent event);
}