package org.wl.infinitewars.client.event.gameplay;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class SpectateNextPlayerEvent extends Event<SpectateNextPlayerHandler> {

	public static final EventType<SpectateNextPlayerHandler> TYPE = new EventType<>();

	@Override
	public EventType<SpectateNextPlayerHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(SpectateNextPlayerHandler handler) {
		handler.onChange(this);
	}
}