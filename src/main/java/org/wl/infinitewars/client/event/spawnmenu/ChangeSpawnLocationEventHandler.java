package org.wl.infinitewars.client.event.spawnmenu;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ChangeSpawnLocationEventHandler extends EventHandler {

	void onChangeSpawnLocation(ChangeSpawnLocationEvent event);
}