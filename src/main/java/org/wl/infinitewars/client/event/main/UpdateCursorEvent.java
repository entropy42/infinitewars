package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class UpdateCursorEvent extends Event<UpdateCursorEventHandler> {

	public static final EventType<UpdateCursorEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<UpdateCursorEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(UpdateCursorEventHandler handler) {
		handler.onUpdateCursor(this);
	}
}