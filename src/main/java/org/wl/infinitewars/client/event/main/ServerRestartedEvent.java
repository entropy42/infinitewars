package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.gameplay.GameInfoInitData;

public class ServerRestartedEvent extends Event<ServerRestartedHandler> {
	
	public static final EventType<ServerRestartedHandler> TYPE = new EventType<>();
	
	private final GameInfoInitData data;
	
	public ServerRestartedEvent(GameInfoInitData data) {
		this.data = data;
	}
	
	@Override
	public EventType<ServerRestartedHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ServerRestartedHandler handler) {
		handler.onServerRestarted(this);
	}
	
	public GameInfoInitData getData() {
		return data;
	}
}