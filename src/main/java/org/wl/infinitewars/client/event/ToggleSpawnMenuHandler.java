package org.wl.infinitewars.client.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ToggleSpawnMenuHandler extends EventHandler {
	
	void onChange(ToggleSpawnMenuEvent event);
}