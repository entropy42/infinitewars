package org.wl.infinitewars.client.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ToggleConsoleHandler extends EventHandler {
	
	void onToggleConsole(ToggleConsoleEvent event);
}