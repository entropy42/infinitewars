package org.wl.infinitewars.client.event.config;

import org.wl.infinitewars.shared.event.EventHandler;

public interface UpdateDebugOptionHandler extends EventHandler {
	
	void onUpdate(UpdateDebugOptionEvent event);
}