package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface UsernameSelectedHandler extends EventHandler {
	
	void onUsernameSelected(UsernameSelectedEvent event);
}