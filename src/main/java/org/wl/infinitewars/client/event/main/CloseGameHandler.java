package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface CloseGameHandler extends EventHandler {
	
	void onCloseGame(CloseGameEvent event);
}