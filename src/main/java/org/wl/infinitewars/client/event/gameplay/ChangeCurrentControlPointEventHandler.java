package org.wl.infinitewars.client.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ChangeCurrentControlPointEventHandler extends EventHandler {

	void onChangeCurrentControlPoint(ChangeCurrentControlPointEvent event);
}