package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class ConnectToServerEvent extends Event<ConnectToServerHandler> {
	
	public static final EventType<ConnectToServerHandler> TYPE = new EventType<>();
	
	private final String serverAddress;
	private final int serverPort;
	
	public ConnectToServerEvent(String serverAddress, int serverPort) {
		this.serverAddress = serverAddress;
		this.serverPort = serverPort;
	}
	
	@Override
	public EventType<ConnectToServerHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ConnectToServerHandler handler) {
		handler.onConnect(this);
	}
	
	public String getServerAddress() {
		return serverAddress;
	}
	
	public int getServerPort() {
		return serverPort;
	}
}