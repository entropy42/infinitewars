package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.network.messages.toclient.gamestate.GameStateResponseMessage;

public class GameStateResponseEvent extends Event<GameStateResponseHandler> {
	
	public static final EventType<GameStateResponseHandler> TYPE = new EventType<>();
	
	private final GameStateResponseMessage message;
	
	public GameStateResponseEvent(GameStateResponseMessage message) {
		this.message = message;
	}
	
	@Override
	public EventType<GameStateResponseHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(GameStateResponseHandler handler) {
		handler.onGameStateResponse(this);
	}
	
	public GameStateResponseMessage getMessage() {
		return message;
	}
}