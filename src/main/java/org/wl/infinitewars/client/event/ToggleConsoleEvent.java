package org.wl.infinitewars.client.event;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class ToggleConsoleEvent extends Event<ToggleConsoleHandler> {
	
	public static final EventType<ToggleConsoleHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<ToggleConsoleHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ToggleConsoleHandler handler) {
		handler.onToggleConsole(this);
	}
}