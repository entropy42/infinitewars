package org.wl.infinitewars.client.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ToggleScoreBoardHandler extends EventHandler {
	
	void onToggleScoreBoard(ToggleScoreBoardEvent event);
}