package org.wl.infinitewars.client.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface SpectateNextPlayerHandler extends EventHandler {

	void onChange(SpectateNextPlayerEvent event);
}