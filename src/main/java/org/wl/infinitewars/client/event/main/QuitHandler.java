package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface QuitHandler extends EventHandler {
	
	void onQuit(QuitEvent e);
}