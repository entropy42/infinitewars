package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface GameStateResponseHandler extends EventHandler {
	
	void onGameStateResponse(GameStateResponseEvent event);
}