package org.wl.infinitewars.client.event.spawnmenu;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class BuyShipEvent extends Event<BuyShipHandler> {
	
	public static final EventType<BuyShipHandler> TYPE = new EventType<>();
	
	private final String shipName;
	
	public BuyShipEvent(String shipName) {
		this.shipName = shipName;
	}
	
	@Override
	public EventType<BuyShipHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(BuyShipHandler handler) {
		handler.onBuyShip(this);
	}
	
	public String getShipName() {
		return shipName;
	}
}