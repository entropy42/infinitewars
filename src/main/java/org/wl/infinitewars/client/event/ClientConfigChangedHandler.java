package org.wl.infinitewars.client.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ClientConfigChangedHandler extends EventHandler {
	
	void onChanged(ClientConfigChangedEvent event);
}