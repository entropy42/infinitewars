package org.wl.infinitewars.client.event.spawnmenu;

import org.wl.infinitewars.shared.event.EventHandler;

public interface WantsRespawntHandler extends EventHandler {
	
	void onWantsRespawn(WantsRespawnEvent event);
}