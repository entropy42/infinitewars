package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class CreateNewServerEvent extends Event<CreateNewServerEventHandler> {

	public static final EventType<CreateNewServerEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<CreateNewServerEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(CreateNewServerEventHandler handler) {
		handler.onCreateNewServer(this);
	}
}