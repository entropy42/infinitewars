package org.wl.infinitewars.client.event;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ToggleMainMenuHandler extends EventHandler {
	
	void onChange(ToggleMainMenuEvent event);
}