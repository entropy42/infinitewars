package org.wl.infinitewars.client.event.spawnmenu;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class WantsRespawnEvent extends Event<WantsRespawntHandler> {
	
	public static final EventType<WantsRespawntHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<WantsRespawntHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(WantsRespawntHandler handler) {
		handler.onWantsRespawn(this);
	}
}