package org.wl.infinitewars.client.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ServerRestartedHandler extends EventHandler {
	
	void onServerRestarted(ServerRestartedEvent event);
}