package org.wl.infinitewars.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.wl.infinitewars.shared.network.messages.toclient.gameplay.RemoveWeaponFireMessage;

import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.scene.Spatial;

public class RemoveWeaponFireMessageHandler extends ClientMessageHandler<RemoveWeaponFireMessage> {

	@Override
	public Class<RemoveWeaponFireMessage> getMessageClass() {
		return RemoveWeaponFireMessage.class;
	}

	@Override
	public void handleMessage(Client conn, RemoveWeaponFireMessage m) {
		long weaponFireId = m.getWeaponFireId();

		final Spatial weaponFire = clientSyncState.getEntity(weaponFireId);

		if (weaponFire != null) {
			final boolean exlode = m.isExplosion();
			final Vector3f lastLocation = m.getLastLocation();

			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					entityAttacher.removeEntity(weaponFire);

					if (exlode) {
						Spatial explosion = entityFactory.createExplosion();
						explosion.setLocalTranslation(lastLocation);
						entityAttacher.attachEntity(explosion);
					}

					return null;
				}
			});
		} else {
			// System.out.println("The weaponFire with id " + weaponFireId + " was not found!");
		}
	}
}