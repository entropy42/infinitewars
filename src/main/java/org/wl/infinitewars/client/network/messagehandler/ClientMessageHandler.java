package org.wl.infinitewars.client.network.messagehandler;

import org.wl.infinitewars.client.gameplay.ClientPlayersContainer;
import org.wl.infinitewars.client.network.ClientState;
import org.wl.infinitewars.client.network.ClientSyncState;
import org.wl.infinitewars.shared.network.AbstractMessageHandler;

import com.google.inject.Inject;
import com.jme3.network.Client;
import com.jme3.network.Message;

public abstract class ClientMessageHandler<M extends Message> extends AbstractMessageHandler<M, Client> {

	protected ClientState clientState;
	protected ClientPlayersContainer playersContainer;
	protected ClientSyncState clientSyncState;

	@Inject
	public void setClientState(ClientState clientState) {
		this.clientState = clientState;
	}

	@Inject
	public void setPlayersContainer(ClientPlayersContainer playersContainer) {
		this.playersContainer = playersContainer;
	}

	@Inject
	public void setClientSyncState(ClientSyncState clientSyncState) {
		this.clientSyncState = clientSyncState;
	}
}