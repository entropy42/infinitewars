package org.wl.infinitewars.client.network;

import java.io.IOException;

import org.wl.infinitewars.client.event.main.ConnectToServerEvent;

import com.jme3.app.state.AppState;
import com.jme3.network.AbstractMessage;
import com.jme3.network.Client;
import com.jme3.network.MessageListener;

public interface ClientState extends AppState {
	
	void disconnectFromServer();
	
	void connect(ConnectToServerEvent event) throws IOException;
	
	void addMessageListener(MessageListener<? super Client> listener, Class<?>... classes);
	
	void sendMessage(AbstractMessage message);
	
	void connectToLastServer() throws IOException;
}