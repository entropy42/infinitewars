package org.wl.infinitewars.client.network.messagehandler;

import org.wl.infinitewars.client.event.main.GameStateResponseEvent;
import org.wl.infinitewars.shared.network.messages.toclient.gamestate.GameStateResponseMessage;

import com.jme3.network.Client;

public class GameStateResponseMessageHandler extends ClientMessageHandler<GameStateResponseMessage> {

	@Override
	public Class<GameStateResponseMessage> getMessageClass() {
		return GameStateResponseMessage.class;
	}

	@Override
	public void handleMessage(Client conn, GameStateResponseMessage m) {
		eventBus.fireEvent(new GameStateResponseEvent(m));
	}
}