package org.wl.infinitewars.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.PlayerPropertyChangeMessage;

import com.jme3.network.Client;

public class PlayerPropertyChangeMessageHandler extends ClientMessageHandler<PlayerPropertyChangeMessage> {

	private static final Logger log = LoggerFactory.getLogger(PlayerPropertyChangeMessageHandler.class);

	@Override
	public Class<PlayerPropertyChangeMessage> getMessageClass() {
		return PlayerPropertyChangeMessage.class;
	}

	@Override
	public void handleMessage(Client conn, PlayerPropertyChangeMessage m) {
		long playerId = m.getPlayerId();
		final Player player = playersContainer.getPlayer(playerId);

		if (player != null) {
			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					player.setProperty(m.getPropertyName(), m.getValue());
					return null;
				}
			});
		} else {
			log.warn("The player with id " + playerId + " was not found!");
		}
	}
}