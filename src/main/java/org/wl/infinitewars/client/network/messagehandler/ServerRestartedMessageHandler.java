package org.wl.infinitewars.client.network.messagehandler;

import org.wl.infinitewars.client.event.main.LoadGameEvent;
import org.wl.infinitewars.shared.network.messages.toclient.main.ServerRestartedMessage;

import com.jme3.network.Client;

public class ServerRestartedMessageHandler extends ClientMessageHandler<ServerRestartedMessage> {

	@Override
	public Class<ServerRestartedMessage> getMessageClass() {
		return ServerRestartedMessage.class;
	}

	@Override
	public void handleMessage(Client source, ServerRestartedMessage m) {
		eventBus.fireEvent(new LoadGameEvent(m.getData()));
	}
}