package org.wl.infinitewars.client.network.messagehandler;

import org.wl.infinitewars.shared.event.config.DebugConfigChangedEvent;
import org.wl.infinitewars.shared.network.messages.toclient.config.DebugConfigChangedMessage;

import com.jme3.network.Client;

public class DebugConfigChangedMessageHandler extends ClientMessageHandler<DebugConfigChangedMessage> {

	@Override
	public Class<DebugConfigChangedMessage> getMessageClass() {
		return DebugConfigChangedMessage.class;
	}

	@Override
	public void handleMessage(Client source, DebugConfigChangedMessage m) {
		eventBus.fireEvent(new DebugConfigChangedEvent(m.getDebugCommandKey(), m.getArguments()));
	}
}