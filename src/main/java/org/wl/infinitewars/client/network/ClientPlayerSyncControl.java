package org.wl.infinitewars.client.network;

import org.wl.infinitewars.shared.control.DelayedControl;
import org.wl.infinitewars.shared.control.MovementInfo;
import org.wl.infinitewars.shared.network.messages.toserver.input.MoveShipMessage;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

public class ClientPlayerSyncControl extends DelayedControl {

	private final ClientState clientState;

	private MovementInfo moveInfo;

	public ClientPlayerSyncControl(ClientState clientState) {
		super(1f / 60f);
		this.clientState = clientState;
	}

	@Override
	protected void onUpdate(float elapsedTime) {
		if (spatial != null && spatial.getParent() != null && moveInfo != null) {
			MoveShipMessage message = new MoveShipMessage(moveInfo);
			clientState.sendMessage(message);
		}
	}

	public void setMoveInfo(MovementInfo moveInfo) {
		this.moveInfo = moveInfo;
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}