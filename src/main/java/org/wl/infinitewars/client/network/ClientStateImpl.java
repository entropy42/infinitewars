package org.wl.infinitewars.client.network;

import java.io.IOException;

import org.wl.infinitewars.client.event.gameplay.SpectatedPlayerChangedEvent;
import org.wl.infinitewars.client.event.gameplay.SpectatedPlayerChangedHandler;
import org.wl.infinitewars.client.event.main.ConnectToServerEvent;
import org.wl.infinitewars.client.event.main.ConnectToServerHandler;
import org.wl.infinitewars.client.event.spawnmenu.BuyShipEvent;
import org.wl.infinitewars.client.event.spawnmenu.BuyShipHandler;
import org.wl.infinitewars.client.event.spawnmenu.ChangeSpawnLocationEvent;
import org.wl.infinitewars.client.event.spawnmenu.ChangeSpawnLocationEventHandler;
import org.wl.infinitewars.client.event.spawnmenu.WantsRespawnEvent;
import org.wl.infinitewars.client.event.spawnmenu.WantsRespawntHandler;
import org.wl.infinitewars.shared.NodeConstants;
import org.wl.infinitewars.shared.network.messages.MessageRegistry;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.ChangePlayerSpawnLocationRequestMessage;
import org.wl.infinitewars.shared.network.messages.toserver.gameplay.BuyShipRequestMessage;
import org.wl.infinitewars.shared.network.messages.toserver.gameplay.ChangeSpectatedPlayerMessage;
import org.wl.infinitewars.shared.network.messages.toserver.gameplay.WantsRespawnMessage;
import org.wl.infinitewars.shared.state.AppStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.network.AbstractMessage;
import com.jme3.network.Client;
import com.jme3.network.ClientStateListener;
import com.jme3.network.MessageListener;
import com.jme3.network.NetworkClient;
import com.jme3.scene.Node;

@Singleton
public class ClientStateImpl extends AppStateImpl implements ClientState, ClientStateListener {

	private final ClientFactory clientFactory;

	private NetworkClient client;

	private ConnectToServerEvent lastConnectToServerEvent;

	@Inject
	public ClientStateImpl(ClientFactory clientFactoy, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(appRootNode, appGuiNode);
		this.clientFactory = clientFactoy;

		MessageRegistry.registerMessages();

		initClient();
	}

	private void initClient() {
		client = clientFactory.createClient();
		client.addClientStateListener(new ClientStateListenerImpl());

		initEventBus();
	}

	private void initEventBus() {
		addEventHandler(ConnectToServerEvent.TYPE, new ConnectToServerHandler() {
			@Override
			public void onConnect(ConnectToServerEvent event) {
				try {
					connect(event);
				} catch (IOException e) {
					e.printStackTrace();
					// TODO
				}
			}
		});

		addEventHandler(BuyShipEvent.TYPE, new BuyShipHandler() {
			@Override
			public void onBuyShip(BuyShipEvent event) {
				client.send(new BuyShipRequestMessage(event.getShipName()));
			}
		});

		addEventHandler(WantsRespawnEvent.TYPE, new WantsRespawntHandler() {
			@Override
			public void onWantsRespawn(WantsRespawnEvent event) {
				client.send(new WantsRespawnMessage());
			}
		});

		addEventHandler(ChangeSpawnLocationEvent.TYPE, new ChangeSpawnLocationEventHandler() {
			@Override
			public void onChangeSpawnLocation(ChangeSpawnLocationEvent event) {
				client.send(new ChangePlayerSpawnLocationRequestMessage(event.getRequestedSpawnLocation()));
			}
		});

		addEventHandler(SpectatedPlayerChangedEvent.TYPE, new SpectatedPlayerChangedHandler() {
			@Override
			public void onChange(SpectatedPlayerChangedEvent event) {
				client.send(new ChangeSpectatedPlayerMessage(event.getPlayer().getPlayerId()));
			}
		});
	}

	@Override
	public void clientConnected(Client c) {

	}

	@Override
	public void clientDisconnected(Client c, DisconnectInfo info) {

	}

	@Override
	public void disconnectFromServer() {
		if (client.isConnected()) {
			client.close();
		}
	}

	@Override
	public void connectToLastServer() throws IOException {
		connect(lastConnectToServerEvent);
	}

	@Override
	public void connect(ConnectToServerEvent event) throws IOException {
		lastConnectToServerEvent = event;

		String serverAddress = event.getServerAddress();
		int serverPort = event.getServerPort();
		client.connectToServer(serverAddress, serverPort, 6543);
		client.start();
	}

	@Override
	public void addMessageListener(MessageListener<? super Client> listener, Class<?>... classes) {
		if (classes == null || classes.length == 0) {
			client.addMessageListener(listener);
		} else {
			client.addMessageListener(listener, classes);
		}
	}

	@Override
	public void sendMessage(AbstractMessage message) {
		client.send(message);
	}

	@Override
	public void cleanup() {
		super.cleanup();

		disconnectFromServer();

		initClient();
	}
}