package org.wl.infinitewars.client.network.messagehandler;

import org.wl.infinitewars.client.event.main.LoadGameEvent;
import org.wl.infinitewars.shared.network.messages.toclient.main.GameInfoInitMessage;

import com.jme3.network.Client;

public class GameInfoInitMessageHandler extends ClientMessageHandler<GameInfoInitMessage> {

	@Override
	public Class<GameInfoInitMessage> getMessageClass() {
		return GameInfoInitMessage.class;
	}

	@Override
	public void handleMessage(Client conn, GameInfoInitMessage m) {
		eventBus.fireEvent(new LoadGameEvent(m.getData()));
	}
}