package org.wl.infinitewars.client.network;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.client.gameplay.ClientPlayersContainer;
import org.wl.infinitewars.shared.loading.EnqueueHelper;
import org.wl.infinitewars.shared.network.messages.toclient.sync.AbstractSyncMessage;
import org.wl.infinitewars.shared.network.messages.toclient.sync.ShipSyncMessage;
import org.wl.infinitewars.shared.network.messages.toclient.sync.WeaponFireSyncMessage;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.scene.Spatial;

@Singleton
public class ClientSyncState extends AbstractSyncState implements MessageListener<Client> {

	private static final Logger log = LoggerFactory.getLogger(ClientSyncState.class);

	private final LinkedList<AbstractSyncMessage> messageQueue = new LinkedList<>();

	private final ClientState clientState;
	private final ClientPlayersContainer playersContainer;

	@Inject
	public ClientSyncState(EnqueueHelper enqueueHelper, ClientState clientState, ClientPlayersContainer playersContainer) {
		super(enqueueHelper, 16);
		this.clientState = clientState;
		this.playersContainer = playersContainer;
	}

	@Override
	public void messageReceived(Client source, final Message message) {
		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				enqueueMessage((AbstractSyncMessage)message);
				return null;
			}
		});
	}

	protected void enqueueMessage(AbstractSyncMessage message) {
		messageQueue.add(message);
	}

	@Override
	public void addEntity(Spatial entity) {
		super.addEntity(entity);

		entity.addControl(new EntitySyncControl());
	}

	@Override
	protected void onUpdate(float tpf) {
		applyMessages();
	}

	protected void applyMessage(AbstractSyncMessage message) {
		Spatial entity = syncEntities.get(message.getEntityId());

		if (entity != null) {
			if (message instanceof ShipSyncMessage) {
				syncPhysicsEntity((ShipSyncMessage)message, entity);
			} else if (message instanceof WeaponFireSyncMessage) {
				syncWeaponFire((WeaponFireSyncMessage)message, entity);
			} else {
				log.error("wtf?!");
			}
		} else {
			// log.info("Cannot find physics object for entity with id " + message.getEntityId());
		}
	}

	protected void applyMessages() {
		Iterator<AbstractSyncMessage> iter = messageQueue.iterator();

		while (iter.hasNext()) {
			AbstractSyncMessage message = iter.next();

			applyMessage(message);
			iter.remove();
		}
	}

	protected void syncPhysicsEntity(ShipSyncMessage message, Spatial entity) {
		EntitySyncControl control = entity.getControl(EntitySyncControl.class);
		control.setSyncMessage(message);
	}

	private void syncWeaponFire(WeaponFireSyncMessage message, Spatial entity) {
		Vector3f location = message.getLocation();
		entity.getControl(RigidBodyControl.class).setPhysicsLocation(location);
	}

	@Override
	public void cleanup() {
		super.cleanup();

		messageQueue.clear();
	}
}