package org.wl.infinitewars.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.wl.infinitewars.shared.event.main.EndGameTimeUpdateEvent;
import org.wl.infinitewars.shared.network.messages.toclient.main.EndTimerUpdateMessage;

import com.jme3.network.Client;

public class EndTimerUpdateMessageHandler extends ClientMessageHandler<EndTimerUpdateMessage> {

	@Override
	public Class<EndTimerUpdateMessage> getMessageClass() {
		return EndTimerUpdateMessage.class;
	}

	@Override
	public void handleMessage(Client conn, EndTimerUpdateMessage m) {
		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				eventBus.fireEvent(new EndGameTimeUpdateEvent(m.getSecondsLeft()));
				return null;
			}
		});
	}
}