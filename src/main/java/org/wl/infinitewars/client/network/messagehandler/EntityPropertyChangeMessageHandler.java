package org.wl.infinitewars.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityHelper;
import org.wl.infinitewars.shared.event.gameplay.DestroyShipEvent;
import org.wl.infinitewars.shared.gameplay.EntityPropertyCauseSync;
import org.wl.infinitewars.shared.gameplay.EntityPropertyChangeCause;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.EntityPropertyChangeMessage;

import com.jme3.network.Client;
import com.jme3.scene.Spatial;

public class EntityPropertyChangeMessageHandler extends ClientMessageHandler<EntityPropertyChangeMessage> {

	@Override
	public Class<EntityPropertyChangeMessage> getMessageClass() {
		return EntityPropertyChangeMessage.class;
	}

	@Override
	public void handleMessage(Client conn, EntityPropertyChangeMessage m) {
		long entityId = m.getEntityId();
		String propertyName = m.getPropertyName();
		Object propertyValue = m.getPropertyValue();
		EntityPropertyCauseSync cause = m.getCause();

		final Spatial entity = clientSyncState.getEntity(entityId);

		if (entity != null) {
			EntityPropertyChangeCause c = null;

			if (cause != null) {
				c = cause.createCollisionCause(clientSyncState);
			}

			EntityHelper.changeProperty(entity, propertyName, propertyValue, c);

			if (propertyName.equals(EntityConstants.hitPoints)) {
				int currentHitPoints = (int)propertyValue;

				if (currentHitPoints <= 0) {
					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							eventBus.fireEvent(new DestroyShipEvent(entity));
							return null;
						}
					});

					Player player = entity.getUserData(EntityConstants.player);

					if (player != null) {
						player.setWantsRespawn(player.isBot());
						player.setAlive(false);

						if (player.isLocalPlayer()) {

						}
					}
				}
			}
		} else {
			// log.warn("The entity with id " + entityId + " was not found!");
		}
	}
}