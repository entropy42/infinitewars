package org.wl.infinitewars.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.wl.infinitewars.shared.event.main.RestartGameEvent;
import org.wl.infinitewars.shared.network.messages.toclient.RestartGameMessage;

import com.jme3.network.Client;

public class RestartGameMessageHandler extends ClientMessageHandler<RestartGameMessage> {

	@Override
	public Class<RestartGameMessage> getMessageClass() {
		return RestartGameMessage.class;
	}

	@Override
	public void handleMessage(Client source, RestartGameMessage m) {
		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				eventBus.fireEvent(new RestartGameEvent());
				return null;
			}
		});
	}
}