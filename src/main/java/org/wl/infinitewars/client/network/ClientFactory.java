package org.wl.infinitewars.client.network;

import com.jme3.network.NetworkClient;

public interface ClientFactory {
	
	NetworkClient createClient();
}