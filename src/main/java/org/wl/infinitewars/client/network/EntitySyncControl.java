package org.wl.infinitewars.client.network;

import org.wl.infinitewars.shared.control.MoveControl;
import org.wl.infinitewars.shared.control.MovementInfo;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.network.messages.toclient.sync.ShipSyncMessage;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class EntitySyncControl extends AbstractControl {

	private RigidBodyControl bodyControl;
	private ShipSyncMessage message;

	// TODO smooth
	@Override
	protected void controlUpdate(float tpf) {
		if (message != null) {
			// tpf *= 60f;

			Player player = spatial.getUserData(EntityConstants.player);

			if (player != null) {
				// Player player = spatial.getUserData(EntityConstants.PLAYER);
				//
				// if (player.getName().equals("Bot 0 (HUMANS)")) {
				// System.out.println(bodyControl.getPhysicsLocation().distance(message.getLocation()));
				// }
				//
				// bodyControl.setPhysicsLocation(message.getLocation());
				// bodyControl.setPhysicsRotation(message.getRotation());

				Vector3f targetLocation = message.getLocation();
				Quaternion targetRotation = message.getRotation();
				Vector3f angularVelocity = message.getAngularVelocity();
				Vector3f linearVelocity = message.getLinearVelocity();

				// float maxInterpolationDifference = 2f;
				//
				// Vector3f currentLocation = new Vector3f(bodyControl.getPhysicsLocation());
				//
				// float locationDistance = currentLocation.distance(targetLocation);
				//
				// if (locationDistance > maxInterpolationDifference) {
				bodyControl.setPhysicsLocation(targetLocation);
				// } else {
				// currentLocation.interpolateLocal(targetLocation, tpf);
				// bodyControl.setPhysicsLocation(currentLocation);
				// }

				// Quaternion currentRotation = new Quaternion(bodyControl.getPhysicsRotation());
				// Vector3f clientViewVector = currentRotation.mult(Vector3f.UNIT_XYZ).normalize();
				// Vector3f serverViewVector = targetRotation.mult(Vector3f.UNIT_XYZ).normalize();

				// float viewAngleDifference = FastMath.abs(clientViewVector.angleBetween(serverViewVector));

				// if (viewAngleDifference > maxInterpolationDifference) {
				bodyControl.setPhysicsRotation(targetRotation);
				// } else {
				// currentRotation.slerp(targetRotation, tpf);
				// bodyControl.setPhysicsRotation(currentRotation);
				// }

				if (!player.isLocalPlayer()) {
					bodyControl.setLinearVelocity(linearVelocity);
					bodyControl.setAngularVelocity(angularVelocity);

					MovementInfo moveInfo = message.getMoveInfo();

					if (moveInfo != null) {
						MoveControl moveControl = spatial.getControl(MoveControl.class);

						if (moveControl != null) {
							moveControl.setMoveInfo(moveInfo);
						}
					}
				}
			}

			message = null;
		}
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);
		bodyControl = spatial.getControl(RigidBodyControl.class);
	}

	public void setSyncMessage(ShipSyncMessage message) {
		this.message = message;
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}