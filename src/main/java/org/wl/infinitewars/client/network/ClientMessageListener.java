package org.wl.infinitewars.client.network;

import org.wl.infinitewars.client.network.messagehandler.ClientMessageHandler;
import org.wl.infinitewars.shared.network.AbstractMessageListener;

import com.jme3.network.Client;

@SuppressWarnings("rawtypes")
public class ClientMessageListener extends AbstractMessageListener<Client, ClientMessageHandler> {
}