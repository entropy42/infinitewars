package org.wl.infinitewars.client.gameplay.mode;

import java.util.Map;

import org.wl.infinitewars.shared.entity.EntityAttacher;
import org.wl.infinitewars.shared.entity.EntityFactory;
import org.wl.infinitewars.shared.gameplay.GameInfo;
import org.wl.infinitewars.shared.gameplay.TeamsContainer;
import org.wl.infinitewars.shared.gameplay.mode.MotherShipLoadingStepFactory;
import org.wl.infinitewars.shared.json.Faction;
import org.wl.infinitewars.shared.json.FactionConfig;
import org.wl.infinitewars.shared.json.MapData;

public class ClientMotherShipLoadingStepFactory extends MotherShipLoadingStepFactory {

	public ClientMotherShipLoadingStepFactory(MapData mapData, EntityFactory entityFactory, TeamsContainer gameDataContainer, Map<Faction, FactionConfig> factionConfigs, EntityAttacher entityAttacher,
			GameInfo gameInfo) {
		super(mapData, entityFactory, gameDataContainer, factionConfigs, entityAttacher, gameInfo);
	}
}