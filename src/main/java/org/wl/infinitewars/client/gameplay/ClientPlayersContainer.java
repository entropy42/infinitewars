package org.wl.infinitewars.client.gameplay;

import java.util.HashMap;
import java.util.Map;

import org.wl.infinitewars.shared.gameplay.player.Player;

import com.google.inject.Singleton;

@Singleton
public class ClientPlayersContainer {

	private final Map<Long, Player> players = new HashMap<>();

	public void addPlayer(Player player) {
		players.put(player.getPlayerId(), player);
	}

	public Player getPlayer(long playerId) {
		return players.get(playerId);
	}

	public void clear() {
		players.clear();
	}
}