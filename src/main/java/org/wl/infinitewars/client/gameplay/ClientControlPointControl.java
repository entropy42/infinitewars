package org.wl.infinitewars.client.gameplay;

import org.wl.infinitewars.client.event.gameplay.ChangeCurrentControlPointEvent;
import org.wl.infinitewars.shared.control.DelayedControl;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.gameplay.TeamsContainer;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.Team;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;

public class ClientControlPointControl extends DelayedControl {

	private final TeamsContainer teamsContainer;

	private Player localPlayer;
	private int captureRadius;
	private boolean isCurrentControlPoint;

	public ClientControlPointControl(TeamsContainer teamsContainer) {
		super(1 / 30f);
		this.teamsContainer = teamsContainer;
	}

	@Override
	protected void onUpdate(float elapsedTime) {
		if (localPlayer == null) {
			localPlayer = findLocalPlayer(teamsContainer.getTeam1());

			if (localPlayer == null) {
				localPlayer = findLocalPlayer(teamsContainer.getTeam2());
			}
		}

		if (localPlayer != null && localPlayer.isAlive() && localPlayer.getShip() != null) {
			Vector3f pos = localPlayer.getShip().getLocalTranslation();

			float dist = pos.distance(spatial.getLocalTranslation());

			if (dist <= captureRadius) {
				isCurrentControlPoint = true;
				eventBus.fireEvent(new ChangeCurrentControlPointEvent(spatial));
			} else if (isCurrentControlPoint) {
				isCurrentControlPoint = false;
				eventBus.fireEvent(new ChangeCurrentControlPointEvent(null));
			}
		}
	}

	private Player findLocalPlayer(Team team) {
		for (Player player : team.getPlayers()) {
			if (player.isLocalPlayer()) {
				return player;
			}
		}

		return null;
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);

		captureRadius = spatial.getUserData(EntityConstants.controlPointCaptureRadius);
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}