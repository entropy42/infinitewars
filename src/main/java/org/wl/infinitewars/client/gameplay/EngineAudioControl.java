package org.wl.infinitewars.client.gameplay;

import org.wl.infinitewars.shared.control.MoveControl;
import org.wl.infinitewars.shared.control.MovementInfo;

import com.jme3.audio.AudioNode;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class EngineAudioControl extends AbstractControl {

	private final AudioNode audioNode;

	private float currentPitch;

	public EngineAudioControl(AudioNode audioNode) {
		this.audioNode = audioNode;

		currentPitch = 0.5f;
	}

	@Override
	protected void controlUpdate(float tpf) {
		MoveControl moveControl = spatial.getControl(MoveControl.class);

		if (moveControl != null) {
			MovementInfo moveInfo = moveControl.getMoveInfo();

			if (moveInfo != null) {
				float forwardSpeedPercentage = moveInfo.getForwardSpeedPercentage();

				float pitch = 0.5f + (forwardSpeedPercentage / 2f);

				if (currentPitch != pitch) {
					audioNode.setPitch(pitch);
					currentPitch = pitch;
				}
			}
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}