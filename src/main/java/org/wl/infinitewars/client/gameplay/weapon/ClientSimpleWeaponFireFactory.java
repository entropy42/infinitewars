package org.wl.infinitewars.client.gameplay.weapon;

import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireCreationData;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireFactory;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFirePhysics;
import org.wl.infinitewars.shared.util.FileConstants;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioData.DataType;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class ClientSimpleWeaponFireFactory extends WeaponFireFactory {

	public ClientSimpleWeaponFireFactory(String prefix, AssetManager assetManager, WeaponFirePhysics physics, String weaponFireType) {
		super(prefix, assetManager, physics, weaponFireType);
	}

	protected ColorRGBA getColor() {
		return new ColorRGBA(0.1f, 0.4f, 1f, 1.0f);
	}

	protected String getAudioFileName() {
		return "assets/sounds/standardWeaponFire.ogg";
	}

	@Override
	public Node createWeaponFire(WeaponFireCreationData data) throws Exception {
		Spatial firingShip = data.getFiringShip();

		Node weaponFire = super.createWeaponFire(data);

		ParticleEmitter particleEmitter = new ParticleEmitter("Weapon fire particle emitter", Type.Triangle, 50);
		weaponFire.attachChild(particleEmitter);
		particleEmitter.setImagesX(4);
		particleEmitter.setImagesY(4);
		particleEmitter.getParticleInfluencer().setInitialVelocity(Vector3f.ZERO);
		particleEmitter.getParticleInfluencer().setVelocityVariation(0.0f);
		particleEmitter.setGravity(0, 0, 0);
		particleEmitter.setParticlesPerSec(100f);

		float size = getSize(firingShip);

		Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
		material.setTexture("Texture", assetManager.loadTexture(FileConstants.TEXTURES_PATH + "standardWeaponFire.png"));
		particleEmitter.setMaterial(material);
		particleEmitter.setStartColor(getColor());
		particleEmitter.setEndColor(getColor());
		particleEmitter.setStartSize(size);
		particleEmitter.setEndSize(size);
		particleEmitter.setLowLife(0.05f);
		particleEmitter.setHighLife(0.05f);

		if (showPaths()) {
			particleEmitter.setHighLife(99999f);
			particleEmitter.setLowLife(99999f);
		}

		AudioNode audioNode = new AudioNode(assetManager, getAudioFileName(), DataType.Buffer);
		audioNode.setName(EntityConstants.weaponFireAudioNode);
		audioNode.setPositional(true);
		data.getFiringShip().attachChild(audioNode);

		return weaponFire;
	}
}