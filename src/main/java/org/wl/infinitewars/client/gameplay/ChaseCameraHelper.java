package org.wl.infinitewars.client.gameplay;

import org.wl.infinitewars.shared.control.RotationConstants;
import org.wl.infinitewars.shared.entity.EntityConstants;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingSphere;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;

public class ChaseCameraHelper {

	public static void setupChaseCamera(Node ship, CameraNode cameraNode, AssetManager assetManager) {
		BoundingSphere sphere = (BoundingSphere)ship.getWorldBound();
		cameraNode.setLocalTranslation(new Vector3f(0f, 0.6f, -4f).mult(sphere.getRadius()));

		ship.addControl(new CameraMovementControl(cameraNode));
		ship.addControl(new SpaceDustControl(assetManager));
		ship.attachChild(cameraNode);

		AudioNode audioNode = (AudioNode)ship.getChild(EntityConstants.shipEngineAudioNode);

		if (audioNode != null) {
			audioNode.setPositional(false);
			ship.addControl(new EngineAudioControl(audioNode));
		}

		cameraNode.rotateUpTo(Vector3f.UNIT_Y);
		cameraNode.lookAt(ship.getLocalTranslation().add(0f, 2f, 0f), ship.getLocalRotation().getRotationColumn(RotationConstants.UP));
	}
}