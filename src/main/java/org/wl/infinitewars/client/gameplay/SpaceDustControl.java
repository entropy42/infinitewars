package org.wl.infinitewars.client.gameplay;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import org.wl.infinitewars.shared.control.MoveControl;
import org.wl.infinitewars.shared.control.MovementInfo;
import org.wl.infinitewars.shared.util.FileConstants;

import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class SpaceDustControl extends AbstractControl {

	private final Random r = new Random();

	private final Collection<ParticleEmitter> particleEmitters;
	private MoveControl moveControl;
	private float moveTimer;

	public SpaceDustControl(AssetManager assetManager) {
		int count = 12;
		particleEmitters = new ArrayList<>(count);

		for (int i = 0; i < count; i++) {
			ParticleEmitter p = new ParticleEmitter("Space dust particle emitter", Type.Triangle, 60);
			p.setImagesX(2);
			p.setImagesY(2);
			p.getParticleInfluencer().setInitialVelocity(Vector3f.ZERO);
			p.getParticleInfluencer().setVelocityVariation(0.0f);
			p.setGravity(0, 0, 0);
			p.setParticlesPerSec(2.5f);

			Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
			material.setTexture("Texture", assetManager.loadTexture(FileConstants.TEXTURES_PATH + "spaceDust.png"));
			p.setMaterial(material);
			p.setStartColor(ColorRGBA.White);
			p.setEndColor(ColorRGBA.White);
			p.setStartSize(0.05f);
			p.setEndSize(0.1f);
			p.setLowLife(3f);
			p.setHighLife(3f);

			particleEmitters.add(p);
		}
	}

	@Override
	protected void controlUpdate(float tpf) {
		moveTimer += tpf;

		if (moveTimer >= 0.5f) {
			int variation = 30;
			moveTimer = 0f;

			particleEmitters.forEach(p -> {
				int x = (r.nextBoolean() ? 1 : -1) * r.nextInt(variation);
				int y = (r.nextBoolean() ? 1 : -1) * r.nextInt(variation);
				p.setLocalTranslation(x, y, 100f);
			});
		}

		MovementInfo moveInfo = moveControl.getMoveInfo();
		float speed = moveInfo.getForwardSpeedPercentage();

		float minSpeed = 0.001f;

		if (speed < minSpeed) {
			speed = moveInfo.isStrafeLeft() || moveInfo.isStrafeRight() ? 0.2f : speed;
		}

		float maxLife = 20f;

		for (ParticleEmitter p : particleEmitters) {
			p.setEnabled(speed >= 0.001f);
			float life = maxLife - (speed * maxLife) + 3f;
			p.setLowLife(life);
			p.setHighLife(life);
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);

		if (spatial != null) {
			Node node = (Node)spatial;
			particleEmitters.forEach(p -> node.attachChild(p));
			moveControl = node.getControl(MoveControl.class);
		} else {
			particleEmitters.forEach(p -> p.removeFromParent());
		}
	}
}