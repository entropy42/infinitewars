package org.wl.infinitewars.client.gameplay.weapon;

import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.gameplay.weapon.SimpleSpecialWeaponFirePhysics;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;

public class ClientStandardWeaponFireFactory extends ClientSimpleWeaponFireFactory {

	@Inject
	public ClientStandardWeaponFireFactory(AssetManager assetManager) {
		super(EntityConstants.standard_weaponFirePrefix, assetManager, new SimpleSpecialWeaponFirePhysics(EntityConstants.standard_weaponFirePrefix), EntityConstants.standard_weaponFirePrefix);
	}
}