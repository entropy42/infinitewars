package org.wl.infinitewars.client.loading;

import java.util.List;

import org.wl.infinitewars.client.gameplay.mode.ClientMotherShipLoadingStepFactory;
import org.wl.infinitewars.shared.loading.GameModeLoadingStepFactory;
import org.wl.infinitewars.shared.loading.LoadingStep;
import org.wl.infinitewars.shared.loading.LoadingStepBuildRequest;
import org.wl.infinitewars.shared.loading.LoadingStepsBuilder;

import com.google.inject.Singleton;

@Singleton
public class ClientLoadingStepsBuilder extends LoadingStepsBuilder {

	@Override
	protected void addSteps(List<LoadingStep> steps, LoadingStepBuildRequest request) {
		steps.add(new LoadingStep(40f, true) {
			@Override
			public void load() throws Exception {
				initSkyBox(request);
			}
		});

		steps.add(new LoadingStep(10f, true) {
			@Override
			public void load() throws Exception {
				initCamera(request);
			}
		});

		steps.add(new LoadingStep(10f, true) {
			@Override
			public void load() throws Exception {
				initLights(request);
			}
		});
	}

	@Override
	protected GameModeLoadingStepFactory createMotherShipLoadingStepFactory(LoadingStepBuildRequest request) {
		return new ClientMotherShipLoadingStepFactory(request.getMapData(), entityFactory, teamsContainer, factionConfigs, entityAttacher, gameInfoProvider.getGameInfo());
	}
}