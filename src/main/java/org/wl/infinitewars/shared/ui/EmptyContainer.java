package org.wl.infinitewars.shared.ui;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;

import tonegod.gui.core.Element;
import tonegod.gui.core.ElementManager;
import tonegod.gui.core.utils.UIDUtil;

public class EmptyContainer extends Element {
	
	public EmptyContainer(ElementManager screen) {
		super(screen, UIDUtil.getUID(), new Vector2f(), new Vector2f(), new Vector4f(), null);
		
		setIgnoreMouse(true);
	}
}