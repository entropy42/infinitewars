package org.wl.infinitewars.shared.ui;

import tonegod.gui.controls.windows.Panel;
import tonegod.gui.core.ElementManager;

import com.jme3.math.Vector2f;

public class Container extends Panel {
	
	protected final float screenWidth;
	protected final float screenHeight;
	
	public Container(ElementManager screen) {
		super(screen, Vector2f.ZERO);
		
		screenWidth = screen.getWidth();
		screenHeight = screen.getHeight();
		
		setIgnoreMouse(true);
	}
}