package org.wl.infinitewars.shared.ui;

import org.wl.infinitewars.shared.event.config.UpdateGraphicsConfigEvent;
import org.wl.infinitewars.shared.event.config.UpdateGraphicsConfigHandler;
import org.wl.infinitewars.shared.state.AppStateImpl;

import com.jme3.scene.Node;

import tonegod.gui.core.Screen;

public abstract class GUIStateImpl extends AppStateImpl implements GUIState {

	protected final Screen screen;

	protected EmptyContainer mainContent;

	public GUIStateImpl(Screen screen, Node appRootNode, Node appGuiNode) {
		super(appRootNode, appGuiNode);
		this.screen = screen;

		mainContent = new EmptyContainer(screen);
		setEnabled(false);

		addEventHandler(UpdateGraphicsConfigEvent.TYPE, new UpdateGraphicsConfigHandler() {
			@Override
			public void onUpdate(UpdateGraphicsConfigEvent event) {
				reinit();
			}
		});
	}

	public abstract void init();

	protected void reinit() {
		mainContent.detachAllChildren();
		mainContent.setX(0f);

		init();
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (enabled && !isEnabled()) {
			screen.addElement(mainContent);
		} else if (!enabled && isEnabled()) {
			screen.removeElement(mainContent);
		}

		super.setEnabled(enabled);
	}

	@Override
	public void cleanup() {
		super.cleanup();

		mainContent.detachAllChildren();
		mainContent.removeFromParent();
	}
}