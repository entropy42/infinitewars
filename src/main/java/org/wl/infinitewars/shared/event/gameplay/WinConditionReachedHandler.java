package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface WinConditionReachedHandler extends EventHandler {
	
	void onWinConditionReached(WinConditionReachedEvent event);
}