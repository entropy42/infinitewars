package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface DestroyShipHandler extends EventHandler {
	
	void onDestroyShip(DestroyShipEvent event);
}