package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface PlayerPropertyChangeHandler extends EventHandler {
	
	void onChange(PlayerPropertyChangeEvent event);
}