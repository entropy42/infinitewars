package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface EntityPropertyChangeHandler extends EventHandler {
	
	void onChange(EntityPropertyChangeEvent event);
}