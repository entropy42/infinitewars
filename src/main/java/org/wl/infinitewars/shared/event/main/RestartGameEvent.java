package org.wl.infinitewars.shared.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class RestartGameEvent extends Event<RestartGameHandler> {
	
	public static final EventType<RestartGameHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<RestartGameHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(RestartGameHandler handler) {
		handler.onRestartGame(this);
	}
}