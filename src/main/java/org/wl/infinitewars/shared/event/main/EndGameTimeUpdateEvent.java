package org.wl.infinitewars.shared.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class EndGameTimeUpdateEvent extends Event<EndGameTimeUpdateHandler> {
	
	public static final EventType<EndGameTimeUpdateHandler> TYPE = new EventType<>();
	
	private final int secondsLeft;
	
	public EndGameTimeUpdateEvent(int secondsLeft) {
		this.secondsLeft = secondsLeft;
	}
	
	@Override
	public EventType<EndGameTimeUpdateHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(EndGameTimeUpdateHandler handler) {
		handler.onUpdateGameTime(this);
	}
	
	public int getSecondsLeft() {
		return secondsLeft;
	}
}