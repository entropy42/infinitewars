package org.wl.infinitewars.shared.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class GameTimeUpdateEvent extends Event<GameTimeUpdateHandler> {
	
	public static final EventType<GameTimeUpdateHandler> TYPE = new EventType<>();
	
	private final float currentTime;
	
	public GameTimeUpdateEvent(float currentTime) {
		this.currentTime = currentTime;
	}
	
	@Override
	public EventType<GameTimeUpdateHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(GameTimeUpdateHandler handler) {
		handler.onGameTimeUpdate(this);
	}
	
	public float getCurrentTime() {
		return currentTime;
	}
}