package org.wl.infinitewars.shared.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface RestartGameHandler extends EventHandler {
	
	void onRestartGame(RestartGameEvent event);
}