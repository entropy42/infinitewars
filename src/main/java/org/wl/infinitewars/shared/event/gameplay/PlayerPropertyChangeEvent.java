package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.gameplay.player.Player;

public class PlayerPropertyChangeEvent extends Event<PlayerPropertyChangeHandler> {
	
	public static final EventType<PlayerPropertyChangeHandler> TYPE = new EventType<>();
	
	private final Player player;
	private final String propertyName;
	private final Object propertyValue;
	private final Object oldPropertyValue;
	
	public PlayerPropertyChangeEvent(Player player, String propertyName, Object propertyValue, Object oldPropertyValue) {
		this.player = player;
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
		this.oldPropertyValue = oldPropertyValue;
	}
	
	@Override
	public EventType<PlayerPropertyChangeHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(PlayerPropertyChangeHandler handler) {
		handler.onChange(this);
	}
	
	public String getPropertyName() {
		return propertyName;
	}
	
	public Object getPropertyValue() {
		return propertyValue;
	}
	
	public Object getOldPropertyValue() {
		return oldPropertyValue;
	}
	
	public Player getPlayer() {
		return player;
	}
}