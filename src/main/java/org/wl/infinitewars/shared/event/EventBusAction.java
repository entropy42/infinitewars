package org.wl.infinitewars.shared.event;

public interface EventBusAction {

	void perform();
}