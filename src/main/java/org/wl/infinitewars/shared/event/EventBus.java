package org.wl.infinitewars.shared.event;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.inject.Singleton;

@Singleton
public class EventBus {

	private static EventBus instance;

	protected final Map<EventType<?>, List<EventHandler>> registeredHandlers = new HashMap<>();
	protected final List<EventBusAction> actions = new LinkedList<>();

	protected final AtomicBoolean firingEvent = new AtomicBoolean(false);
	protected final AtomicBoolean performingActions = new AtomicBoolean(false);

	public static void init(EventBus eventBus) {
		instance = eventBus;
	}

	public static EventBus get() {
		if (instance == null) {
			instance = new EventBus();
		}

		return instance;
	}

	public <H extends EventHandler> void addHandler(final EventType<H> type, final H handler) {
		if (type == null) {
			throw new IllegalArgumentException("type must not be null!");
		}

		if (handler == null) {
			throw new IllegalArgumentException("handler must not be null!");
		}

		synchronized (firingEvent) {
			if (firingEvent.get()) {
				synchronized (performingActions) {
					actions.add(new EventBusAction() {
						@Override
						public void perform() {
							performAdd(type, handler);
						}
					});
				}
			} else {
				performAdd(type, handler);
			}
		}
	}

	protected <H extends EventHandler> void performAdd(EventType<H> type, H handler) {
		if (type == null) {
			throw new IllegalArgumentException("type must not be null!");
		}

		if (handler == null) {
			throw new IllegalArgumentException("handler must not be null!");
		}

		List<EventHandler> handlers = registeredHandlers.get(type);

		if (handlers == null) {
			handlers = new LinkedList<>();
			registeredHandlers.put(type, handlers);
		}

		handlers.add(handler);
	}

	@SuppressWarnings("unchecked")
	public <H extends EventHandler> void fireEvent(Event<H> event) {
		if (event == null) {
			throw new IllegalArgumentException("event must not be null!");
		}

		EventType<H> type = event.getType();
		List<EventHandler> handlers = registeredHandlers.get(type);

		if (handlers != null) {
			synchronized (firingEvent) {
				firingEvent.set(true);

				for (EventHandler handler : handlers) {
					event.callHandler((H)handler);
				}

				firingEvent.set(false);
			}
		}

		synchronized (performingActions) {
			performingActions.set(true);

			for (EventBusAction action : actions) {
				action.perform();
			}

			performingActions.set(false);
		}

		actions.clear();
	}

	public void clear() {
		registeredHandlers.clear();
		actions.clear();
		firingEvent.set(false);
	}

	public void removeHandler(final EventHandler handler, EventType<?> type) {
		final List<EventHandler> handlers = registeredHandlers.get(type);

		if (handlers != null) {
			synchronized (firingEvent) {
				if (firingEvent.get()) {
					synchronized (performingActions) {
						actions.add(new EventBusAction() {
							@Override
							public void perform() {
								handlers.remove(handler);
							}
						});
					}
				} else {
					handlers.remove(handler);
				}
			}
		}
	}
}