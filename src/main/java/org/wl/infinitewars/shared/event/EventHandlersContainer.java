package org.wl.infinitewars.shared.event;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class EventHandlersContainer {

	private final EventBus eventBus = EventBus.get();

	private final Map<EventType<?>, EventHandler> registeredEventHandlers = new HashMap<>();

	public <H extends EventHandler> void addHandler(EventType<H> eventType, H handler) {
		eventBus.addHandler(eventType, handler);
		registeredEventHandlers.put(eventType, handler);
	}

	public void cleanup() {
		for (Entry<EventType<?>, EventHandler> entry : registeredEventHandlers.entrySet()) {
			eventBus.removeHandler(entry.getValue(), entry.getKey());
		}
	}
}