package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

import com.jme3.scene.Spatial;

public class DestroyShipEvent extends Event<DestroyShipHandler> {
	
	public static final EventType<DestroyShipHandler> TYPE = new EventType<>();
	
	private final Spatial entity;
	
	public DestroyShipEvent(Spatial entity) {
		this.entity = entity;
	}
	
	@Override
	public EventType<DestroyShipHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(DestroyShipHandler handler) {
		handler.onDestroyShip(this);
	}
	
	public Spatial getEntity() {
		return entity;
	}
}