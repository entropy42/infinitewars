package org.wl.infinitewars.shared.event.config;

import org.wl.infinitewars.shared.event.EventHandler;

public interface DebugConfigChangedHandler extends EventHandler {
	
	void onChanged(DebugConfigChangedEvent event);
}