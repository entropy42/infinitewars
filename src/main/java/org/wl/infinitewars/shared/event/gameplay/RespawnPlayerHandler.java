package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface RespawnPlayerHandler extends EventHandler {
	
	void onRespawnPlayer(RespawnPlayerEvent event);
}