package org.wl.infinitewars.shared.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface GameTimeUpdateHandler extends EventHandler {
	
	void onGameTimeUpdate(GameTimeUpdateEvent event);
}