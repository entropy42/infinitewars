package org.wl.infinitewars.shared.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface RemovePlayerHandler extends EventHandler {
	
	void onRemovePlayer(RemovePlayerEvent event);
}