package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.gameplay.EntityPropertyChangeCause;

import com.jme3.scene.Spatial;

public class EntityPropertyChangeEvent extends Event<EntityPropertyChangeHandler> {
	
	public static final EventType<EntityPropertyChangeHandler> TYPE = new EventType<>();
	
	private final Spatial entity;
	private final long entityId;
	private final String propertyName;
	private final Object propertyValue;
	private final Object oldPropertyValue;
	private final EntityPropertyChangeCause cause;
	
	public EntityPropertyChangeEvent(Spatial entity, long entityId, String propertyName, Object propertyValue, Object oldPropertyValue, EntityPropertyChangeCause cause) {
		this.entity = entity;
		this.entityId = entityId;
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
		this.oldPropertyValue = oldPropertyValue;
		this.cause = cause;
	}
	
	@Override
	public EventType<EntityPropertyChangeHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(EntityPropertyChangeHandler handler) {
		handler.onChange(this);
	}
	
	public Spatial getEntity() {
		return entity;
	}
	
	public long getEntityId() {
		return entityId;
	}
	
	public String getPropertyName() {
		return propertyName;
	}
	
	public Object getPropertyValue() {
		return propertyValue;
	}
	
	public Object getOldPropertyValue() {
		return oldPropertyValue;
	}
	
	public EntityPropertyChangeCause getCause() {
		return cause;
	}
}