package org.wl.infinitewars.shared.event.main;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.gameplay.player.Player;

public class RemovePlayerEvent extends Event<RemovePlayerHandler> {
	
	public static final EventType<RemovePlayerHandler> TYPE = new EventType<>();
	
	private final Player player;
	
	public RemovePlayerEvent(Player player) {
		this.player = player;
	}
	
	@Override
	public EventType<RemovePlayerHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(RemovePlayerHandler handler) {
		handler.onRemovePlayer(this);
	}
	
	public Player getPlayer() {
		return player;
	}
}