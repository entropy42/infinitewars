package org.wl.infinitewars.shared.event.config;

import org.wl.infinitewars.shared.event.EventHandler;

public interface UpdateGraphicsConfigHandler extends EventHandler {
	
	void onUpdate(UpdateGraphicsConfigEvent event);
}