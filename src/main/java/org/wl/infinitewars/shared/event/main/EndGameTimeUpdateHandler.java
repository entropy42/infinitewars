package org.wl.infinitewars.shared.event.main;

import org.wl.infinitewars.shared.event.EventHandler;

public interface EndGameTimeUpdateHandler extends EventHandler {
	
	void onUpdateGameTime(EndGameTimeUpdateEvent event);
}