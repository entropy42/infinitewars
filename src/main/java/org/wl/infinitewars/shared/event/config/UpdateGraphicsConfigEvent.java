package org.wl.infinitewars.shared.event.config;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;

public class UpdateGraphicsConfigEvent extends Event<UpdateGraphicsConfigHandler> {
	
	public static final EventType<UpdateGraphicsConfigHandler> TYPE = new EventType<>();
	
	private final String key;
	private final String value;
	
	public UpdateGraphicsConfigEvent(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	@Override
	public EventType<UpdateGraphicsConfigHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(UpdateGraphicsConfigHandler handler) {
		handler.onUpdate(this);
	}
	
	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
}