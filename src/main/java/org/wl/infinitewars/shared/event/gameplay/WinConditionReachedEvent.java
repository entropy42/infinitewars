package org.wl.infinitewars.shared.event.gameplay;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventType;
import org.wl.infinitewars.shared.gameplay.player.Team;

public class WinConditionReachedEvent extends Event<WinConditionReachedHandler> {
	
	public static final EventType<WinConditionReachedHandler> TYPE = new EventType<>();
	
	private final Team winnerTeam;
	
	public WinConditionReachedEvent(Team winnerTeam) {
		this.winnerTeam = winnerTeam;
	}
	
	@Override
	public EventType<WinConditionReachedHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(WinConditionReachedHandler handler) {
		handler.onWinConditionReached(this);
	}
	
	public Team getWinnerTeam() {
		return winnerTeam;
	}
}