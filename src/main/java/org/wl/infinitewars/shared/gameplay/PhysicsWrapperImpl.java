package org.wl.infinitewars.shared.gameplay;

import com.google.inject.Singleton;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

@Singleton
public class PhysicsWrapperImpl implements PhysicsWrapper {

	private PhysicsSpace physicsSpace;

	@Override
	public void addEntity(Spatial entity) {
		physicsSpace.add(entity);
	}

	@Override
	public void removeEntity(Spatial entity) {
		physicsSpace.remove(entity);
	}

	@Override
	public void setPhysicsSpace(PhysicsSpace physicsSpace) {
		this.physicsSpace = physicsSpace;
	}

	@Override
	public void addTickListener(PhysicsTickListener tickListener) {
		physicsSpace.addTickListener(tickListener);
	}

	@Override
	public void cleanup(Node rootNode) {
		physicsSpace.removeAll(rootNode);
	}
}