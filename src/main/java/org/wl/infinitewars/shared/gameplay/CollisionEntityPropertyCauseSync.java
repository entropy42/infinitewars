package org.wl.infinitewars.shared.gameplay;

import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

import com.jme3.network.serializing.Serializable;

@Serializable
public class CollisionEntityPropertyCauseSync implements EntityPropertyCauseSync {

	private long entityId;

	public CollisionEntityPropertyCauseSync() {

	}

	public CollisionEntityPropertyCauseSync(long entityId) {
		this.entityId = entityId;
	}

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

	@Override
	public CollisionCause createCollisionCause(AbstractSyncState syncState) {
		return null;
	}
}