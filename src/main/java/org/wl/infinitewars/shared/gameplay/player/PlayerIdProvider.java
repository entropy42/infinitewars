package org.wl.infinitewars.shared.gameplay.player;

public interface PlayerIdProvider {
	
	int generatePlayerId();
}