package org.wl.infinitewars.shared.gameplay;

import java.util.HashMap;
import java.util.Map;

import org.wl.infinitewars.server.ai.AILevel;
import org.wl.infinitewars.shared.json.GameMode;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

@Serializable
public class GameInfo implements java.io.Serializable {

	private static final long serialVersionUID = -7846825798673405921L;

	private String mapName;
	private int maxPlayers;
	private long initMoney;
	private Map<Vector3f, Long> objectIds = new HashMap<>();
	private GameMode gameMode;
	private int serverTickRate;
	private int gameTime;
	private boolean enableDebug;
	private boolean enableBots;
	private AILevel aiLevel;
	private float spawnDelay;

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public long getInitMoney() {
		return initMoney;
	}

	public void setInitMoney(long initMoney) {
		this.initMoney = initMoney;
	}

	public Map<Vector3f, Long> getObjectIds() {
		return objectIds;
	}

	public void setObjectIds(Map<Vector3f, Long> objectIds) {
		this.objectIds = objectIds;
	}

	public GameMode getGameMode() {
		return gameMode;
	}

	public void setGameMode(GameMode gameMode) {
		this.gameMode = gameMode;
	}

	public int getServerTickRate() {
		return serverTickRate;
	}

	public void setServerTickRate(int serverTickRate) {
		this.serverTickRate = serverTickRate;
	}

	public boolean isEnableDebug() {
		return enableDebug;
	}

	public void setEnableDebug(boolean enableDebug) {
		this.enableDebug = enableDebug;
	}

	public boolean isEnableBots() {
		return enableBots;
	}

	public void setEnableBots(boolean enableBots) {
		this.enableBots = enableBots;
	}

	public int getGameTime() {
		return gameTime;
	}

	public void setGameTime(int gameTime) {
		this.gameTime = gameTime;
	}

	public AILevel getAiLevel() {
		return aiLevel;
	}

	public void setAiLevel(AILevel aiLevel) {
		this.aiLevel = aiLevel;
	}

	public float getSpawnDelay() {
		return spawnDelay;
	}

	public void setSpawnDelay(float spawnDelay) {
		this.spawnDelay = spawnDelay;
	}
}