package org.wl.infinitewars.shared.gameplay;

import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

public interface EntityPropertyCauseSync {

	EntityPropertyChangeCause createCollisionCause(AbstractSyncState syncState);
}