package org.wl.infinitewars.shared.gameplay;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public interface PhysicsWrapper {
	
	void addEntity(Spatial entity);
	
	void removeEntity(Spatial entity);
	
	void setPhysicsSpace(PhysicsSpace physicsSpace);
	
	void addTickListener(PhysicsTickListener tickListener);
	
	void cleanup(Node rootNode);
}