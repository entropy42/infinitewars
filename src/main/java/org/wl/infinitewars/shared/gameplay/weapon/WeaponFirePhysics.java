package org.wl.infinitewars.shared.gameplay.weapon;

import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public abstract class WeaponFirePhysics {

	protected final String prefix;

	public WeaponFirePhysics(String prefix) {
		this.prefix = prefix;
	}

	public abstract CollisionShape createCollisionShape(Node weaponFire, Spatial firingShip);
}