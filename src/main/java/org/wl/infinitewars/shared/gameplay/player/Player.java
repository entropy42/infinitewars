package org.wl.infinitewars.shared.gameplay.player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.wl.infinitewars.client.event.gameplay.UpdateNearestTargetEvent;
import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.event.gameplay.PlayerPropertyChangeEvent;
import org.wl.infinitewars.shared.gameplay.TeamsContainer;
import org.wl.infinitewars.shared.network.messages.toclient.gamestate.PropertyInitEntry;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class Player implements Savable, Comparable<Player> {

	protected final EventBus eventBus = EventBus.get();

	protected final List<AbstractControl> playerControls = new LinkedList<>();
	protected final Map<String, Object> properties = new HashMap<>();

	protected final String name;
	protected final long playerId;
	protected final boolean isLocalPlayer;
	protected final long initMoney;

	protected Node ship;
	protected boolean isAlive;
	protected boolean wantsRespawn;
	protected TeamsContainer teamsContainer;

	protected boolean isBot;

	protected Player target;

	public Player(String name, long playerId, long initMoney, boolean isLocalPlayer, short teamId, TeamsContainer teamsContainer) {
		this.name = name;
		this.playerId = playerId;
		this.isLocalPlayer = isLocalPlayer;
		this.initMoney = initMoney;
		this.teamsContainer = teamsContainer;

		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("name must not be blank!");
		}

		if (playerId <= 0l) {
			throw new IllegalArgumentException("playerId must be higher than zero!");
		}

		if (teamId <= 0) {
			throw new IllegalArgumentException("teamId must be higher than zero!");
		}

		if (initMoney < 0l) {
			throw new IllegalArgumentException("initMoney must not be negative!");
		}

		properties.put(PlayerConstants.MONEY, initMoney);
		properties.put(PlayerConstants.KILLS, 0);
		properties.put(PlayerConstants.DEATHS, 0);
		properties.put(PlayerConstants.TEAM, teamId);
	}

	public String getName() {
		return name;
	}

	public long getPlayerId() {
		return playerId;
	}

	public Team getTeam() {
		short teamId = MapUtils.getShortValue(properties, PlayerConstants.TEAM);
		return teamsContainer.getTeam(teamId);
	}

	public Team getEnemyTeam() {
		short teamId = MapUtils.getShortValue(properties, PlayerConstants.TEAM);
		return teamsContainer.getEnemyTeam(teamId);
	}

	public long getMoney() {
		return MapUtils.getLongValue(properties, PlayerConstants.MONEY);
	}

	public int getKills() {
		return MapUtils.getIntValue(properties, PlayerConstants.KILLS);
	}

	public int getDeaths() {
		return MapUtils.getIntValue(properties, PlayerConstants.DEATHS);
	}

	public long getSpawnPointEntityId() {
		return MapUtils.getIntValue(properties, PlayerConstants.SPAWN_POINT);
	}

	public void addKill() {
		int kills = getKills();
		kills++;
		setProperty(PlayerConstants.KILLS, kills);

		kills = getKills();

		if (kills < 0) {
			throw new IllegalStateException("The kill count of player " + playerId + " is below zero!");
		}
	}

	public void addDeath() {
		int deaths = getDeaths();
		deaths++;
		setProperty(PlayerConstants.DEATHS, deaths);

		deaths = getDeaths();

		if (deaths < 0) {
			throw new IllegalStateException("The death count of player " + playerId + " is below zero!");
		}
	}

	public void addMoney(long addition) {
		if (addition <= 0l) {
			throw new IllegalArgumentException("money to add must be higher than zero!");
		}

		long money = getMoney();
		money += addition;

		setProperty(PlayerConstants.MONEY, money);
		checkMoney();
	}

	public void removeMoney(long costs) {
		if (costs <= 0l) {
			throw new IllegalArgumentException("money to subtract must be higher than zero!");
		}

		long money = getMoney();
		money -= costs;

		if (money < 0l) {
			money = 0l;
		}

		setProperty(PlayerConstants.MONEY, money);
		checkMoney();
	}

	protected void checkMoney() {
		long money = getMoney();

		if (money < 0l) {
			throw new IllegalStateException("The money of player " + playerId + " is below zero!");
		}
	}

	public void setSpawnPointEntityId(long spawnPointId) {
		if (spawnPointId <= 0l) {
			throw new IllegalArgumentException("spawnPointId needs to be higher than zero!");
		}

		setProperty(PlayerConstants.SPAWN_POINT, spawnPointId);
	}

	public void setProperty(String name, Object value) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("the property name must not be blank!");
		}

		Object oldValue = properties.get(name);
		properties.put(name, value);

		eventBus.fireEvent(new PlayerPropertyChangeEvent(this, name, value, oldValue));
	}

	public Collection<PropertyInitEntry> createInitEntries() {
		List<PropertyInitEntry> list = new ArrayList<>(properties.size());

		for (Entry<String, Object> e : properties.entrySet()) {
			list.add(new PropertyInitEntry(playerId, e.getKey(), e.getValue()));
		}

		return list;
	}

	public void addPlayerControl(AbstractControl control) {
		playerControls.add(control);
	}

	public Node getShip() {
		return ship;
	}

	public void setShip(Node ship) {
		this.ship = ship;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public boolean isWantsRespawn() {
		return wantsRespawn;
	}

	public void setWantsRespawn(boolean wantsRespawn) {
		this.wantsRespawn = wantsRespawn;
	}

	public void setRespawned() {
		isAlive = true;
		wantsRespawn = false;
	}

	public boolean isLocalPlayer() {
		return isLocalPlayer;
	}

	public void detachPlayerControls() {
		for (AbstractControl control : playerControls) {
			control.setEnabled(false);
			Spatial spatial = control.getSpatial();

			if (spatial != null) {
				spatial.removeControl(control);
			}
		}

		playerControls.clear();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int)(playerId ^ (playerId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player)obj;
		if (playerId != other.playerId)
			return false;
		return true;
	}

	public boolean isBot() {
		return isBot;
	}

	public void setBot(boolean isBot) {
		this.isBot = isBot;
	}

	@Override
	public void write(JmeExporter ex) throws IOException {

	}

	@Override
	public void read(JmeImporter im) throws IOException {

	}

	@Override
	public int compareTo(Player o) {
		Integer kills = getKills();
		Integer otherKills = o.getKills();

		int value = otherKills.compareTo(kills);

		if (value == 0) {
			Integer deaths = getDeaths();
			Integer otherDeaths = o.getDeaths();

			return deaths.compareTo(otherDeaths);
		}

		return value;
	}

	public void resetMoney() {
		setProperty(PlayerConstants.MONEY, initMoney);
	}

	public Player getTarget() {
		return target;
	}

	public void setTarget(Player target) {
		this.target = target;
		eventBus.fireEvent(new UpdateNearestTargetEvent(target));
	}

	@Override
	public String toString() {
		return name + " (ID: " + playerId + ", Team: " + getTeam().getFaction().name() + ")";
	}
}