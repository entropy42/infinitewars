package org.wl.infinitewars.shared.gameplay.weapon;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public class WeaponFireCreationData {

	private final Vector3f startPoint;
	private final Node firingShip;
	private final String weaponSlotsContainerName;
	private final Vector3f direction;

	public WeaponFireCreationData(Vector3f startPoint, Node firingShip, String weaponSlotsContainerName, Vector3f direction) {
		this.startPoint = startPoint;
		this.firingShip = firingShip;
		this.weaponSlotsContainerName = weaponSlotsContainerName;
		this.direction = direction;
	}

	public Vector3f getStartPoint() {
		return startPoint;
	}

	public Node getFiringShip() {
		return firingShip;
	}

	public String getWeaponSlotsContainerName() {
		return weaponSlotsContainerName;
	}

	public Vector3f getDirection() {
		return direction;
	}
}