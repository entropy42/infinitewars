package org.wl.infinitewars.shared.gameplay;

public interface EntityPropertyChangeCause {

	EntityPropertyCauseSync createCauseForMessage();
}