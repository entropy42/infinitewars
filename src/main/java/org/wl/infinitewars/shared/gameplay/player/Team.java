package org.wl.infinitewars.shared.gameplay.player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.event.EventHandlersContainer;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeEvent;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeHandler;
import org.wl.infinitewars.shared.event.gameplay.PlayerPropertyChangeEvent;
import org.wl.infinitewars.shared.event.gameplay.PlayerPropertyChangeHandler;
import org.wl.infinitewars.shared.json.Faction;
import org.wl.infinitewars.shared.json.FactionConfig;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.util.FileConstants;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;
import com.jme3.scene.Spatial;

public class Team implements Savable {

	private final EventHandlersContainer eventHandlersContainer = new EventHandlersContainer();

	private List<Player> players;
	private Map<Long, Player> playerIdMap;

	private final Faction faction;
	private final FactionConfig factionConfig;

	private Spatial motherShip;
	private final Set<Spatial> activeSpawnPoints = new HashSet<>();

	private final short teamId;

	public Team(short teamId, Faction faction, int maxPlayers) throws Exception {
		this.teamId = teamId;
		this.faction = faction;

		factionConfig = JsonHelper.get().toPOJO(FileConstants.FACTIONS_PATH_COMPLETE + faction.name().toLowerCase() + FileConstants.FACTION_CONFIG_FILE, FactionConfig.class);

		players = new ArrayList<>(maxPlayers);
		playerIdMap = new HashMap<>(maxPlayers);

		eventHandlersContainer.addHandler(EntityPropertyChangeEvent.TYPE, new EntityPropertyChangeHandler() {
			@Override
			public void onChange(EntityPropertyChangeEvent event) {
				Object controlPointCapturePoints = event.getEntity().getUserData(EntityConstants.controlPointCapturePoints);

				if (event.getPropertyName().equals(EntityConstants.faction) && controlPointCapturePoints != null) {
					if (faction.name().equals(event.getPropertyValue())) {
						activeSpawnPoints.add(event.getEntity());
					} else {
						activeSpawnPoints.remove(event.getEntity());
					}
				}
			}
		});

		eventHandlersContainer.addHandler(PlayerPropertyChangeEvent.TYPE, new PlayerPropertyChangeHandler() {
			@Override
			public void onChange(PlayerPropertyChangeEvent event) {
				if (event.getPropertyName().equals(PlayerConstants.TEAM)) {
					refreshPlayerList();
				}
			}
		});
	}

	protected void refreshPlayerList() {
		players = players.stream().filter(p -> p.getTeam() == this).collect(Collectors.toList());
		playerIdMap = new HashMap<>();

		for (Player player : players) {
			playerIdMap.put(player.getPlayerId(), player);
		}
	}

	public Set<Spatial> getActiveSpawnPoints() {
		return activeSpawnPoints;
	}

	public void addPlayer(Player player) {
		players.add(player);
		playerIdMap.put(player.getPlayerId(), player);
	}

	public void removePlayer(Player player) {
		players.remove(player);
		playerIdMap.remove(player.getPlayerId());
	}

	public Faction getFaction() {
		return faction;
	}

	public int getPlayerCount() {
		return players.size();
	}

	public Spatial getMotherShip() {
		return motherShip;
	}

	public void setMotherShip(Spatial motherShip) {
		this.motherShip = motherShip;
		activeSpawnPoints.add(motherShip);
	}

	public short getTeamId() {
		return teamId;
	}

	public Player getLastJoindedPlayerBot() {
		for (int i = players.size() - 1; i >= 0; i--) {
			Player player = players.get(i);

			if (player.isBot()) {
				return player;
			}
		}

		return null;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public Player getPlayer(long playerId) {
		return playerIdMap.get(playerId);
	}

	public FactionConfig getFactionConfig() {
		return factionConfig;
	}

	@Override
	public void read(JmeImporter arg0) throws IOException {

	}

	@Override
	public void write(JmeExporter arg0) throws IOException {

	}

	public void cleanup() {
		List<Player> bots = new ArrayList<>(players.size());

		for (Player player : players) {
			if (player.isBot()) {
				bots.add(player);
			} else {
				player.setAlive(false);
				player.setShip(null);
				player.setWantsRespawn(false);
				player.resetMoney();
			}
		}

		for (Player bot : bots) {
			removePlayer(bot);
		}

		eventHandlersContainer.cleanup();
	}
}