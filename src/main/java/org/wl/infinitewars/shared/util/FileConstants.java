package org.wl.infinitewars.shared.util;

public interface FileConstants {

	String UI_PATH = "assets/ui/";
	String HUD_PATH = UI_PATH + "hud/";
	String LOADING_PATH = UI_PATH + "loading/";

	String MAPS_PATH = "assets/maps/";
	String MAPS_PATH_COMPLETE = "/" + MAPS_PATH;
	String MAP_DATA_FILE = "/mapData.json";

	String FACTIONS_PATH = "assets/factions/";
	String FACTIONS_PATH_COMPLETE = "/" + FACTIONS_PATH;
	String FACTION_CONFIG_FILE = "/config.json";

	String MODELS_PATH = "assets/models/";
	String MODELS_PATH_COMPLETE = "/" + MODELS_PATH;
	String MODEL_FILE = "/model.blend";
	String MODEL_FILE_LOW_POLY = "/model_low_poly.blend";
	String MODEL_PROPERTY_FILE = "/properties.json";

	String TEXTURES_PATH = "assets/textures/";
	String SKYBOXES_PATH = TEXTURES_PATH + "skyboxes/";

	String SERVER_CONFIG = "serverConfig.json";

	String EHCACHE_CONFIG = "/" + "ehcache/config.xml";

	String GAME_CONFIG_FILE_DIR = ".infinitewars/";
	String CLIENT_CONFIG_FILE = "config.properties";
}