package org.wl.infinitewars.shared.util;

import com.jme3.math.Vector3f;

public class TorqueResult {
	
	private final Vector3f negationTorque;
	private final Vector3f torque;
	
	public TorqueResult(Vector3f negationTorque, Vector3f torque) {
		this.negationTorque = negationTorque;
		this.torque = torque;
	}
	
	public Vector3f getNegationTorque() {
		return negationTorque;
	}
	
	public Vector3f getTorque() {
		return torque;
	}
}