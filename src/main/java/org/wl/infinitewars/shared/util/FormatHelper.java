package org.wl.infinitewars.shared.util;

public class FormatHelper {
	
	public static String formatTime(float time) {
		int minutes = (int)Math.floor(time / 60f);
		String minutesString = minutes + "";
		
		if (minutes < 10) {
			minutesString = "0" + minutesString;
		}
		
		int secondsLeft = Math.round(time - (minutes * 60f));
		String secondsString = secondsLeft + "";
		
		if (secondsLeft < 10) {
			secondsString = "0" + secondsLeft;
		}
		
		String timeString = minutesString + ":" + secondsString;
		
		return timeString;
	}
}