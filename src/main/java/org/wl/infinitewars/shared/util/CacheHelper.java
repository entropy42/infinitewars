package org.wl.infinitewars.shared.util;

import java.io.IOException;
import java.io.InputStream;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

public class CacheHelper {

	private static CacheHelper instance;

	protected final CacheManager cacheManager;
	protected final Cache jsonCache;

	public CacheHelper(CacheManager cacheManager, Cache xmlCache) {
		this.cacheManager = cacheManager;
		this.jsonCache = xmlCache;
	}

	public static CacheHelper get() throws IOException {
		if (instance == null) {
			try (InputStream inputStream = CacheHelper.class.getResourceAsStream(FileConstants.EHCACHE_CONFIG)) {
				CacheManager cacheManager = CacheManager.create(inputStream);
				Cache jsonCache = cacheManager.getCache("jsonCache");
				instance = new CacheHelper(cacheManager, jsonCache);
			}
		}

		return instance;
	}

	public static void setInstance(CacheHelper cacheHelper) {
		instance = cacheHelper;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public Cache getJsonCache() {
		return jsonCache;
	}
}