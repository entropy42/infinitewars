package org.wl.infinitewars.shared.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class EntityProperty implements Serializable {

	private static final long serialVersionUID = -5486912566096470321L;

	private String key;
	private String value;
	private EntityPropertyType type;

	public EntityProperty() {

	}

	public EntityProperty(String key, String value, EntityPropertyType type) {
		this.key = key;
		this.value = value;
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public EntityPropertyType getType() {
		return type;
	}

	public void setType(EntityPropertyType type) {
		this.type = type;
	}

	@JsonIgnore
	public Object getTypedValue() {
		return getTypedValue(value);
	}

	@JsonIgnore
	public Object getTypedValue(String value) {
		switch (type) {
			case BOOLEAN:
				return Boolean.valueOf(value);
			case FLOAT:
				return Float.valueOf(value);
			case INTEGER:
				return Integer.valueOf(value);
			case STRING:
				return value;
		}

		throw new RuntimeException("WTF?");
	}
}