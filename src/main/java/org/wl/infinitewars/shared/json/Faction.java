package org.wl.infinitewars.shared.json;

public enum Faction {
	
	HUMANS, ALIENS
}