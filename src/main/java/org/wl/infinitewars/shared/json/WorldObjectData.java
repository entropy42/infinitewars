package org.wl.infinitewars.shared.json;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class WorldObjectData implements Serializable {

	private static final long serialVersionUID = -6839155998455517901L;

	private String name;
	private Position location;
	private Rotation rotation;
	private Position scale;
	private List<GameMode> gameModes;
	private Map<String, Object> data;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Position getLocation() {
		return location;
	}

	public void setLocation(Position location) {
		this.location = location;
	}

	public Rotation getRotation() {
		return rotation;
	}

	public void setRotation(Rotation rotation) {
		this.rotation = rotation;
	}

	public Position getScale() {
		return scale;
	}

	public void setScale(Position scale) {
		this.scale = scale;
	}

	public List<GameMode> getGameModes() {
		return gameModes;
	}

	public void setGameModes(List<GameMode> gameModes) {
		this.gameModes = gameModes;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}
}