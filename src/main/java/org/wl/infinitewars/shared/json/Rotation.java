package org.wl.infinitewars.shared.json;

import java.io.Serializable;

import com.jme3.math.Quaternion;

public class Rotation implements Serializable {

	private static final long serialVersionUID = -4064964026640656837L;

	private float x;
	private float y;
	private float z;
	private float w;

	public Rotation() {

	}

	public Rotation(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Rotation(Quaternion quaternion) {
		this.x = quaternion.getX();
		this.y = quaternion.getY();
		this.z = quaternion.getZ();
		this.w = quaternion.getW();
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float getW() {
		return w;
	}

	public void setW(float w) {
		this.w = w;
	}

	public Quaternion toQuaternion() {
		return new Quaternion(x, y, z, w);
	}
}