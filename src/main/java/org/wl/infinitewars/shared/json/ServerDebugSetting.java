package org.wl.infinitewars.shared.json;

import java.io.Serializable;

public class ServerDebugSetting implements Serializable {

	private static final long serialVersionUID = 7916441868381885041L;

	private String command;
	private String[] arguments;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String[] getArguments() {
		return arguments;
	}

	public void setArguments(String[] arguments) {
		this.arguments = arguments;
	}
}