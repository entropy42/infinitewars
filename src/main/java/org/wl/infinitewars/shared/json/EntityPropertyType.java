package org.wl.infinitewars.shared.json;

public enum EntityPropertyType {

	STRING, INTEGER, FLOAT, BOOLEAN
}