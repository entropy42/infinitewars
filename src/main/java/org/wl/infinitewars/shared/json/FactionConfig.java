package org.wl.infinitewars.shared.json;

import java.io.Serializable;
import java.util.List;

public class FactionConfig implements Serializable {

	private static final long serialVersionUID = -2904441868495240375L;

	private Faction faction;
	private String motherShipName;
	private List<ShipConfig> ships;

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	public String getMotherShipName() {
		return motherShipName;
	}

	public void setMotherShipName(String motherShipName) {
		this.motherShipName = motherShipName;
	}

	public List<ShipConfig> getShips() {
		return ships;
	}

	public void setShips(List<ShipConfig> ships) {
		this.ships = ships;
	}
}