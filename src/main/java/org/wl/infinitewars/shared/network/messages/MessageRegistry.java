package org.wl.infinitewars.shared.network.messages;

import org.wl.infinitewars.shared.control.MovementInfo;
import org.wl.infinitewars.shared.gameplay.CollisionEntityPropertyCauseSync;
import org.wl.infinitewars.shared.gameplay.GameInfo;
import org.wl.infinitewars.shared.gameplay.GameInfoInitData;
import org.wl.infinitewars.shared.gameplay.WeaponFireEntityPropertyCauseSync;
import org.wl.infinitewars.shared.network.messages.toclient.RestartGameMessage;
import org.wl.infinitewars.shared.network.messages.toclient.config.DebugConfigChangedMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.ChangePlayerSpawnLocationRequestMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.EntityPropertyChangeMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.PlayerPropertyChangeMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.PlayerShipChangeMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.RemoveWeaponFireMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.RespawnPlayerMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.SpectatedPlayerUpdateMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.WeaponFireCreationMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.WinConditionReachedMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gamestate.GameStateResponseMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gamestate.PropertyInitEntry;
import org.wl.infinitewars.shared.network.messages.toclient.main.EndTimerUpdateMessage;
import org.wl.infinitewars.shared.network.messages.toclient.main.GameInfoInitMessage;
import org.wl.infinitewars.shared.network.messages.toclient.main.GameTimeUpdateMessage;
import org.wl.infinitewars.shared.network.messages.toclient.main.PlayerNameRequestMessage;
import org.wl.infinitewars.shared.network.messages.toclient.main.ServerRestartedMessage;
import org.wl.infinitewars.shared.network.messages.toclient.sync.AbstractSyncMessage;
import org.wl.infinitewars.shared.network.messages.toclient.sync.ShipSyncMessage;
import org.wl.infinitewars.shared.network.messages.toclient.sync.WeaponFireSyncMessage;
import org.wl.infinitewars.shared.network.messages.toserver.EntityCreationRequestMessage;
import org.wl.infinitewars.shared.network.messages.toserver.GameStateRequestMessage;
import org.wl.infinitewars.shared.network.messages.toserver.config.DebugConfigChangeRequestMessage;
import org.wl.infinitewars.shared.network.messages.toserver.gameplay.BuyShipRequestMessage;
import org.wl.infinitewars.shared.network.messages.toserver.gameplay.ChangeSpectatedPlayerMessage;
import org.wl.infinitewars.shared.network.messages.toserver.gameplay.WantsRespawnMessage;
import org.wl.infinitewars.shared.network.messages.toserver.input.FireWeaponsMessage;
import org.wl.infinitewars.shared.network.messages.toserver.input.MoveShipMessage;
import org.wl.infinitewars.shared.network.messages.toserver.main.PlayerNameResponseMessage;

import com.jme3.network.serializing.Serializer;

public class MessageRegistry {

	public static void registerMessages() {
		Serializer.registerClass(MovementInfo.class);
		Serializer.registerClass(PropertyInitEntry.class);
		Serializer.registerClass(GameInfo.class);
		Serializer.registerClass(GameInfoInitMessage.class);
		Serializer.registerClass(EntityCreationRequestMessage.class);
		Serializer.registerClass(PlayerNameRequestMessage.class);
		Serializer.registerClass(PlayerNameResponseMessage.class);
		Serializer.registerClass(BuyShipRequestMessage.class);
		Serializer.registerClass(AbstractSyncMessage.class);
		Serializer.registerClass(ShipSyncMessage.class);
		Serializer.registerClass(WeaponFireSyncMessage.class);
		Serializer.registerClass(GameTimeUpdateMessage.class);
		Serializer.registerClass(WantsRespawnMessage.class);
		Serializer.registerClass(RespawnPlayerMessage.class);
		Serializer.registerClass(PlayerShipChangeMessage.class);
		Serializer.registerClass(MoveShipMessage.class);
		Serializer.registerClass(GameStateResponseMessage.class);
		Serializer.registerClass(GameStateRequestMessage.class);
		Serializer.registerClass(FireWeaponsMessage.class);
		Serializer.registerClass(WeaponFireCreationMessage.class);
		Serializer.registerClass(RemoveWeaponFireMessage.class);
		Serializer.registerClass(EntityPropertyChangeMessage.class);
		Serializer.registerClass(PlayerPropertyChangeMessage.class);
		Serializer.registerClass(WinConditionReachedMessage.class);
		Serializer.registerClass(EndTimerUpdateMessage.class);
		Serializer.registerClass(RestartGameMessage.class);
		Serializer.registerClass(ServerRestartedMessage.class);
		Serializer.registerClass(GameInfoInitData.class);
		Serializer.registerClass(DebugConfigChangeRequestMessage.class);
		Serializer.registerClass(DebugConfigChangedMessage.class);
		Serializer.registerClass(ChangePlayerSpawnLocationRequestMessage.class);
		Serializer.registerClass(CollisionEntityPropertyCauseSync.class);
		Serializer.registerClass(WeaponFireEntityPropertyCauseSync.class);
		Serializer.registerClass(ChangeSpectatedPlayerMessage.class);
		Serializer.registerClass(SpectatedPlayerUpdateMessage.class);
	}
}