package org.wl.infinitewars.shared.network.messages.toserver;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class EntityCreationRequestMessage extends AbstractMessage {
	
	private String file;
	
	public EntityCreationRequestMessage() {
		super(true);
	}
	
	public EntityCreationRequestMessage(String file) {
		this();
		this.file = file;
	}
	
	public String getFile() {
		return file;
	}
	
	public void setFile(String file) {
		this.file = file;
	}
}