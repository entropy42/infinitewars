package org.wl.infinitewars.shared.network.messages.toclient.sync;

import org.wl.infinitewars.shared.control.MoveControl;
import org.wl.infinitewars.shared.control.MovementInfo;
import org.wl.infinitewars.shared.entity.EntityHelper;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import com.jme3.scene.Spatial;

@Serializable
public class ShipSyncMessage extends AbstractSyncMessage {

	private Vector3f location;
	private Quaternion rotation;
	private Vector3f linearVelocity;
	private Vector3f angularVelocity;
	private MovementInfo moveInfo;

	public ShipSyncMessage() {
		super();
	}

	public ShipSyncMessage(Spatial ship) {
		this();
		this.entityId = EntityHelper.getEntityId(ship);

		RigidBodyControl body = ship.getControl(RigidBodyControl.class);
		location = body.getPhysicsLocation();
		rotation = body.getPhysicsRotation();
		linearVelocity = body.getLinearVelocity();
		angularVelocity = body.getAngularVelocity();

		MoveControl moveControl = ship.getControl(MoveControl.class);

		if (moveControl != null) {
			moveInfo = moveControl.getMoveInfo();
		}
	}

	public Vector3f getLocation() {
		return location;
	}

	public void setLocation(Vector3f location) {
		this.location = location;
	}

	public Quaternion getRotation() {
		return rotation;
	}

	public void setRotation(Quaternion rotation) {
		this.rotation = rotation;
	}

	public Vector3f getLinearVelocity() {
		return linearVelocity;
	}

	public void setLinearVelocity(Vector3f linearVelocity) {
		this.linearVelocity = linearVelocity;
	}

	public Vector3f getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(Vector3f angularVelocity) {
		this.angularVelocity = angularVelocity;
	}

	public MovementInfo getMoveInfo() {
		return moveInfo;
	}

	public void setMoveInfo(MovementInfo moveInfo) {
		this.moveInfo = moveInfo;
	}
}