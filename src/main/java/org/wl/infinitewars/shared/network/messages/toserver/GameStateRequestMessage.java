package org.wl.infinitewars.shared.network.messages.toserver;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class GameStateRequestMessage extends AbstractMessage {
	
	public GameStateRequestMessage() {
		super(true);
	}
}