package org.wl.infinitewars.shared.network.messages.toserver.main;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class PlayerNameResponseMessage extends AbstractMessage {
	
	private String playerName;
	
	public PlayerNameResponseMessage() {
		super(true);
	}
	
	public PlayerNameResponseMessage(String playerName) {
		this();
		this.playerName = playerName;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
}