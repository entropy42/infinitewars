package org.wl.infinitewars.shared.network.messages.toserver.gameplay;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class BuyShipRequestMessage extends AbstractMessage {
	
	private String shipName;
	
	public BuyShipRequestMessage() {
		super(true);
	}
	
	public BuyShipRequestMessage(String shipName) {
		this();
		this.shipName = shipName;
	}
	
	public String getShipName() {
		return shipName;
	}
	
	public void setShipName(String shipName) {
		this.shipName = shipName;
	}
}