package org.wl.infinitewars.shared.network.messages.toclient.main;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class PlayerNameRequestMessage extends AbstractMessage {
	
	public PlayerNameRequestMessage() {
		super(true);
	}
}