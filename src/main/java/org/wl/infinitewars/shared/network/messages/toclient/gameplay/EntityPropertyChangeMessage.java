package org.wl.infinitewars.shared.network.messages.toclient.gameplay;

import org.wl.infinitewars.shared.gameplay.EntityPropertyCauseSync;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class EntityPropertyChangeMessage extends AbstractMessage {

	private long entityId;
	private String propertyName;
	private Object propertyValue;
	private EntityPropertyCauseSync cause;

	public EntityPropertyChangeMessage() {
		super(true);
	}

	public EntityPropertyChangeMessage(long entityId, String propertyName, Object propertyValue, EntityPropertyCauseSync cause) {
		this();
		this.entityId = entityId;
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
		this.cause = cause;
	}

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public Object getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(Object propertyValue) {
		this.propertyValue = propertyValue;
	}

	public EntityPropertyCauseSync getCause() {
		return cause;
	}

	public void setCause(EntityPropertyCauseSync cause) {
		this.cause = cause;
	}
}