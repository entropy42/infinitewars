package org.wl.infinitewars.shared.network.messages.toclient.gameplay;

import com.jme3.math.Vector3f;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class RemoveWeaponFireMessage extends AbstractMessage {
	
	private boolean explosion;
	private long weaponFireId;
	private Vector3f lastLocation;
	
	public RemoveWeaponFireMessage() {
		super(true);
	}
	
	public RemoveWeaponFireMessage(boolean explosion, long weaponFireId, Vector3f lastLocation) {
		this();
		this.explosion = explosion;
		this.weaponFireId = weaponFireId;
		this.lastLocation = lastLocation;
	}
	
	public long getWeaponFireId() {
		return weaponFireId;
	}
	
	public void setWeaponFireId(long weaponFireId) {
		this.weaponFireId = weaponFireId;
	}
	
	public boolean isExplosion() {
		return explosion;
	}
	
	public void setExplosion(boolean explosion) {
		this.explosion = explosion;
	}
	
	public Vector3f getLastLocation() {
		return lastLocation;
	}
	
	public void setLastLocation(Vector3f lastLocation) {
		this.lastLocation = lastLocation;
	}
}