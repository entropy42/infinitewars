package org.wl.infinitewars.shared.network.messages.toserver.input;

import org.wl.infinitewars.shared.control.MovementInfo;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class MoveShipMessage extends AbstractMessage {

	private MovementInfo movementInfo;

	public MoveShipMessage() {
		super(false);
	}

	public MoveShipMessage(MovementInfo movementInfo) {
		this();
		this.movementInfo = movementInfo;
	}

	public MovementInfo getMovementInfo() {
		return movementInfo;
	}

	public void setMovementInfo(MovementInfo movementInfo) {
		this.movementInfo = movementInfo;
	}
}