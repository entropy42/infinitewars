package org.wl.infinitewars.shared.network.messages.toclient.sync;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

@Serializable
public class WeaponFireSyncMessage extends AbstractSyncMessage {
	
	private Vector3f location;
	
	public WeaponFireSyncMessage() {
		super();
	}
	
	public WeaponFireSyncMessage(long entityId, Vector3f location) {
		this();
		this.entityId = entityId;
		this.location = location;
	}
	
	public Vector3f getLocation() {
		return location;
	}
	
	public void setLocation(Vector3f location) {
		this.location = location;
	}
}