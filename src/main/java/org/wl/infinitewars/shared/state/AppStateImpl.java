package org.wl.infinitewars.shared.state;

import org.wl.infinitewars.shared.event.Event;
import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.event.EventHandler;
import org.wl.infinitewars.shared.event.EventHandlersContainer;
import org.wl.infinitewars.shared.event.EventType;

import com.jme3.app.state.AbstractAppState;
import com.jme3.scene.Node;

public class AppStateImpl extends AbstractAppState implements IAppState {

	private final EventBus eventBus = EventBus.get();
	private final EventHandlersContainer eventHandlersContainer = new EventHandlersContainer();

	protected final Node rootNode;
	protected final Node guiNode;

	protected final Node appRootNode;
	protected final Node appGuiNode;

	public AppStateImpl(Node appRootNode, Node appGuiNode) {
		super();
		this.appRootNode = appRootNode;
		this.appGuiNode = appGuiNode;

		String className = getClass().getName();
		rootNode = new Node("Root node of " + className);
		guiNode = new Node("GUI node of " + getClass().getName());

		checkNodes();
	}

	protected void fireEvent(Event<?> event) {
		eventBus.fireEvent(event);
	}

	protected <H extends EventHandler> void addEventHandler(EventType<H> eventType, H handler) {
		eventHandlersContainer.addHandler(eventType, handler);
	}

	@Override
	public void setEnabled(boolean enabled) {
		boolean changed = isEnabled() != enabled;
		super.setEnabled(enabled);

		if (changed) {
			checkNodes();
		}
	}

	private void checkNodes() {
		if (isEnabled()) {
			appRootNode.attachChild(rootNode);
			appGuiNode.attachChild(guiNode);
		} else {
			appRootNode.detachChild(rootNode);
			appGuiNode.detachChild(guiNode);
		}
	}

	@Override
	public void cleanup() {
		super.cleanup();

		eventHandlersContainer.cleanup();

		setEnabled(false);

		rootNode.detachAllChildren();
		guiNode.detachAllChildren();
	}

	@Override
	public Node getRootNode() {
		return rootNode;
	}

	@Override
	public Node getGUINode() {
		return guiNode;
	}
}