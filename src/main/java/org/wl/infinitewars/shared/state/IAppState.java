package org.wl.infinitewars.shared.state;

import com.jme3.app.state.AppState;
import com.jme3.scene.Node;

public interface IAppState extends AppState {
	
	Node getRootNode();
	
	Node getGUINode();
}