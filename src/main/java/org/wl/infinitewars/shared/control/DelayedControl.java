package org.wl.infinitewars.shared.control;

import org.wl.infinitewars.shared.event.EventBus;

import com.jme3.scene.control.AbstractControl;

public abstract class DelayedControl extends AbstractControl {

	protected final EventBus eventBus = EventBus.get();

	protected final float delayTime;
	protected float lastUpdate;

	public DelayedControl(float delayTime) {
		this.delayTime = delayTime;
	}

	protected abstract void onUpdate(float elapsedTime);

	@Override
	protected void controlUpdate(float tpf) {
		lastUpdate += tpf;

		if (lastUpdate >= delayTime) {
			onUpdate(lastUpdate);
			lastUpdate = 0;
		}
	}
}