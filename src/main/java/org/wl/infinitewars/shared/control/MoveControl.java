package org.wl.infinitewars.shared.control;

import org.wl.infinitewars.shared.config.DebugConfigConstants;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedEvent;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedHandler;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.util.CheckUtils;
import org.wl.infinitewars.shared.util.PIDController;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class MoveControl extends AbstractControl {

	protected final EventBus eventBus = EventBus.get();

	protected final MovementInfo moveInfo = new MovementInfo();

	protected RigidBodyControl bodyControl;

	protected final SpeedMeasureControl forwardSpeedMeasureControl = new SpeedMeasureControl(RotationConstants.FORWARD);
	protected final SpeedMeasureControl strafeSpeedMeasureControl = new SpeedMeasureControl(RotationConstants.LEFT);
	protected final PIDController forwardPidController = new PIDController(10, 0f, 0.05f);

	protected boolean logData;

	public MoveControl() {
		super();
	}

	public MoveControl(RigidBodyControl bodyControl) {
		this();
		this.bodyControl = bodyControl;

		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				if (event.getDebugCommandKey().equals(DebugConfigConstants.dbg_toggle_log_player_infos)) {
					String[] arguments = event.getArguments();

					if (arguments.length == 2) {
						String playerName = arguments[0];
						String logType = arguments[1];
						Player player = spatial.getUserData(EntityConstants.player);

						if (player.getName().equals(playerName) && logType.equals(DebugConfigConstants.dbg_toggle_log_player_info_type_speed)) {
							logData = !logData;
						}
					}
				}
			}
		});
	}

	@Override
	public void controlUpdate(float tpf) {
		applyForwardMovement(tpf);
		roll(tpf);
		rotate(tpf);
	}

	private void applyForwardMovement(float tpf) {
		calculateMovement(tpf, moveInfo.getForwardSpeedPercentage(), getMaxForwardSpeed(), RotationConstants.FORWARD, getCurrentSpeed());

		float strafeLeft = moveInfo.isStrafeLeft() ? 1f : 0f;
		float strafeRight = moveInfo.isStrafeRight() ? 1f : 0f;
		calculateMovement(tpf, strafeLeft, getMaxStrafeSpeed(), RotationConstants.LEFT, strafeSpeedMeasureControl.getCurrentSpeed());
		calculateMovement(tpf, strafeRight, -getMaxStrafeSpeed(), RotationConstants.LEFT, strafeSpeedMeasureControl.getCurrentSpeed());
	}

	private void calculateMovement(float tpf, float movePercentage, float maxValue, int rotationColumn, float currentSpeed) {
		float targetSpeed = movePercentage * maxValue;
		float diff = targetSpeed - currentSpeed;

		if (FastMath.abs(diff) > 0.3) {
			Vector3f newForce = null;
			Vector3f moveDirection = bodyControl.getPhysicsRotation().getRotationColumn(rotationColumn).normalize();
			Vector3f targetSpeedVector = moveDirection.mult(targetSpeed);

			if (diff > 0f) {
				Vector3f error = targetSpeedVector.subtract(bodyControl.getLinearVelocity());
				CheckUtils.checkVectorForZeroValues(error);
				newForce = forwardPidController.getCorrection(tpf, error, false);
			} else {
				Vector3f error = bodyControl.getLinearVelocity().subtract(targetSpeedVector);
				CheckUtils.checkVectorForZeroValues(error);
				newForce = forwardPidController.getCorrection(tpf, error, false);
				newForce.negateLocal();
			}

			newForce.multLocal(getAcceleration() * tpf);

			CheckUtils.checkVectorForZeroValues(newForce);
			bodyControl.applyCentralForce(newForce);

			if (logData) {
				// System.out.println("maxSpeed: " + getMaxForwardSpeed());
				// System.out.println("currentSpeed: " + currentSpeed);
				// System.out.println("forwardSpeedPercentage: " + moveInfo.getForwardSpeedPercentage());
				// System.out.println("targetForwardSpeed: " + targetSpeed);
				// System.out.println("newForwardForce: " + newForce);
				// System.out.println("diff: " + diff);
				// System.out.println("newForwardForceLength: " + newForce.length());
				System.out.println("-------------------");
			}
		}
	}

	public float getCurrentSpeed() {
		return forwardSpeedMeasureControl.getCurrentSpeed();
	}

	private void roll(float tpf) {
		float rollSpeed = 0f;

		if (moveInfo.isRollLeft()) {
			rollSpeed = -getMaxRollSpeed();
		} else if (moveInfo.isRollRight()) {
			rollSpeed = getMaxRollSpeed();
		}

		if (rollSpeed != 0f) {
			Vector3f roll = bodyControl.getPhysicsRotation().getRotationColumn(RotationConstants.FORWARD).mult(rollSpeed);
			bodyControl.applyTorque(roll);
		}
	}

	private void rotate(float tpf) {
		if (logData) {
			System.out.println("x: " + moveInfo.getRotationPercentageX());
			System.out.println("y: " + moveInfo.getRotationPercentageY());
		}

		if (moveInfo.getRotationPercentageX() != 0f) {
			Vector3f yaw = bodyControl.getPhysicsRotation().getRotationColumn(RotationConstants.UP).mult(moveInfo.getRotationPercentageX() * getYawSpeed() * tpf);
			bodyControl.applyTorque(yaw);
		}

		if (moveInfo.getRotationPercentageY() != 0f) {
			Vector3f pitch = bodyControl.getPhysicsRotation().getRotationColumn(RotationConstants.LEFT).mult(moveInfo.getRotationPercentageY() * getPitchSpeed() * tpf);
			bodyControl.applyTorque(pitch);
		}
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);

		if (spatial != null) {
			spatial.addControl(forwardSpeedMeasureControl);
		}
	}

	protected float getAcceleration() {
		return spatial.getUserData(EntityConstants.acceleration);
	}

	public float getMaxForwardSpeed() {
		float maxForwardSpeed = spatial.getUserData(EntityConstants.maxForwardSpeed);

		if (moveInfo.isBoosting()) {
			float boostEnergy = spatial.getUserData(EntityConstants.boostEnergy);

			if (boostEnergy > 0f) {
				float maxBoostSpeed = spatial.getUserData(EntityConstants.maxBoostSpeed);
				maxForwardSpeed += maxBoostSpeed;
			}
		}

		return maxForwardSpeed;
	}

	protected float getMaxStrafeSpeed() {
		return spatial.getUserData(EntityConstants.maxStrafeSpeed);
	}

	protected float getMaxRollSpeed() {
		return spatial.getUserData(EntityConstants.maxRollSpeed);
	}

	protected float getPitchSpeed() {
		return spatial.getUserData(EntityConstants.maxPitchSpeed);
	}

	protected float getYawSpeed() {
		return spatial.getUserData(EntityConstants.maxYawSpeed);
	}

	public MovementInfo getMoveInfo() {
		return moveInfo;
	}

	public void setMoveInfo(MovementInfo moveInfo) {
		this.moveInfo.apply(moveInfo);
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}