package org.wl.infinitewars.shared.control;

public interface RotationConstants {

	int LEFT = 0;
	int UP = 1;
	int FORWARD = 2;
}