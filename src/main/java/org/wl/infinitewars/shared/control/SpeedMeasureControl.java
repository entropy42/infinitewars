package org.wl.infinitewars.shared.control;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;

public class SpeedMeasureControl extends DelayedControl {

	private static final float MEASURE_RATE = 60f;

	private final int rotationColumn;

	private RigidBodyControl bodyControl;
	private Vector3f lastLocation;
	private float currentSpeed;

	public SpeedMeasureControl(int rotationColumn) {
		super(1f / MEASURE_RATE);
		this.rotationColumn = rotationColumn;
	}

	@Override
	protected void onUpdate(float elapsedTime) {
		// float distance = bodyControl.getPhysicsLocation().distance(lastLocation);
		// lastLocation = bodyControl.getPhysicsLocation();
		// float newCurrentSpeed = distance * MEASURE_RATE;
		// float maxSpeed = spatial.getUserData(EntityConstants.maxForwardSpeed);
		//
		// if (newCurrentSpeed <= maxSpeed) {
		// if (currentSpeed > 0f) {
		// float diff = FastMath.abs(newCurrentSpeed - currentSpeed);
		//
		// if (diff < maxSpeed / 8f) {
		// currentSpeed = newCurrentSpeed;
		// }
		// } else {
		// currentSpeed = newCurrentSpeed;
		// }
		// }

		Vector3f linearVelocity = bodyControl.getLinearVelocity();

		if (linearVelocity.length() > 0.01f) {
			Vector3f forward = bodyControl.getPhysicsRotation().getRotationColumn(rotationColumn).normalize();
			float angle = FastMath.cos(forward.angleBetween(linearVelocity.normalize()));

			if (angle < 0.01f) {
				currentSpeed = linearVelocity.length();
			} else if (angle < FastMath.TWO_PI) {
				currentSpeed = angle * linearVelocity.length();
			} else {
				currentSpeed = 0f;
			}

			// System.out.println("linearVelocity: " + linearVelocity.length());
			// System.out.println("angle: " + angle);
			// System.out.println("currentSpeed: " + currentSpeed);
			// System.out.println("=============================");
		} else {
			currentSpeed = 0f;
		}
	}

	public float getCurrentSpeed() {
		return currentSpeed;
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);
		bodyControl = spatial.getControl(RigidBodyControl.class);
		lastLocation = bodyControl.getPhysicsLocation();
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}