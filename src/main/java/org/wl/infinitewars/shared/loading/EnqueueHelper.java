package org.wl.infinitewars.shared.loading;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public interface EnqueueHelper {
	
	Future<Void> enqueue(Callable<Void> callable);
}