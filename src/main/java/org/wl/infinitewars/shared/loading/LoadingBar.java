package org.wl.infinitewars.shared.loading;

import org.wl.infinitewars.shared.ui.EmptyContainer;
import org.wl.infinitewars.shared.ui.ImageButton;
import org.wl.infinitewars.shared.ui.ScreenUtils;
import org.wl.infinitewars.shared.util.FileConstants;

import tonegod.gui.core.ElementManager;

public class LoadingBar extends EmptyContainer {

	private final ImageButton panInner;
	private final float loadingPadding;
	private final boolean vertical;
	private final float maxHeight;
	private final float maxWidth;

	public LoadingBar(ElementManager screen, float width, float height, String innerBarImage) {
		this(screen, width, height, innerBarImage, false);
	}

	public LoadingBar(ElementManager screen, float width, float height, String innerBarImage, boolean vertical) {
		super(screen);
		this.vertical = vertical;

		setColorMap(FileConstants.UI_PATH + "colors/lightgrey.jpg");
		setDimensions(width, height);
		loadingPadding = ScreenUtils.getPadding(screen) / 4f;

		maxHeight = getHeight() - (loadingPadding / 2f);
		maxWidth = getWidth() - (loadingPadding / 2f);

		ImageButton panBackground = new ImageButton(screen, FileConstants.UI_PATH + "colors/white.jpg");
		panBackground.setDimensions(maxWidth, maxHeight);
		panBackground.setPosition(loadingPadding, loadingPadding);
		addChild(panBackground);

		panInner = new ImageButton(screen, FileConstants.LOADING_PATH + innerBarImage);
		panInner.setDimensions(maxWidth, maxHeight);
		panInner.setPosition(loadingPadding, loadingPadding);
		addChild(panInner);
	}

	public void updateProgress(float percentage) {
		if (vertical) {
			float height = percentage * maxHeight;
			panInner.setHeight(height);
		} else {
			float width = percentage * maxWidth;
			panInner.setWidth(width);
		}
	}
}