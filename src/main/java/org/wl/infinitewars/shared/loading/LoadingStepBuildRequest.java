package org.wl.infinitewars.shared.loading;

import org.wl.infinitewars.shared.json.MapData;

public class LoadingStepBuildRequest {

	private MapData mapData;

	public MapData getMapData() {
		return mapData;
	}

	public void setMapData(MapData mapData) {
		this.mapData = mapData;
	}
}