package org.wl.infinitewars.shared.loading;

import org.wl.infinitewars.shared.ui.GUIState;

public interface LoadingState extends GUIState {
	
	void updatePercentage(float percentage);
}