package org.wl.infinitewars.shared;

import org.wl.infinitewars.shared.gameplay.PhysicsWrapper;
import org.wl.infinitewars.shared.gameplay.PhysicsWrapperImpl;
import org.wl.infinitewars.shared.loading.EnqueueHelper;
import org.wl.infinitewars.shared.loading.EnqueueHelperImpl;
import org.wl.infinitewars.shared.loading.LoadingState;
import org.wl.infinitewars.shared.loading.LoadingStateImpl;
import org.wl.infinitewars.shared.state.fps.FPSState;
import org.wl.infinitewars.shared.state.fps.FPSStateImpl;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.input.InputManager;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.system.JmeContext;

import tonegod.gui.core.Screen;

public class SharedModule extends AbstractModule {

	protected final AbstractGameApplication app;

	public SharedModule(AbstractGameApplication app) {
		this.app = app;
	}

	@Override
	protected void configure() {
		bind(JmeContext.Type.class).toInstance(app.getContext().getType());
		bind(EnqueueHelper.class).toInstance(new EnqueueHelperImpl(app));
		bind(Screen.class).toInstance(new Screen(app));
		bind(ApplicationStopHelper.class).toInstance(new ApplicationStopHelper(app));

		bind(AppStateManager.class).toInstance(app.getStateManager());
		bind(AssetManager.class).toInstance(app.getAssetManager());
		bind(Camera.class).toInstance(app.getCamera());
		bind(InputManager.class).toInstance(app.getInputManager());
		bind(ViewPort.class).toInstance(app.getViewPort());
		bind(Node.class).annotatedWith(Names.named(NodeConstants.ROOT_NODE)).toInstance(app.getRootNode());
		bind(Node.class).annotatedWith(Names.named(NodeConstants.GUI_NODE)).toInstance(app.getGuiNode());

		bind(LoadingState.class).to(LoadingStateImpl.class);
		bind(FPSState.class).to(FPSStateImpl.class);

		bind(PhysicsWrapper.class).to(PhysicsWrapperImpl.class);
	}
}