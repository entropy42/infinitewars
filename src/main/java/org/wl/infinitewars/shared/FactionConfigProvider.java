package org.wl.infinitewars.shared;

import java.util.HashMap;

import org.wl.infinitewars.shared.json.Faction;
import org.wl.infinitewars.shared.json.FactionConfig;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class FactionConfigProvider extends HashMap<Faction, FactionConfig> {

	private static final long serialVersionUID = -3535222406369476717L;

	@Inject
	public FactionConfigProvider() {
		super(2);
	}
}