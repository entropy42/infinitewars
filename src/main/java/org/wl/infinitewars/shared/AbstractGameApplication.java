package org.wl.infinitewars.shared;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AppState;

public abstract class AbstractGameApplication extends SimpleApplication {

	public AbstractGameApplication() {
		super();
	}

	public AbstractGameApplication(AppState... initialStates) {
		super(initialStates);
	}
}