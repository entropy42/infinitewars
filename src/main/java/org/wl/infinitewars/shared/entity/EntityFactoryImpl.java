package org.wl.infinitewars.shared.entity;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.client.BoundingVolumeDisplayControl;
import org.wl.infinitewars.server.gameplay.DamageHistory;
import org.wl.infinitewars.shared.config.DebugConfigConstants;
import org.wl.infinitewars.shared.gameplay.TeamsContainer;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.gameplay.weapon.SpecialWeaponControl;
import org.wl.infinitewars.shared.gameplay.weapon.SpecialWeaponEnergyControl;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponControl;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireCreationData;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireFactory;
import org.wl.infinitewars.shared.json.EntityProperties;
import org.wl.infinitewars.shared.json.EntityProperty;
import org.wl.infinitewars.shared.json.Faction;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.json.WorldObjectData;
import org.wl.infinitewars.shared.util.FileConstants;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;
import com.jme3.asset.BlenderKey;
import com.jme3.asset.ModelKey;
import com.jme3.audio.AudioData.DataType;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingBox;
import com.jme3.bounding.BoundingSphere;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.CompoundCollisionShape;
import com.jme3.bullet.collision.shapes.MeshCollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.BatchNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.LodControl;
import com.jme3.system.JmeContext;

import jme3tools.optimize.LodGenerator;

public abstract class EntityFactoryImpl implements EntityFactory {

	private final Logger log = LoggerFactory.getLogger(getClass());

	protected AssetManager assetManager;
	protected EntityIdProvider entityIdProvider;
	protected TeamsContainer teamsContainer;
	protected JmeContext.Type jmeContextType;

	protected final Map<String, WeaponFireFactory> weaponFireFactories = new HashMap<>(5);
	protected final Map<String, BatchNode> batchNodes = new HashMap<>();

	public EntityFactoryImpl() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public Node createObject(WorldObjectData object) throws Exception {
		Vector3f location = object.getLocation().toVector();
		Quaternion rotation = object.getRotation().toQuaternion();
		Vector3f scale = object.getScale().toVector();
		Map<String, Object> data = object.getData();

		String name = object.getName();
		String propFilePath = FileConstants.MODELS_PATH_COMPLETE + name + FileConstants.MODEL_PROPERTY_FILE;
		EntityProperties properties = JsonHelper.get().toPOJO(propFilePath, EntityProperties.class);

		Node entity = createWorldObject(name, properties != null);

		if (properties != null) {
			Boolean isMotherShip = properties.getValue(EntityConstants.isMotherShip);

			if (BooleanUtils.isTrue(isMotherShip)) {
				prepareMotherShip(properties, entity);
			}
		}

		if (data != null) {
			for (Entry<String, Object> entry : data.entrySet()) {
				Object value = entry.getValue();

				if (value != null && value instanceof LinkedHashMap) {
					value = new HashMap<>((Map<String, Object>)value);
				}

				EntityHelper.changeProperty(entity, entry.getKey(), value, null);
			}

			String batchId = (String)data.get(EntityConstants.batchId);

			if (batchId != null) {
				BatchNode batchNode = batchNodes.get(batchId);

				if (batchNode == null) {
					batchNode = new BatchNode("BatchNode " + batchId);
					batchNodes.put(batchId, batchNode);
				}

				batchNode.attachChild(entity);
			}
		}

		entity.setLocalScale(scale);
		entity.setLocalTranslation(location);
		entity.setLocalRotation(rotation);

		updatePhysicsForEntity(location, rotation, scale, entity);

		BoundingVolumeDisplayControl debugControl = new BoundingVolumeDisplayControl(assetManager, DebugConfigConstants.dbg_toggle_show_bounding_volumes);
		entity.addControl(debugControl);

		return entity;
	}

	protected void updatePhysicsForEntity(Vector3f location, Quaternion rotation, Vector3f scale, Node entity) {
		RigidBodyControl control = entity.getControl(RigidBodyControl.class);
		CollisionShape collisionShape = control.getCollisionShape();
		collisionShape.setScale(scale);
		control.setCollisionShape(collisionShape);
		control.setPhysicsLocation(location);
		control.setPhysicsRotation(rotation);
	}

	protected void prepareMotherShip(EntityProperties properties, Node motherShip) {
		String factionString = properties.getValue(EntityConstants.faction);
		Faction faction = Faction.valueOf(factionString);
		motherShip.setUserData(EntityConstants.damageHistory, new DamageHistory());

		if (teamsContainer != null) {
			Team team = teamsContainer.getTeam(faction);
			team.setMotherShip(motherShip);
			motherShip.setUserData(EntityConstants.team, team);
		}
	}

	@Override
	public Node createEntity(String fileName) throws Exception {
		EntityCreationResult creationResult = create(fileName, true);
		return creationResult.getEntity();
	}

	@Override
	public Node createShip(String fileName) throws Exception {
		EntityCreationResult creationResult = create(fileName, true);

		Node ship = creationResult.getEntity();

		Mesh mesh = creationResult.getMesh();
		EntityProcessResult result = creationResult.getResult();

		mesh.setBound(new BoundingSphere());
		mesh.updateBound();

		ship.setModelBound(new BoundingSphere());
		ship.updateModelBound();

		if (!result.getStandardWeaponSlots().isEmpty()) {
			WeaponSlotContainer standardWeaponSlots = createWeaponSlotContainer(ship, EntityConstants.standard_weaponFirePrefix);
			ship.setUserData(EntityConstants.standardWeaponSlots, standardWeaponSlots);

			for (Vector3f weaponLocation : result.getStandardWeaponSlots()) {
				Node weaponNode = new Node("Standard weapon");
				weaponNode.addControl(new WeaponControl(EntityConstants.standard_weaponFireCoolDown));
				weaponNode.setLocalTranslation(weaponLocation);
				ship.attachChild(weaponNode);
				standardWeaponSlots.add(weaponNode);
			}
		}

		if (!result.getSpecialWeaponSlots().isEmpty()) {
			ship.addControl(new SpecialWeaponEnergyControl());

			WeaponSlotContainer specialWeaponSlots = createWeaponSlotContainer(ship, EntityConstants.special_weaponFirePrefix);
			ship.setUserData(EntityConstants.specialWeaponSlots, specialWeaponSlots);

			for (Vector3f weaponLocation : result.getSpecialWeaponSlots()) {
				Node weaponNode = new Node("Special weapon");
				weaponNode.addControl(new SpecialWeaponControl(EntityConstants.special_weaponFireCoolDown));
				weaponNode.setLocalTranslation(weaponLocation);
				ship.attachChild(weaponNode);
				specialWeaponSlots.add(weaponNode);
			}
		}

		if (!result.getLightningSlots().isEmpty()) {
			ship.setUserData(EntityConstants.lightningSlots, result.getLightningSlots());
		}

		if (!result.getNozzleSlots().isEmpty()) {
			ship.setUserData(EntityConstants.nozzleSlots, result.getNozzleSlots());
		}

		float radius = ((BoundingSphere)ship.getWorldBound()).getRadius();
		CollisionShape shape = new SphereCollisionShape(radius);

		float mass = ship.getUserData(EntityConstants.mass);
		RigidBodyControl bodyControl = new RigidBodyControl(shape, mass);
		bodyControl.setCollisionGroup(CollisionGroups.SMALL_SHIPS);
		bodyControl.addCollideWithGroup(CollisionGroups.LARGE_OBJECTS);
		bodyControl.addCollideWithGroup(CollisionGroups.SMALL_SHIPS);
		bodyControl.removeCollideWithGroup(CollisionGroups.WEAPON_FIRE);
		bodyControl.setFriction(0f);
		bodyControl.setAngularDamping(70f);
		bodyControl.setRestitution(0f);

		ship.addControl(bodyControl);

		BoundingVolumeDisplayControl debugControl = new BoundingVolumeDisplayControl(assetManager, DebugConfigConstants.dbg_toggle_show_bounding_volumes);
		ship.addControl(debugControl);

		return ship;
	}

	private WeaponSlotContainer createWeaponSlotContainer(Spatial ship, String prefix) {
		int weaponFireDamage = ship.getUserData(prefix + EntityConstants.weaponFireDamage);
		float weaponFireCoolDown = ship.getUserData(prefix + EntityConstants.weaponFireCoolDown);
		float weaponFireSpeed = ship.getUserData(prefix + EntityConstants.weaponFireSpeed);
		float weaponFireLifeTime = ship.getUserData(prefix + EntityConstants.weaponFireLifeTime);
		return new WeaponSlotContainer(weaponFireDamage, weaponFireCoolDown, weaponFireSpeed, weaponFireLifeTime);
	}

	protected EntityCreationResult create(String fileName, boolean loadProperties) throws Exception {
		log.info("Loading " + fileName);
		BlenderKey key = new BlenderKey(FileConstants.MODELS_PATH + fileName + FileConstants.MODEL_FILE);
		Spatial entity = assetManager.loadModel(key);

		Node node = null;

		if (entity instanceof Node) {
			node = (Node)entity;
		} else {
			node = new Node();
			node.attachChild(entity);
			entity = node;
		}

		prepareEntity(fileName, entity, loadProperties);

		EntityProcessResult result = new EntityProcessResult();
		processEntity(entity, result);

		Geometry mainGeom = result.getMainGeom();

		if (mainGeom == null) {
			mainGeom = result.getGeoms().get(0);
		}

		Mesh mesh = mainGeom.getMesh();

		result.getGeoms().forEach(geom -> {
			LodGenerator lodGen = new LodGenerator(geom);
			lodGen.bakeLods(LodGenerator.TriangleReductionMethod.PROPORTIONAL, 0.25f, 0.5f, 0.75f);
			geom.addControl(new LodControl());
		});

		EntityCreationResult creationResult = new EntityCreationResult(node, mesh, result);
		return creationResult;
	}

	protected void prepareEntity(String fileName, Spatial entity, boolean loadProperties) throws Exception {
		if (loadProperties) {
			entity.setUserData(EntityConstants.fileName, fileName);

			EntityProperties properties = JsonHelper.get().toPOJO(FileConstants.MODELS_PATH_COMPLETE + fileName + FileConstants.MODEL_PROPERTY_FILE, EntityProperties.class);

			for (EntityProperty prop : properties.getProperties()) {
				entity.setUserData(prop.getKey(), prop.getTypedValue());
			}
		}

		initEntityId(fileName, entity);
	}

	protected void initEntityId(String fileName, Spatial entity) {
		long nextId = entityIdProvider.getNextId();
		entity.setUserData(EntityConstants.id, nextId);
		entity.setName(fileName);
	}

	protected void processEntity(Spatial spatial, EntityProcessResult result) {
		String name = spatial.getName();

		if (spatial instanceof Node) {
			Node node = (Node)spatial;

			if (name.contains(EntityConstants.slot)) {
				Vector3f location = node.getLocalTranslation().clone();
				node.removeFromParent();

				if (name.contains(EntityConstants.standard_fireslot)) {
					result.addStandardWeaponSlot(location);
				} else if (name.contains(EntityConstants.special_fireslot)) {
					result.addSpecialWeaponSlot(location);
				} else if (name.contains(EntityConstants.lightning_slot)) {
					result.addLightningSlot(location);
				} else if (name.contains(EntityConstants.nozzle_slot)) {
					result.addNozzleSlot(location);
				}
			} else {
				for (Spatial child : node.getChildren()) {
					processEntity(child, result);
				}
			}
		} else {
			Geometry geom = (Geometry)spatial;
			result.addGeometry(geom);

			if (name.contains(EntityConstants.main_geom)) {
				result.setMainGeom(geom);
			}
		}
	}

	@Override
	public Node createWeaponFire(WeaponFireCreationData data) throws Exception {
		Spatial firingShip = data.getFiringShip();
		String weaponSlotsContainerName = data.getWeaponSlotsContainerName();

		String type = weaponSlotsContainerName.equals(EntityConstants.standardWeaponSlots) ? EntityConstants.standard_weaponFirePrefix : firingShip.getUserData(EntityConstants.special_weaponFireType);
		WeaponFireFactory weaponFireFactory = weaponFireFactories.get(type);

		Node weaponFire = weaponFireFactory.createWeaponFire(data);
		initEntityId("Weapon fire", weaponFire);

		return weaponFire;
	}

	@Override
	public Node createWorldObject(String fileName, boolean loadProperties) throws Exception {
		EntityCreationResult result = create(fileName, loadProperties);

		Node entity = result.getEntity();
		entity.setModelBound(new BoundingSphere());
		entity.updateModelBound();

		initPhysicsForEntity(fileName, result, entity);

		return entity;
	}

	protected void initPhysicsForEntity(String fileName, EntityCreationResult result, Node entity) {
		List<Geometry> geoms = result.getResult().getGeoms();

		CompoundCollisionShape shape = new CompoundCollisionShape();

		String lowPolyFile = FileConstants.MODELS_PATH + fileName + FileConstants.MODEL_FILE_LOW_POLY;
		Spatial lowPoly = null;

		try {
			ModelKey key = new BlenderKey(lowPolyFile);
			lowPoly = assetManager.loadModel(key);
		} catch (Exception e) {

		}

		if (lowPoly != null) {
			EntityProcessResult lowPolyResult = new EntityProcessResult();
			processEntity(lowPoly, lowPolyResult);

			for (Geometry geom : lowPolyResult.getGeoms()) {
				geom.setModelBound(new BoundingSphere());
				geom.updateModelBound();
				MeshCollisionShape child = new MeshCollisionShape(geom.getMesh());
				shape.addChildShape(child, geom.getLocalTranslation());
			}
		} else {
			for (Geometry geom : geoms) {
				geom.setModelBound(new BoundingSphere());
				geom.updateModelBound();
				MeshCollisionShape child = new MeshCollisionShape(geom.getMesh());
				shape.addChildShape(child, geom.getLocalTranslation());
			}
		}

		RigidBodyControl bodyControl = new RigidBodyControl(shape, 1000f);
		bodyControl.setCollisionGroup(CollisionGroups.LARGE_OBJECTS);
		bodyControl.addCollideWithGroup(CollisionGroups.LARGE_OBJECTS);
		bodyControl.addCollideWithGroup(CollisionGroups.SMALL_SHIPS);
		bodyControl.removeCollideWithGroup(CollisionGroups.WEAPON_FIRE);
		bodyControl.setKinematic(true);
		entity.addControl(bodyControl);
	}

	@Override
	public Spatial createExplosion() {
		Node node = new Node("Node for epxlosion");
		ParticleEmitter explosion = new ParticleEmitter("Explosion", Type.Triangle, 10);

		Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
		material.setTexture("Texture", assetManager.loadTexture(FileConstants.TEXTURES_PATH + "explosion1" + ".png"));

		explosion.setMaterial(material);
		explosion.setImagesX(4);
		explosion.setImagesY(4);
		explosion.setStartColor(new ColorRGBA(1f, 1f, 0f, 1.0f));
		explosion.setEndColor(new ColorRGBA(1f, 0f, 0f, 1f));
		explosion.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0, 0));
		explosion.setStartSize(0.5f);
		explosion.setEndSize(1.5f);
		explosion.setGravity(0, 0, 0);
		explosion.setLowLife(0.4f);
		explosion.setHighLife(1.1f);
		explosion.setRandomAngle(true);
		explosion.setSelectRandomImage(true);
		explosion.getParticleInfluencer().setVelocityVariation(0.0f);
		explosion.setParticlesPerSec(10f);

		explosion.addControl(new ExplosionControl(1f));
		explosion.setModelBound(new BoundingBox());
		explosion.updateModelBound();

		node.attachChild(explosion);

		AudioNode audioNode = new AudioNode(assetManager, "assets/sounds/explosion1.ogg", DataType.Buffer);
		audioNode.setPositional(true);
		node.attachChild(audioNode);
		audioNode.play();

		return node;
	}

	@Override
	public void finishBatchNodes() {
		batchNodes.values().forEach(b -> b.batch());
	}

	@Inject
	public void setAssetManager(AssetManager assetManager) {
		this.assetManager = assetManager;
	}

	@Inject
	public void setEntityIdProvider(EntityIdProvider entityIdProvider) {
		this.entityIdProvider = entityIdProvider;
	}

	@Inject
	public void setTeamsContainer(TeamsContainer teamsContainer) {
		this.teamsContainer = teamsContainer;
	}

	@Inject
	public void setJmeContextType(JmeContext.Type jmeContextType) {
		this.jmeContextType = jmeContextType;
	}

	@Inject
	public void setWeaponFireFactories(Set<WeaponFireFactory> factories) {
		for (WeaponFireFactory factory : factories) {
			weaponFireFactories.put(factory.getWeaponFireFactoryId(), factory);
		}
	}
}