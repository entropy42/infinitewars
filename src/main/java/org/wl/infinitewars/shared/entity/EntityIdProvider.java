package org.wl.infinitewars.shared.entity;

public interface EntityIdProvider {
	
	long getNextId();
}