package org.wl.infinitewars.shared.entity;

import java.io.IOException;
import java.util.ArrayList;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public class WeaponSlotContainer extends ArrayList<Node> implements Savable {

	private static final long serialVersionUID = -2973657010207219077L;

	private final int weaponFireDamage;
	private final float weaponFireCoolDown;
	private final float weaponFireSpeed;
	private final float weaponFireLifeTime;

	public WeaponSlotContainer(int weaponFireDamage, float weaponFireCoolDown, float weaponFireSpeed, float weaponFireLifeTime) {
		this.weaponFireDamage = weaponFireDamage;
		this.weaponFireCoolDown = weaponFireCoolDown;
		this.weaponFireSpeed = weaponFireSpeed;
		this.weaponFireLifeTime = weaponFireLifeTime;
	}

	public Vector3f calculateInBetweenPoint() {
		Vector3f weapon1 = get(0).getWorldTranslation();
		Vector3f weapon2 = get(1).getWorldTranslation();

		Vector3f dir = weapon1.subtract(weapon2);
		return weapon2.add(dir.divide(2f));
	}

	@Override
	public void write(JmeExporter ex) throws IOException {

	}

	@Override
	public void read(JmeImporter im) throws IOException {

	}

	public int getWeaponFireDamage() {
		return weaponFireDamage;
	}

	public float getWeaponFireCoolDown() {
		return weaponFireCoolDown;
	}

	public float getWeaponFireSpeed() {
		return weaponFireSpeed;
	}

	public float getWeaponFireLifeTime() {
		return weaponFireLifeTime;
	}
}