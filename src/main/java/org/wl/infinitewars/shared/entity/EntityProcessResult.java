package org.wl.infinitewars.shared.entity;

import java.util.ArrayList;
import java.util.List;

import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;

public class EntityProcessResult {

	private final List<Geometry> geoms = new ArrayList<>();
	private final List<Vector3f> standardWeaponSlots = new ArrayList<>();
	private final List<Vector3f> specialWeaponSlots = new ArrayList<>();
	private final List<Vector3f> lightningSlots = new ArrayList<>();
	private final List<Vector3f> nozzleSlots = new ArrayList<>();
	private Geometry mainGeom;

	public void addStandardWeaponSlot(Vector3f slot) {
		standardWeaponSlots.add(slot);
	}

	public void addSpecialWeaponSlot(Vector3f slot) {
		specialWeaponSlots.add(slot);
	}

	public void addGeometry(Geometry geom) {
		geoms.add(geom);
	}

	public List<Geometry> getGeoms() {
		return geoms;
	}

	public Geometry getMainGeom() {
		return mainGeom;
	}

	public void setMainGeom(Geometry mainGeom) {
		this.mainGeom = mainGeom;
	}

	public List<Vector3f> getStandardWeaponSlots() {
		return standardWeaponSlots;
	}

	public List<Vector3f> getSpecialWeaponSlots() {
		return specialWeaponSlots;
	}

	public void addLightningSlot(Vector3f location) {
		lightningSlots.add(location);
	}

	public List<Vector3f> getLightningSlots() {
		return lightningSlots;
	}

	public void addNozzleSlot(Vector3f location) {
		nozzleSlots.add(location);
	}

	public List<Vector3f> getNozzleSlots() {
		return nozzleSlots;
	}
}