package org.wl.infinitewars.shared.entity;

import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.event.gameplay.DestroyShipEvent;
import org.wl.infinitewars.shared.event.gameplay.DestroyShipHandler;
import org.wl.infinitewars.shared.event.gameplay.RespawnPlayerEvent;
import org.wl.infinitewars.shared.event.gameplay.RespawnPlayerHandler;
import org.wl.infinitewars.shared.event.main.RemovePlayerEvent;
import org.wl.infinitewars.shared.event.main.RemovePlayerHandler;
import org.wl.infinitewars.shared.gameplay.IngameState;
import org.wl.infinitewars.shared.gameplay.PhysicsWrapper;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.bullet.control.PhysicsControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.BatchNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

@Singleton
public class EntityAttacher {

	private final Logger log = LoggerFactory.getLogger(getClass());

	protected final EventBus eventBus = EventBus.get();

	protected final Node sceneNode;
	protected final AbstractSyncState syncState;
	protected final PhysicsWrapper physicsWrapper;

	protected final Random random = new Random();

	@Inject
	public EntityAttacher(IngameState ingameState, AbstractSyncState syncState, PhysicsWrapper physicsWrapper) {
		this.sceneNode = ingameState.getRootNode();
		this.syncState = syncState;
		this.physicsWrapper = physicsWrapper;

		init();
	}

	private void init() {
		eventBus.addHandler(RespawnPlayerEvent.TYPE, new RespawnPlayerHandler() {
			@Override
			public void onRespawnPlayer(RespawnPlayerEvent event) {
				Player player = event.getPlayer();
				long spawnPointId = player.getSpawnPointEntityId();

				if (spawnPointId > 0L) {
					Spatial spawnPoint = syncState.getEntity(spawnPointId);

					if (spawnPoint != null) {
						Vector3f spawnPointPosition = spawnPoint.getLocalTranslation();
						Map<String, Object> direction = spawnPoint.getUserData(EntityConstants.controlPointSpawnDirection);
						Vector3f directionVector = new Vector3f();
						directionVector.x = ((Double)direction.get("x")).floatValue();
						directionVector.y = ((Double)direction.get("y")).floatValue();
						directionVector.z = ((Double)direction.get("z")).floatValue();

						Node ship = player.getShip();

						RigidBodyControl bodyControl = ship.getControl(RigidBodyControl.class);
						ship.rotateUpTo(Vector3f.UNIT_Y);

						while (true) {
							float x = random.nextInt(60);
							float y = random.nextInt(60);
							float z = random.nextInt(60);
							Vector3f randomVector = new Vector3f(x, y, z);
							Vector3f location = spawnPointPosition.add(randomVector);

							if (!ship.getWorldBound().intersects(location)) {
								bodyControl.setPhysicsLocation(location);
								break;
							}
						}

						Quaternion physicsRotation = bodyControl.getPhysicsRotation();
						physicsRotation.lookAt(directionVector, Vector3f.UNIT_Y);
						bodyControl.setPhysicsRotation(physicsRotation);

						attachEntity(ship);
					} else {
						log.error("No entity found for spawn point id " + spawnPointId + "!");
					}
				} else {
					log.error("Player " + player.getPlayerId() + " has no spawn point!");
				}
			}
		});

		eventBus.addHandler(RemovePlayerEvent.TYPE, new RemovePlayerHandler() {
			@Override
			public void onRemovePlayer(RemovePlayerEvent event) {
				Node ship = event.getPlayer().getShip();

				if (ship != null) {
					removeEntity(ship);
				}
			}
		});

		eventBus.addHandler(DestroyShipEvent.TYPE, new DestroyShipHandler() {
			@Override
			public void onDestroyShip(DestroyShipEvent event) {
				Spatial entity = event.getEntity();

				RigidBodyControl bodyControl = entity.getControl(RigidBodyControl.class);

				if (bodyControl != null) {
					bodyControl.setLinearVelocity(new Vector3f());
					bodyControl.setAngularVelocity(new Vector3f());
					bodyControl.clearForces();
				}

				removeEntity(entity);
			}
		});
	}

	public void attachEntity(Spatial entity) {
		Node parent = entity.getParent();

		if (parent != null && parent instanceof BatchNode) {
			sceneNode.attachChild(parent);
		} else {
			sceneNode.attachChild(entity);
		}

		if (isPhysicsEntity(entity)) {
			physicsWrapper.addEntity(entity);

			if (isSyncEntity(entity)) {
				syncState.addEntity(entity);
			}
		}
	}

	public void removeEntity(Spatial entity) {
		sceneNode.detachChild(entity);

		if (isPhysicsEntity(entity)) {
			physicsWrapper.removeEntity(entity);

			if (isSyncEntity(entity)) {
				syncState.removeEntity(entity);
			}
		}
	}

	private boolean isPhysicsEntity(Spatial entity) {
		PhysicsControl control = entity.getControl(PhysicsControl.class);
		return control != null;
	}

	private boolean isSyncEntity(Spatial entity) {
		Boolean sync = entity.getUserData(EntityConstants.sync);
		return BooleanUtils.isTrue(sync);
	}
}