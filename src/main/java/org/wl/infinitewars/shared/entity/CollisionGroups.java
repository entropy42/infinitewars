package org.wl.infinitewars.shared.entity;

import com.jme3.bullet.collision.PhysicsCollisionObject;

public interface CollisionGroups {

	int WEAPON_FIRE = PhysicsCollisionObject.COLLISION_GROUP_01;
	int LARGE_OBJECTS = PhysicsCollisionObject.COLLISION_GROUP_02;
	int SMALL_SHIPS = PhysicsCollisionObject.COLLISION_GROUP_03;
}