package org.wl.infinitewars.shared.entity;

import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeEvent;
import org.wl.infinitewars.shared.gameplay.EntityPropertyChangeCause;

import com.jme3.scene.Spatial;

public class EntityHelper {
	
	public static long getEntityId(Spatial entity) {
		Long id = entity.getUserData(EntityConstants.id);
		return id.longValue();
	}
	
	public static void changeProperty(Spatial entity, String key, Object value, EntityPropertyChangeCause cause) {
		Object oldValue = entity.getUserData(key);
		entity.setUserData(key, value);
		
		long entityId = getEntityId(entity);
		EventBus.get().fireEvent(new EntityPropertyChangeEvent(entity, entityId, key, value, oldValue, cause));
	}
}