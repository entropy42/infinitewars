package org.wl.infinitewars;

import org.wl.infinitewars.client.ClientApp;
import org.wl.infinitewars.server.ServerApp;

import com.jme3.system.JmeContext;

public class Main {

	public static void main(String[] args) {
		try {
			if (args.length == 1) {
				String arg = args[0];

				if (arg.equals("-server")) {
					startServer();
				} else {
					startClient();
				}
			} else {
				startClient();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void startClient() {
		ClientApp client = new ClientApp(null, JmeContext.Type.Display);
		client.start(JmeContext.Type.Display);
	}

	private static void startServer() throws Exception {
		ServerApp app = new ServerApp();
		app.start(JmeContext.Type.Headless);
	}
}