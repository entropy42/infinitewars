package org.wl.infinitewars.server.network.messagehandler;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.server.event.gameplay.ShootEvent;
import org.wl.infinitewars.server.event.gameplay.ShootHandler;
import org.wl.infinitewars.server.gameplay.weapon.ServerWeaponFireControl;
import org.wl.infinitewars.shared.config.DebugConfigConstants;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityHelper;
import org.wl.infinitewars.shared.entity.WeaponSlotContainer;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedEvent;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedHandler;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponControl;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireCreationData;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.WeaponFireCreationMessage;
import org.wl.infinitewars.shared.network.messages.toserver.input.FireWeaponsMessage;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.network.HostedConnection;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class FireWeaponsMessageHandler extends ServerMessageHandler<FireWeaponsMessage> {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private boolean syncWeaponFireMovement;

	public FireWeaponsMessageHandler() {
		eventBus.addHandler(ShootEvent.TYPE, new ShootHandler() {
			@Override
			public void onShoot(ShootEvent event) {
				shoot(event.getDirection(), event.getPlayer(), event.getWeaponSlotsContainerName());
			}
		});

		syncWeaponFireMovement = true;

		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				if (event.getDebugCommandKey().equals(DebugConfigConstants.dbg_toggle_weapon_fire_movement)) {
					syncWeaponFireMovement = !syncWeaponFireMovement;
				}
			}
		});
	}

	@Override
	public Class<FireWeaponsMessage> getMessageClass() {
		return FireWeaponsMessage.class;
	}

	@Override
	public void handleMessage(HostedConnection conn, FireWeaponsMessage m) {
		int connectionId = conn.getId();

		Player player = playerManager.getPlayer(connectionId);

		if (player != null) {
			shoot(m.getDirection(), player, m.getWeaponSlotsContainerName());
		} else {
			noPlayerForConnection(connectionId);
		}
	}

	protected void shoot(Vector3f direction, Player player, String weaponSlotsContainerName) {
		final Node ship = player.getShip();

		if (ship != null) {
			WeaponSlotContainer weaponSlotContainer = ship.getUserData(weaponSlotsContainerName);

			if (weaponSlotContainer != null && !weaponSlotContainer.isEmpty()) {
				for (Node weapon : weaponSlotContainer) {
					WeaponControl weaponControl = weapon.getControl(WeaponControl.class);

					if (weaponControl.canFire()) {
						weaponControl.weaponFired();

						enqueueHelper.enqueue(new Callable<Void>() {
							@Override
							public Void call() throws Exception {
								fireWeapon(direction, ship, weapon, weaponSlotsContainerName, weaponSlotContainer);

								return null;
							}
						});
					}
				}
			} else {
				log.error("The player " + player.getPlayerId() + " does not have the weapon slots '" + weaponSlotsContainerName + "' on his ship!");
			}
		} else {
			log.error("The player " + player.getPlayerId() + " does not have a ship!");
		}
	}

	protected void fireWeapon(Vector3f direction, Node ship, Spatial weapon, String weaponSlotsContainerName, WeaponSlotContainer weaponSlotContainer) throws Exception {
		long shipId = EntityHelper.getEntityId(ship);

		Vector3f startPosWorld = new Vector3f();
		ship.localToWorld(weapon.getLocalTranslation().add(new Vector3f(0f, 0f, 10f)), startPosWorld);

		Spatial weaponFire = entityFactory.createWeaponFire(new WeaponFireCreationData(startPosWorld, ship, weaponSlotsContainerName, direction));

		float speed = weaponSlotContainer.getWeaponFireSpeed();
		int damage = weaponSlotContainer.getWeaponFireDamage();

		weaponFire.setUserData(EntityConstants.weaponFireDamage, damage);

		entityAttacher.attachEntity(weaponFire);

		Vector3f physicsStartLocation = startPosWorld.clone();

		// String weaponFireType = weaponFire.getUserData(EntityConstants.weaponFireType);

		// if (weaponFireType.equals(EntityConstants.special_weaponFireType_railGun)) {
		// Node parent = weaponFire.getParent();
		//
		// Ray ray = new Ray(physicsStartLocation, direction);
		// CollisionResults results = new CollisionResults();
		// parent.collideWith(ray, results);
		//
		// CollisionResult closestCollision = results.getClosestCollision();
		//
		// if (closestCollision != null) {
		// physicsStartLocation = closestCollision.getContactPoint();
		// }
		// }

		weaponFire.getControl(RigidBodyControl.class).setPhysicsLocation(physicsStartLocation);
		weaponFire.setLocalTranslation(physicsStartLocation);

		long weaponFireId = EntityHelper.getEntityId(weaponFire);

		float lifeTime = weaponSlotContainer.getWeaponFireLifeTime();

		if (syncWeaponFireMovement) {
			weaponFire.addControl(new ServerWeaponFireControl(lifeTime, speed, direction, weaponFireId, serverState, entityAttacher));
		}

		serverState.broadcast(new WeaponFireCreationMessage(startPosWorld, weaponFireId, direction, shipId, weaponSlotsContainerName));
	}
}