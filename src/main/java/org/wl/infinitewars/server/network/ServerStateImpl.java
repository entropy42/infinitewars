package org.wl.infinitewars.server.network;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.server.ai.BotFactory;
import org.wl.infinitewars.server.gameplay.DamageHistory;
import org.wl.infinitewars.server.gameplay.DamageHistoryEntry;
import org.wl.infinitewars.server.gameplay.EndTimerControl;
import org.wl.infinitewars.server.gameplay.ServerPlayersContainer;
import org.wl.infinitewars.shared.NodeConstants;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityHelper;
import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedEvent;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedHandler;
import org.wl.infinitewars.shared.event.gameplay.DestroyShipEvent;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeEvent;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeHandler;
import org.wl.infinitewars.shared.event.gameplay.PlayerPropertyChangeEvent;
import org.wl.infinitewars.shared.event.gameplay.PlayerPropertyChangeHandler;
import org.wl.infinitewars.shared.event.gameplay.RespawnPlayerEvent;
import org.wl.infinitewars.shared.event.gameplay.RespawnPlayerHandler;
import org.wl.infinitewars.shared.event.gameplay.WinConditionReachedEvent;
import org.wl.infinitewars.shared.event.gameplay.WinConditionReachedHandler;
import org.wl.infinitewars.shared.event.main.EndGameTimeUpdateEvent;
import org.wl.infinitewars.shared.event.main.EndGameTimeUpdateHandler;
import org.wl.infinitewars.shared.event.main.GameTimeUpdateEvent;
import org.wl.infinitewars.shared.event.main.GameTimeUpdateHandler;
import org.wl.infinitewars.shared.event.main.RemovePlayerEvent;
import org.wl.infinitewars.shared.event.main.RemovePlayerHandler;
import org.wl.infinitewars.shared.event.main.RestartGameEvent;
import org.wl.infinitewars.shared.gameplay.EntityPropertyCauseSync;
import org.wl.infinitewars.shared.gameplay.EntityPropertyChangeCause;
import org.wl.infinitewars.shared.gameplay.GameInfoProvider;
import org.wl.infinitewars.shared.gameplay.WeaponFireCause;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.loading.EnqueueHelper;
import org.wl.infinitewars.shared.network.messages.MessageRegistry;
import org.wl.infinitewars.shared.network.messages.toclient.RestartGameMessage;
import org.wl.infinitewars.shared.network.messages.toclient.config.DebugConfigChangedMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.EntityPropertyChangeMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.PlayerPropertyChangeMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.RespawnPlayerMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.SpectatedPlayerUpdateMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.WinConditionReachedMessage;
import org.wl.infinitewars.shared.network.messages.toclient.main.EndTimerUpdateMessage;
import org.wl.infinitewars.shared.network.messages.toclient.main.GameTimeUpdateMessage;
import org.wl.infinitewars.shared.network.messages.toclient.main.PlayerNameRequestMessage;
import org.wl.infinitewars.shared.state.AppStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.Server;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

@Singleton
public class ServerStateImpl extends AppStateImpl implements ServerState, ConnectionListener {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final EventBus eventBus = EventBus.get();

	private Server server;

	private final GameInfoProvider gameInfoProvider;
	private final ServerPlayersContainer playersContainer;
	private final BotFactory botFactory;
	private final EnqueueHelper enqueueHelper;
	private final ServerFactory serverFactory;

	private ServerMessageListener serverMessageListener;

	private final Map<Long, Long> spectatingPlayerData = new HashMap<>();

	@Inject
	public ServerStateImpl(GameInfoProvider gameInfoProvider, ServerPlayersContainer playersContainer, BotFactory botFactory, EnqueueHelper enqueueHelper, ServerFactory serverFactory, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) throws IOException {
		super(appRootNode, appGuiNode);
		this.gameInfoProvider = gameInfoProvider;
		this.playersContainer = playersContainer;
		this.botFactory = botFactory;
		this.enqueueHelper = enqueueHelper;
		this.serverFactory = serverFactory;

		init();
	}

	private void init() throws IOException {
		MessageRegistry.registerMessages();

		server = serverFactory.createServer();
		server.addConnectionListener(this);

		addEventHandler(RespawnPlayerEvent.TYPE, new RespawnPlayerHandler() {
			@Override
			public void onRespawnPlayer(RespawnPlayerEvent event) {
				respawn(event.getPlayer());
			}
		});

		addEventHandler(EntityPropertyChangeEvent.TYPE, new EntityPropertyChangeHandler() {
			@Override
			public void onChange(EntityPropertyChangeEvent event) {
				handleEntityPropertyChange(event);
			}
		});

		addEventHandler(RemovePlayerEvent.TYPE, new RemovePlayerHandler() {
			@Override
			public void onRemovePlayer(RemovePlayerEvent event) {
				removePlayer(event.getPlayer());
			}
		});

		addEventHandler(PlayerPropertyChangeEvent.TYPE, new PlayerPropertyChangeHandler() {
			@Override
			public void onChange(PlayerPropertyChangeEvent event) {
				server.broadcast(new PlayerPropertyChangeMessage(event.getPlayer().getPlayerId(), event.getPropertyName(), event.getPropertyValue()));
			}
		});

		addEventHandler(WinConditionReachedEvent.TYPE, new WinConditionReachedHandler() {
			@Override
			public void onWinConditionReached(WinConditionReachedEvent event) {
				server.broadcast(new WinConditionReachedMessage(event.getWinnerTeam().getTeamId()));
				rootNode.addControl(new EndTimerControl(5));
			}
		});

		addEventHandler(EndGameTimeUpdateEvent.TYPE, new EndGameTimeUpdateHandler() {
			@Override
			public void onUpdateGameTime(EndGameTimeUpdateEvent event) {
				final int secondsLeft = event.getSecondsLeft();
				server.broadcast(new EndTimerUpdateMessage(secondsLeft));

				if (secondsLeft == 0) {
					server.broadcast(new RestartGameMessage());
					eventBus.fireEvent(new RestartGameEvent());
				}
			}
		});

		addEventHandler(GameTimeUpdateEvent.TYPE, new GameTimeUpdateHandler() {
			@Override
			public void onGameTimeUpdate(GameTimeUpdateEvent event) {
				server.broadcast(new GameTimeUpdateMessage(event.getCurrentTime()));
			}
		});

		addEventHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				server.broadcast(new DebugConfigChangedMessage(event.getDebugCommandKey(), event.getArguments()));
			}
		});
	}

	private void respawn(Player player) {
		player.setRespawned();

		final Node ship = player.getShip();
		final int maxHitPoints = ship.getUserData(EntityConstants.maxHitPoints);
		EntityHelper.changeProperty(ship, EntityConstants.hitPoints, maxHitPoints, null);

		final int maxShields = ship.getUserData(EntityConstants.maxShields);
		EntityHelper.changeProperty(ship, EntityConstants.shields, maxShields, null);

		broadcast(new RespawnPlayerMessage(player.getPlayerId()));
	}

	private void removePlayer(Player player) {
		final Team team = player.getTeam();
		team.removePlayer(player);
		player.detachPlayerControls();
	}

	private void handleEntityPropertyChange(EntityPropertyChangeEvent event) {
		final Spatial entity = event.getEntity();
		final long entityId = event.getEntityId();
		final String propertyName = event.getPropertyName();
		final Object propertyValue = event.getPropertyValue();
		final EntityPropertyChangeCause cause = event.getCause();

		EntityPropertyCauseSync createCauseForMessage = null;

		if (cause != null) {
			createCauseForMessage = cause.createCauseForMessage();
		}

		server.broadcast(new EntityPropertyChangeMessage(entityId, propertyName, propertyValue, createCauseForMessage));

		if (propertyName.equals(EntityConstants.hitPoints)) {
			final int currentHitPoints = (int)propertyValue;
			final int oldHitPoints = (int)event.getOldPropertyValue();

			if (oldHitPoints < currentHitPoints) {
				updateDamageHistory(entity, currentHitPoints - oldHitPoints);
			}

			if (oldHitPoints > 0 && currentHitPoints <= 0) {
				eventBus.fireEvent(new DestroyShipEvent(entity));

				final Player player = entity.getUserData(EntityConstants.player);

				if (player != null) {
					player.setWantsRespawn(player.isBot());
					player.setAlive(false);
					player.addDeath();

					if (cause != null && cause instanceof WeaponFireCause) {
						final WeaponFireCause weaponFireCause = (WeaponFireCause)cause;
						final Spatial firingShip = weaponFireCause.getWeaponFire().getUserData(EntityConstants.weaponFireFiringShip);

						final Player firingPlayer = firingShip.getUserData(EntityConstants.player);

						if (firingPlayer != null) {
							firingPlayer.addKill();
						}
					}
				}
			}
		} else if (propertyName.equals(EntityConstants.shields)) {
			final int currentShields = (int)propertyValue;
			final int oldShields = (int)event.getOldPropertyValue();

			if (oldShields < currentShields) {
				updateDamageHistory(entity, currentShields - oldShields);
			}
		}
	}

	private void updateDamageHistory(final Spatial entity, final int diff) {
		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				final DamageHistory damageHistory = entity.getUserData(EntityConstants.damageHistory);

				int d = diff;

				while (!damageHistory.isEmpty()) {
					final DamageHistoryEntry first = damageHistory.getFirst();

					final int damage = first.getDamage();

					if (damage > d) {
						first.setDamage(damage - d);
						break;
					}

					d -= damage;
					damageHistory.removeFirst();
				}

				return null;
			}
		});
	}

	@Override
	public void startServer() {
		server.start();
	}

	@Override
	public void connectionAdded(Server server, HostedConnection conn) {
		log.info("New player connecting...");
		conn.send(new PlayerNameRequestMessage());
	}

	@Override
	public void connectionRemoved(Server server, HostedConnection conn) {
		final int connectionId = conn.getId();
		final Player player = playersContainer.removePlayer(connectionId);

		if (player != null) {
			synchronized (spectatingPlayerData) {
				spectatingPlayerData.remove(player.getPlayerId());
			}

			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					eventBus.fireEvent(new RemovePlayerEvent(player));

					if (gameInfoProvider.getGameInfo().isEnableBots()) {
						final Team team = player.getTeam();

						try {
							botFactory.createBot("Bot" + team.getPlayerCount(), team);
						} catch (final Exception e) {
							e.printStackTrace();
						}
					}

					return null;
				}
			});
		} else {
			log.warn("No player found for connection " + connectionId + "!");
		}
	}

	@Override
	public void setServerMessageListener(ServerMessageListener serverMessageListener) {
		if (this.serverMessageListener != null) {
			server.removeMessageListener(serverMessageListener);
		}

		this.serverMessageListener = serverMessageListener;
		server.addMessageListener(serverMessageListener);
	}

	@Override
	public void shutDown() {
		if (server.isRunning()) {
			server.close();
		}
	}

	@Override
	public void broadcast(Message message) {
		server.broadcast(message);
	}

	@Override
	public void sendPlayerNameRequestMessages() {
		Collection<HostedConnection> connections = server.getConnections();

		for (HostedConnection conn : connections) {
			conn.send(new PlayerNameRequestMessage());
		}
	}

	@Override
	public void update(float tpf) {
		super.update(tpf);

		synchronized (spectatingPlayerData) {
			for (Entry<Long, Long> entry : spectatingPlayerData.entrySet()) {
				Long playerId = entry.getKey();
				Long spectatedPlayerId = entry.getValue();

				Integer connectionId = playersContainer.getConnectionId(playerId);

				if (connectionId != null) {
					HostedConnection connection = server.getConnection(connectionId);
					Player player = playersContainer.getPlayer(connectionId);

					Player spectatedPlayer = player.getTeam().getPlayer(spectatedPlayerId);

					if (spectatedPlayer != null) {
						Player target = spectatedPlayer.getTarget();
						long targetPlayerId = 0L;

						if (target != null) {
							targetPlayerId = target.getPlayerId();
						}

						connection.send(new SpectatedPlayerUpdateMessage(spectatedPlayerId, targetPlayerId));
					}
				} else {
					log.error("No connection found for player " + playerId + "!");
				}
			}
		}
	}

	@Override
	public void changeSpectatedPlayer(long playerId, long spectatedPlayerId) {
		spectatingPlayerData.put(playerId, spectatedPlayerId);
	}
}