package org.wl.infinitewars.server.network.messagehandler;

import org.wl.infinitewars.server.gameplay.ServerPlayerFactory;
import org.wl.infinitewars.server.gameplay.ServerPlayersContainer;
import org.wl.infinitewars.server.network.ServerState;
import org.wl.infinitewars.shared.FactionConfigProvider;
import org.wl.infinitewars.shared.gameplay.GameInfoProvider;
import org.wl.infinitewars.shared.network.AbstractMessageHandler;

import com.google.inject.Inject;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;

public abstract class ServerMessageHandler<M extends Message> extends AbstractMessageHandler<M, HostedConnection> {

	protected ServerState serverState;
	protected ServerPlayerFactory playerFactory;
	protected ServerPlayersContainer playerManager;
	protected GameInfoProvider gameInfoProvider;
	protected FactionConfigProvider factionConfigs;

	@Inject
	public void setServerState(ServerState serverState) {
		this.serverState = serverState;
	}

	@Inject
	public void setPlayerFactory(ServerPlayerFactory playerFactory) {
		this.playerFactory = playerFactory;
	}

	@Inject
	public void setPlayerManager(ServerPlayersContainer playerManager) {
		this.playerManager = playerManager;
	}

	@Inject
	public void setGameInfoProvider(GameInfoProvider gameInfoProvider) {
		this.gameInfoProvider = gameInfoProvider;
	}

	@Inject
	public void setFactionConfigs(FactionConfigProvider factionConfigs) {
		this.factionConfigs = factionConfigs;
	}
}