package org.wl.infinitewars.server.network.messagehandler;

import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.server.network.GameInfoInitDataFactory;
import org.wl.infinitewars.shared.gameplay.GameInfoInitData;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.network.messages.toclient.main.GameInfoInitMessage;
import org.wl.infinitewars.shared.network.messages.toserver.main.PlayerNameResponseMessage;

import com.google.inject.Inject;
import com.jme3.network.HostedConnection;

public class PlayerNameResponseMessageHandler extends ServerMessageHandler<PlayerNameResponseMessage> {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private GameInfoInitDataFactory gameInfoInitDataFactory;

	@Override
	public Class<PlayerNameResponseMessage> getMessageClass() {
		return PlayerNameResponseMessage.class;
	}

	@Override
	public void handleMessage(final HostedConnection conn, PlayerNameResponseMessage m) {
		final String playerName = m.getPlayerName();

		if (StringUtils.isNotBlank(playerName)) {
			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					log.info("Adding new Player '" + playerName + "'...");
					Team team = teamsContainer.getTeamForNextPlayer();
					Player player = playerFactory.createPlayer(playerName, team, false, teamsContainer.getEnemyTeam(team));
					playerManager.addPlayer(conn.getId(), player);

					GameInfoInitData data = gameInfoInitDataFactory.createGameInfoInitData(player);
					conn.send(new GameInfoInitMessage(data));

					return null;
				}
			});
		} else {
			log.error("The given playerName of the PlayerNameResponseMessage was empty!");
			conn.close("The given playerName of the PlayerNameResponseMessage was empty!");
		}
	}

	@Inject
	public void setGameInfoInitDataFactory(GameInfoInitDataFactory gameInfoInitDataFactory) {
		this.gameInfoInitDataFactory = gameInfoInitDataFactory;
	}
}