package org.wl.infinitewars.server.network;

import java.io.IOException;

import com.jme3.network.Server;

public interface ServerFactory {
	
	Server createServer() throws IOException;
}