package org.wl.infinitewars.server.network;

import com.jme3.app.state.AppState;
import com.jme3.network.Message;

public interface ServerState extends AppState {

	void setServerMessageListener(ServerMessageListener serverMessageListener);

	void shutDown();

	void broadcast(Message message);

	void startServer();

	void sendPlayerNameRequestMessages();

	void changeSpectatedPlayer(long playerId, long spectatedPlayerId);
}