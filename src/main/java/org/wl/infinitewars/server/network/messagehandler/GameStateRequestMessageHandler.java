package org.wl.infinitewars.server.network.messagehandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityHelper;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.network.messages.toclient.gamestate.GameStateResponseMessage;
import org.wl.infinitewars.shared.network.messages.toclient.gamestate.PropertyInitEntry;
import org.wl.infinitewars.shared.network.messages.toserver.GameStateRequestMessage;
import org.wl.infinitewars.shared.util.DebugState;

import com.google.common.collect.Sets;
import com.jme3.network.HostedConnection;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class GameStateRequestMessageHandler extends ServerMessageHandler<GameStateRequestMessage> {

	public static final Set<String> synchronizedEntityPropertyKeys = Sets.newHashSet(
			EntityConstants.shields,
			EntityConstants.hitPoints,
			EntityConstants.faction,
			EntityConstants.controlPointCapturePointsTeam1,
			EntityConstants.controlPointCapturePointsTeam2);

	@Override
	public Class<GameStateRequestMessage> getMessageClass() {
		return GameStateRequestMessage.class;
	}

	@Override
	public void handleMessage(HostedConnection conn, GameStateRequestMessage m) {
		Player player = playerManager.getPlayer(conn.getId());

		if (player == null) {
			noPlayerForConnection(conn.getId());
		} else {
			Team team1 = teamsContainer.getTeam1();
			Team team2 = teamsContainer.getTeam2();

			int playerCount = team1.getPlayerCount() + team2.getPlayerCount();

			List<Long> alivePlayers = new ArrayList<>(playerCount);
			addAlivePlayersIds(team1, alivePlayers);
			addAlivePlayersIds(team2, alivePlayers);
			Long[] alivePlayersArray = alivePlayers.toArray(new Long[alivePlayers.size()]);

			List<PropertyInitEntry> playaerPropertiesList = new ArrayList<>(playerCount);
			addPlayerProperties(team1, playaerPropertiesList);
			addPlayerProperties(team2, playaerPropertiesList);
			PropertyInitEntry[] playerProperties = playaerPropertiesList.toArray(new PropertyInitEntry[playaerPropertiesList.size()]);

			List<PropertyInitEntry> list = new ArrayList<>(team1.getActiveSpawnPoints().size() + team2.getActiveSpawnPoints().size());
			addEntityProperties(team1, list);
			addEntityProperties(team2, list);
			PropertyInitEntry[] entityProperties = list.toArray(new PropertyInitEntry[list.size()]);

			Map<String, String[]> debugCommands = DebugState.get().getCommandsArgumentsMap();
			Set<String> toggledCommands = DebugState.get().getToggledCommands();

			GameStateResponseMessage message = new GameStateResponseMessage(alivePlayersArray, playerProperties, entityProperties, debugCommands, toggledCommands);
			conn.send(message);
		}
	}

	protected void addEntityProperties(Team team, List<PropertyInitEntry> list) {
		Set<Spatial> activeSpawnPoints = team.getActiveSpawnPoints();

		for (Spatial point : activeSpawnPoints) {
			addEntityProperties(point, list);
		}
	}

	protected void addEntityProperties(Spatial spatial, List<PropertyInitEntry> list) {
		Collection<String> userDataKeys = spatial.getUserDataKeys();
		long entityId = EntityHelper.getEntityId(spatial);

		for (String key : userDataKeys) {
			if (synchronizedEntityPropertyKeys.contains(key)) {
				Object value = spatial.getUserData(key);
				PropertyInitEntry entry = new PropertyInitEntry(entityId, key, value);
				list.add(entry);
			}
		}
	}

	protected void addPlayerProperties(Team team, List<PropertyInitEntry> list) {
		List<Player> players = team.getPlayers();

		for (Player player : players) {
			Collection<PropertyInitEntry> entries = player.createInitEntries();
			list.addAll(entries);
		}
	}

	protected void addAlivePlayersIds(Team team, List<Long> alivePlayerIds) {
		for (Player player : team.getPlayers()) {
			Node ship = player.getShip();

			if (player.isAlive() && ship != null) {
				alivePlayerIds.add(player.getPlayerId());
			}
		}
	}
}