package org.wl.infinitewars.server.network;

import org.wl.infinitewars.server.network.messagehandler.ServerMessageHandler;
import org.wl.infinitewars.shared.network.AbstractMessageListener;

import com.jme3.network.HostedConnection;

@SuppressWarnings("rawtypes")
public class ServerMessageListener extends AbstractMessageListener<HostedConnection, ServerMessageHandler> {

}