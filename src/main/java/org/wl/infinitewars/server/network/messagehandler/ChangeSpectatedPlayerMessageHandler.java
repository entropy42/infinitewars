package org.wl.infinitewars.server.network.messagehandler;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.network.messages.toserver.gameplay.ChangeSpectatedPlayerMessage;

import com.jme3.network.HostedConnection;

public class ChangeSpectatedPlayerMessageHandler extends ServerMessageHandler<ChangeSpectatedPlayerMessage> {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public Class<ChangeSpectatedPlayerMessage> getMessageClass() {
		return ChangeSpectatedPlayerMessage.class;
	}

	@Override
	public void handleMessage(HostedConnection conn, ChangeSpectatedPlayerMessage m) {
		Player player = playerManager.getPlayer(conn.getId());

		if (player == null) {
			noPlayerForConnection(conn.getId());
		} else {
			Optional<Player> result = player.getTeam().getPlayers().stream().filter(p -> p.getPlayerId() == m.getPlayerId()).findFirst();

			if (result.isPresent()) {
				serverState.changeSpectatedPlayer(player.getPlayerId(), m.getPlayerId());
			} else {
				log.warn("Team " + player.getTeam().getFaction() + " has no player with id " + m.getPlayerId() + "!");
			}
		}
	}
}