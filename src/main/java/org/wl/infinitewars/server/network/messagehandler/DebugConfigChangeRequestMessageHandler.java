package org.wl.infinitewars.server.network.messagehandler;

import org.wl.infinitewars.shared.event.config.DebugConfigChangedEvent;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.network.messages.toserver.config.DebugConfigChangeRequestMessage;

import com.jme3.network.HostedConnection;

public class DebugConfigChangeRequestMessageHandler extends ServerMessageHandler<DebugConfigChangeRequestMessage> {

	@Override
	public Class<DebugConfigChangeRequestMessage> getMessageClass() {
		return DebugConfigChangeRequestMessage.class;
	}

	@Override
	public void handleMessage(HostedConnection source, DebugConfigChangeRequestMessage m) {
		if (gameInfoProvider.getGameInfo().isEnableDebug()) {
			Player player = playerManager.getPlayer(source.getId());
			eventBus.fireEvent(new DebugConfigChangedEvent(player, m.getDebugCommandKey(), m.getArguments()));
		}
	}
}