package org.wl.infinitewars.server.ai;

import java.util.List;

import org.wl.infinitewars.shared.gameplay.GameInfo;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

public class AIHardThread extends AIMediumThread {

	public AIHardThread(List<Player> players, Team enemyTeam, GameInfo gameInfo, MapData mapData, AbstractSyncState syncState) {
		super(players, enemyTeam, gameInfo, mapData, syncState);
	}
}