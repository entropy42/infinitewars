package org.wl.infinitewars.server.ai;

import java.util.List;
import java.util.stream.Collectors;

import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.gameplay.GameInfo;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class AIMediumThread extends AIEasyThread {

	public AIMediumThread(List<Player> players, Team enemyTeam, GameInfo gameInfo, MapData mapData, AbstractSyncState syncState) {
		super(players, enemyTeam, gameInfo, mapData, syncState);
	}

	@Override
	protected Spatial getNewTarget(Player player) {
		Vector3f localTranslation = player.getShip().getLocalTranslation();

		List<Player> enemyPlayers = enemyTeam.getPlayers();

		Spatial nearestShip = null;
		float minDist = Float.MAX_VALUE;

		String specialWeapon = player.getShip().getUserData(EntityConstants.special_weaponFireType);
		String targetWeaponFireType = getTargetWeaponFireType(specialWeapon);

		enemyPlayers = enemyPlayers.stream()
				.filter(p -> {
					Node ship = p.getShip();

					if (ship != null) {
						return ship.getUserData(EntityConstants.special_weaponFireType).equals(targetWeaponFireType);
					}

					return false;
				}).collect(Collectors.toList());

		for (Player enemyPlayer : enemyPlayers) {
			Node ship = enemyPlayer.getShip();

			if (ship != null) {
				float dist = ship.getLocalTranslation().distance(localTranslation);

				if (dist < minDist) {
					minDist = dist;
					nearestShip = ship;
				}
			}
		}

		return nearestShip;
	}

	protected String getTargetWeaponFireType(String weaponFireType) {
		switch (weaponFireType) {
			case EntityConstants.special_weaponFireType_gatling:
				return EntityConstants.special_weaponFireType_rocketLauncher;
			case EntityConstants.special_weaponFireType_railGun:
				return EntityConstants.special_weaponFireType_lightning;
			case EntityConstants.special_weaponFireType_lightning:
				return EntityConstants.special_weaponFireType_gatling;
			case EntityConstants.special_weaponFireType_rocketLauncher:
				return EntityConstants.special_weaponFireType_railGun;
		}

		return null;
	}

	@Override
	protected long getCheckTime() {
		return 10000;
	}

	@Override
	protected long getSleepTime() {
		return 5000;
	}
}