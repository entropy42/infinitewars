package org.wl.infinitewars.server.ai;

import org.wl.infinitewars.shared.entity.WeaponSlotContainer;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.util.TargetLeadCalculator;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class AIHardFlightControl extends AIMediumFlightControl {

	public AIHardFlightControl(Player botPlayer) {
		super(botPlayer);
	}

	@Override
	protected Vector3f getShootDirection(Spatial target, String weaponFireSpeedPropertyKey, String containerKey) {
		float weaponFireSpeed = spatial.getUserData(weaponFireSpeedPropertyKey);
		WeaponSlotContainer weapons = spatial.getUserData(containerKey);

		Vector3f startLocation = weapons.size() == 2 ? weapons.calculateInBetweenPoint() : weapons.get(0).getWorldTranslation();
		Vector3f targetLeadPosition = TargetLeadCalculator.getTargetLeadPosition(startLocation, target, weaponFireSpeed);

		return targetLeadPosition.subtract(bodyControl.getPhysicsLocation()).normalizeLocal();
	}
}