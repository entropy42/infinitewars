package org.wl.infinitewars.server.ai;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.server.event.gameplay.ShootEvent;
import org.wl.infinitewars.shared.config.DebugConfigConstants;
import org.wl.infinitewars.shared.control.DelayedControl;
import org.wl.infinitewars.shared.control.MoveControl;
import org.wl.infinitewars.shared.control.MovementInfo;
import org.wl.infinitewars.shared.control.RotationConstants;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedEvent;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedHandler;
import org.wl.infinitewars.shared.gameplay.player.Player;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.FastMath;
import com.jme3.math.Plane;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;

public class AIEasyFlightControl extends DelayedControl {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	protected final EventBus eventBus = EventBus.get();

	protected final float MAX_VIEW_ANGLE = FastMath.PI / 8;
	protected final float MAX_VIEW_ANGLE_TAN = FastMath.tan(MAX_VIEW_ANGLE);

	protected final Random random = new Random();

	protected final Player botPlayer;
	protected final Spatial ship;
	protected final RigidBodyControl bodyControl;

	protected Spatial targetShip;
	protected RigidBodyControl targetBodyControl;
	protected Spatial objective;

	protected boolean isReturningToObjective;
	protected boolean isAttackingTarget = true;

	protected boolean logData;

	public AIEasyFlightControl(Player botPlayer) {
		super(1 / 60f);
		this.botPlayer = botPlayer;

		ship = botPlayer.getShip();
		bodyControl = ship.getControl(RigidBodyControl.class);

		init();
	}

	protected void init() {
		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				switch (event.getDebugCommandKey()) {
					case DebugConfigConstants.dbg_toggle_bot_targeting:
						setEnabled(!isEnabled());
						break;
					case DebugConfigConstants.dbg_toggle_log_player_infos:
						String[] arguments = event.getArguments();

						if (arguments.length == 2) {
							String playerName = arguments[0];
							String logType = arguments[1];

							if (playerName.equals(botPlayer.getName()) && logType.equals(DebugConfigConstants.dbg_toggle_log_player_info_type_rotation)) {
								logData = !logData;
							}
						}
						break;
				}
			}
		});
	}

	@Override
	protected void onUpdate(float tpf) {
		if (objective == null || !focusesOnObjective(tpf)) {
			if (targetShip != null) {
				handleTargetShip(tpf);
			}
		}
	}

	protected boolean focusesOnObjective(float tpf) {
		float objectiveDist = objective.getLocalTranslation().distance(bodyControl.getPhysicsLocation());

		if (isReturningToObjective) {
			if (objectiveDist <= getMinObjectiveDist()) {
				isReturningToObjective = false;
				return false;
			}

			returnToObjective(tpf);
			return true;
		}

		if (objectiveDist > getMaxObjectiveDist()) {
			returnToObjective(tpf);
			return true;
		}

		return false;
	}

	protected void handleTargetShip(float tpf) {
		Vector3f targetLocation = targetBodyControl.getPhysicsLocation();

		Vector3f targetDirection = targetLocation.subtract(bodyControl.getPhysicsLocation());
		float targetDist = targetDirection.length();

		if (isAttackingTarget) {
			checkAttacking(tpf, targetLocation, targetDist);
		} else {
			checkEvading(tpf, targetLocation, targetDist);
		}
	}

	protected void checkEvading(float tpf, Vector3f targetLocation, float targetDist) {
		if (targetDist <= getMaxEvadeDist()) {
			evade(tpf, targetLocation);
		} else {
			attackTargetShip(tpf, targetLocation);
		}
	}

	protected void checkAttacking(float tpf, Vector3f targetLocation, float targetDist) {
		if (targetDist >= getMinChaseDist()) {
			attackTargetShip(tpf, targetLocation);
		} else {
			Vector3f evadeLocation = bodyControl.getPhysicsLocation().add(bodyControl.getPhysicsLocation().subtract(targetLocation));
			evade(tpf, evadeLocation);
		}
	}

	protected void attackTargetShip(float tpf, Vector3f targetLocation) {
		isAttackingTarget = true;
		steerToLocation(targetLocation, tpf);

		Vector3f physicsLocation = bodyControl.getPhysicsLocation();
		float distance = physicsLocation.distance(targetLocation);

		if (distance <= getMaxShootDist()) {
			Vector3f normalize = targetLocation.subtract(physicsLocation).normalize();
			Vector3f viewDir = bodyControl.getPhysicsRotation().getRotationColumn(RotationConstants.FORWARD).normalize();

			float angle = FastMath.abs(viewDir.angleBetween(normalize));

			if (angle <= MAX_VIEW_ANGLE) {
				shoot(targetShip);
			}
		}
	}

	protected void shoot(Spatial target) {
		eventBus.fireEvent(new ShootEvent(getShootDirection(target, EntityConstants.standard_weaponFireSpeed, EntityConstants.standardWeaponSlots), botPlayer, EntityConstants.standardWeaponSlots));
		eventBus.fireEvent(new ShootEvent(getShootDirection(target, EntityConstants.special_weaponFireSpeed, EntityConstants.specialWeaponSlots), botPlayer, EntityConstants.specialWeaponSlots));
	}

	protected Vector3f getShootDirection(Spatial target, String weaponFireSpeedPropertyKey, String containerKey) {
		return target.getControl(RigidBodyControl.class).getPhysicsLocation().subtract(bodyControl.getPhysicsLocation()).normalizeLocal();
	}

	protected void evade(float tpf, Vector3f targetLocation) {
		isAttackingTarget = false;
		steerToLocation(targetLocation, tpf);
	}

	protected void returnToObjective(float tpf) {
		isReturningToObjective = true;
		steerToLocation(objective.getLocalTranslation(), tpf);
	}

	protected void steerToLocation(Vector3f targetLocation, float tpf) {
		if (logData) {
			System.out.println("isAttackingTarget: " + isAttackingTarget);
			System.out.println("isReturningToObjective: " + isReturningToObjective);
		}

		MoveControl moveControl = spatial.getControl(MoveControl.class);

		Vector3f forward = bodyControl.getPhysicsRotation().getRotationColumn(RotationConstants.FORWARD).normalize();
		Vector3f left = bodyControl.getPhysicsRotation().getRotationColumn(RotationConstants.LEFT).normalize();
		Vector3f up = bodyControl.getPhysicsRotation().getRotationColumn(RotationConstants.UP).normalize();
		Vector3f location = bodyControl.getPhysicsLocation();

		Plane planeZY = new Plane();
		planeZY.setPlanePoints(location, location.add(forward), location.add(up));

		Plane planeZX = new Plane();
		planeZX.setPlanePoints(location, location.add(forward), location.add(left));

		MovementInfo moveInfo = moveControl.getMoveInfo();
		moveInfo.setRotationPercentageX(getRotationPercentage(planeZY, targetLocation, planeZX, forward, moveInfo.getRotationPercentageX()));
		moveInfo.setRotationPercentageY(getRotationPercentage(planeZX, targetLocation, planeZY, forward, moveInfo.getRotationPercentageY()));
	}

	protected float getRotationPercentage(Plane plane, Vector3f targetLocation, Plane plane2, Vector3f forward, float currentPercentage) {
		Vector3f closestPoint2 = plane2.getClosestPoint(targetLocation);
		float angle = forward.angleBetween(closestPoint2.subtract(bodyControl.getPhysicsLocation()).normalize());

		float dist = plane.pseudoDistance(targetLocation);
		float percentage = 0f;

		if (angle > MAX_VIEW_ANGLE) {
			percentage = dist < 0f ? 1f : -1f;
		} else {
			Vector3f closestPoint = plane.getClosestPoint(targetLocation);

			float maxDist = closestPoint.distance(bodyControl.getPhysicsLocation()) * MAX_VIEW_ANGLE_TAN;

			if (maxDist < FastMath.abs(dist)) {
				percentage = dist < 0f ? 1f : -1f;
			} else {
				percentage = -dist / maxDist;
			}
		}

		float diff = currentPercentage - percentage;

		float maxDiff = 0.1f;

		if (FastMath.abs(diff) > maxDiff) {
			percentage = currentPercentage + (diff < 0 ? maxDiff : -maxDiff);
		}

		if (percentage > 1f) {
			percentage = 1f;
		} else if (percentage < -1f) {
			percentage = -1f;
		}

		return percentage;
	}

	protected float getMinChaseDist() {
		return 30f;
	}

	protected float getMaxEvadeDist() {
		return 140f;
	}

	protected float getMaxObjectiveDist() {
		return 1400f;
	}

	protected float getMinObjectiveDist() {
		return 100f;
	}

	protected float getMaxShootDist() {
		return 80f;
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

	public void setTargetShip(Spatial targetShip) {
		this.targetShip = targetShip;
		targetBodyControl = targetShip.getControl(RigidBodyControl.class);
	}

	public void setObjective(Spatial objective) {
		this.objective = objective;
	}
}