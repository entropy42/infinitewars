package org.wl.infinitewars.server.ai;

import org.wl.infinitewars.shared.config.DebugConfigConstants;
import org.wl.infinitewars.shared.control.MoveControl;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedEvent;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedHandler;

import com.jme3.bullet.control.RigidBodyControl;

public class AIMoveControl extends MoveControl {

	public AIMoveControl(RigidBodyControl bodyControl) {
		super(bodyControl);

		moveInfo.setForwardSpeedPercentage(1f);

		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				if (event.getDebugCommandKey().equals(DebugConfigConstants.dbg_toggle_bot_movement)) {
					moveInfo.setForwardSpeedPercentage(moveInfo.getForwardSpeedPercentage() == 1f ? 0f : 1f);
				}
			}
		});
	}
}