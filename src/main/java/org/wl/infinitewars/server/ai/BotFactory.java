package org.wl.infinitewars.server.ai;

import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.Team;

public interface BotFactory {
	
	Player createBot(String name, Team team) throws Exception;
}