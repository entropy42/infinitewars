package org.wl.infinitewars.server.ai;

public enum AILevel {

	EASY,

	MEDIUM,

	HARD
}