package org.wl.infinitewars.server;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wl.infinitewars.client.config.GameConfig;
import org.wl.infinitewars.server.ai.AIEasyThread;
import org.wl.infinitewars.server.ai.AIHardThread;
import org.wl.infinitewars.server.ai.AILevel;
import org.wl.infinitewars.server.ai.AIMediumThread;
import org.wl.infinitewars.server.gameplay.control.GameTimeUpdateControl;
import org.wl.infinitewars.server.loading.ServerLoadingStepsBuilder;
import org.wl.infinitewars.server.network.ServerMessageListener;
import org.wl.infinitewars.server.network.ServerState;
import org.wl.infinitewars.server.network.ServerSyncState;
import org.wl.infinitewars.shared.FactionConfigProvider;
import org.wl.infinitewars.shared.config.DebugConfigConstants;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityHelper;
import org.wl.infinitewars.shared.event.EventBus;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedEvent;
import org.wl.infinitewars.shared.event.config.DebugConfigChangedHandler;
import org.wl.infinitewars.shared.event.gameplay.WinConditionReachedEvent;
import org.wl.infinitewars.shared.event.gameplay.WinConditionReachedHandler;
import org.wl.infinitewars.shared.event.main.RestartGameEvent;
import org.wl.infinitewars.shared.event.main.RestartGameHandler;
import org.wl.infinitewars.shared.gameplay.GameInfo;
import org.wl.infinitewars.shared.gameplay.GameInfoProvider;
import org.wl.infinitewars.shared.gameplay.IngameState;
import org.wl.infinitewars.shared.gameplay.PhysicsWrapper;
import org.wl.infinitewars.shared.gameplay.TeamsContainer;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.PlayerConstants;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.json.EntityProperties;
import org.wl.infinitewars.shared.json.Faction;
import org.wl.infinitewars.shared.json.FactionConfig;
import org.wl.infinitewars.shared.json.GameMode;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.json.MapData;
import org.wl.infinitewars.shared.json.MapRotationEntry;
import org.wl.infinitewars.shared.json.ServerConfig;
import org.wl.infinitewars.shared.json.ServerDebugSetting;
import org.wl.infinitewars.shared.loading.EnqueueHelper;
import org.wl.infinitewars.shared.loading.LoadingStep;
import org.wl.infinitewars.shared.loading.LoadingStepBuildRequest;
import org.wl.infinitewars.shared.util.FileConstants;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.BulletAppState.ThreadingType;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

@Singleton
public class Server {

	private static final Logger log = LoggerFactory.getLogger(Server.class);

	private final EventBus eventBus = EventBus.get();

	private final List<AIEasyThread> aiThreads = new ArrayList<>(2);

	private ServerConfig serverConfig;
	private FactionConfigProvider factionConfigProvider;
	private EnqueueHelper enqueueHelper;
	private AppStateManager stateManager;
	private ServerState serverState;
	private GameInfoProvider gameInfoProvider;
	private IngameState ingameState;
	private ServerSyncState syncState;
	private TeamsContainer teamsContainer;
	private PhysicsCollisionListener collisionHandler;
	private ServerLoadingStepsBuilder loadingStepsBuilder;
	private PhysicsWrapper physicsWrapper;
	private ServerMessageListener serverMessageListener;

	private MapData mapData;
	private BulletAppState bulletAppState;
	private GameTimeUpdateControl gameTimeUpdateControl;

	public void init() {
		try {
			eventBus.addHandler(RestartGameEvent.TYPE, new RestartGameHandler() {
				@Override
				public void onRestartGame(RestartGameEvent event) {
					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							cleanupActiveGame();
							initNewGame(true);
							return null;
						}
					});
				}
			});

			eventBus.addHandler(WinConditionReachedEvent.TYPE, new WinConditionReachedHandler() {
				@Override
				public void onWinConditionReached(WinConditionReachedEvent event) {
					gameTimeUpdateControl.setEnabled(false);
				}
			});

			eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
				@Override
				public void onChanged(DebugConfigChangedEvent event) {
					if (event.getDebugCommandKey().equals(DebugConfigConstants.dbg_change_entity_property)) {
						String[] arguments = event.getArguments();

						try {
							Long entityId = Long.valueOf(arguments[0]);
							Spatial entity = syncState.getEntity(entityId);

							if (entity != null) {
								String fileName = entity.getUserData(EntityConstants.fileName);
								EntityProperties properties = JsonHelper.get().toPOJO(FileConstants.MODELS_PATH_COMPLETE + fileName + FileConstants.MODEL_PROPERTY_FILE, EntityProperties.class);

								String key = arguments[1];
								String value = arguments[2];

								properties.getProperties().stream()
										.filter(prop -> prop.getKey().equals(key))
										.forEach(prop -> EntityHelper.changeProperty(entity, key, prop.getTypedValue(value), null));
							} else {
								log.error("Entity with id " + entityId + " not found!");
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else if (event.getDebugCommandKey().equals(DebugConfigConstants.dbg_switch_team)) {
						Player player = event.getPlayer();
						player.setProperty(PlayerConstants.TEAM, player.getEnemyTeam().getTeamId());

						Node ship = player.getShip();

						if (ship != null) {
							EntityHelper.changeProperty(ship, EntityConstants.hitPoints, 0, null);
						}
					}
				}
			});

			int mapRotationIndex = serverConfig.getMapRotationIndex();
			MapRotationEntry mapRotationEntry = serverConfig.getMapRotationEntries().get(mapRotationIndex);

			int maxPlayers = serverConfig.getMaxPlayers();

			GameInfo gameInfo = new GameInfo();
			gameInfoProvider.setGameInfo(gameInfo);
			updateGameInfo(mapRotationEntry);
			gameInfo.setMaxPlayers(maxPlayers);
			gameInfo.setEnableDebug(serverConfig.isEnableDebug());
			gameInfo.setEnableBots(serverConfig.isEnableBots());
			gameInfo.setAiLevel(AILevel.valueOf(serverConfig.getAiLevel()));

			ingameState.setEnabled(false);
			stateManager.attach(ingameState);

			serverState.setEnabled(true);
			stateManager.attach(serverState);

			stateManager.attach(syncState);
			syncState.setEnabled(true);

			gameTimeUpdateControl = new GameTimeUpdateControl();
			Node ingameRootNode = ingameState.getRootNode();
			ingameRootNode.addControl(gameTimeUpdateControl);

			bulletAppState = new BulletAppState();
			bulletAppState.setEnabled(false);
			bulletAppState.setThreadingType(ThreadingType.PARALLEL);
			stateManager.attach(bulletAppState);

			PhysicsSpace physicsSpace = bulletAppState.getPhysicsSpace();
			physicsWrapper.setPhysicsSpace(physicsSpace);

			physicsSpace.setGravity(new Vector3f());
			physicsSpace.setAccuracy(1f / serverConfig.getPhysicsTickRate());
			physicsSpace.addCollisionListener(collisionHandler);

			serverState.setServerMessageListener(serverMessageListener);

			serverState.startServer();

			initNewGame(false);
		} catch (Throwable e) {
			onServerStartFailed(e);
		}
	}

	protected void updateGameInfo(MapRotationEntry mapRotationEntry) {
		GameInfo gameInfo = gameInfoProvider.getGameInfo();
		gameInfo.setInitMoney(mapRotationEntry.getInitMoney());
		gameInfo.setGameTime(mapRotationEntry.getGameTime());
		gameInfo.setGameMode(GameMode.valueOf(mapRotationEntry.getGameMode()));
		gameInfo.setMapName(mapRotationEntry.getMapName());
	}

	protected void onServerStartFailed(Throwable e) {
		log.error("Unable to start the server, shutting server down now...", e);

		if (serverState != null) {
			serverState.shutDown();
			stopServer();
			// stopApp(); // TODO
		}
	}

	protected void initNewGame(boolean isRestart) {
		try {
			loadMap();

			ingameState.setEnabled(true);
			bulletAppState.setEnabled(true);

			for (AIEasyThread thread : aiThreads) {
				thread.start();
			}

			log.info("Server startup done.");

			if (isRestart) {
				serverState.sendPlayerNameRequestMessages();
			}

			gameTimeUpdateControl.setEnabled(true);

			if (serverConfig.isEnableDebug()) {
				List<ServerDebugSetting> debugSettings = serverConfig.getDebugSettings();

				if (debugSettings != null) {
					for (ServerDebugSetting debugSetting : debugSettings) {
						String debugCommandKey = debugSetting.getCommand();
						String[] arguments = debugSetting.getArguments();

						eventBus.fireEvent(new DebugConfigChangedEvent(debugCommandKey, arguments));
					}
				}
			}
		} catch (Throwable e) {
			onServerStartFailed(e);
		}
	}

	private void loadMap() throws Exception {
		String mapName = gameInfoProvider.getGameInfo().getMapName();
		mapData = JsonHelper.get().toPOJO(FileConstants.MAPS_PATH_COMPLETE + mapName + FileConstants.MAP_DATA_FILE, MapData.class);

		int maxPlayers = serverConfig.getMaxPlayers();

		short teamId = 1;
		Team team1 = new Team(teamId, mapData.getFaction1(), maxPlayers);
		teamId++;
		Team team2 = new Team(teamId, mapData.getFaction2(), maxPlayers);

		teamsContainer.setTeam1(team1);
		teamsContainer.setTeam2(team2);

		LoadingStepBuildRequest request = new LoadingStepBuildRequest();
		request.setMapData(mapData);

		List<LoadingStep> loadingSteps = loadingStepsBuilder.createLoadingSteps(request);

		float totalTime = 0f;

		for (LoadingStep step : loadingSteps) {
			totalTime += step.getTime();
		}

		float elapsedTime = 0f;

		for (LoadingStep step : loadingSteps) {
			step.load();

			elapsedTime += step.getTime();

			float percentage = Precision.round((elapsedTime / totalTime) * 100f, 2);

			log.info(percentage + " % done...");
		}

		aiThreads.add(createAIThread(team1, team2));
		aiThreads.add(createAIThread(team2, team1));

		mapData = JsonHelper.get().toPOJO(FileConstants.MAPS_PATH_COMPLETE + mapName + FileConstants.MAP_DATA_FILE, MapData.class);
		Faction faction1 = mapData.getFaction1();
		Faction faction2 = mapData.getFaction2();

		FactionConfig factionConfig1 = JsonHelper.get().toPOJO(FileConstants.FACTIONS_PATH_COMPLETE + faction1.name().toLowerCase() + FileConstants.FACTION_CONFIG_FILE, FactionConfig.class);
		factionConfigProvider.put(faction1, factionConfig1);

		FactionConfig factionConfig2 = JsonHelper.get().toPOJO(FileConstants.FACTIONS_PATH_COMPLETE + faction2.name().toLowerCase() + FileConstants.FACTION_CONFIG_FILE, FactionConfig.class);
		factionConfigProvider.put(faction2, factionConfig2);
	}

	private AIEasyThread createAIThread(Team team, Team enemyTeam) {
		GameInfo gameInfo = gameInfoProvider.getGameInfo();

		switch (gameInfoProvider.getGameInfo().getAiLevel()) {
			case EASY:
				return new AIEasyThread(team.getPlayers(), enemyTeam, gameInfo, mapData, syncState);
			case MEDIUM:
				return new AIMediumThread(team.getPlayers(), enemyTeam, gameInfo, mapData, syncState);
			case HARD:
				return new AIHardThread(team.getPlayers(), enemyTeam, gameInfo, mapData, syncState);
		}

		return null;
	}

	public void cleanupActiveGame() throws Exception {
		int index = serverConfig.getMapRotationIndex();
		List<MapRotationEntry> entries = serverConfig.getMapRotationEntries();
		index++;

		if (index >= entries.size()) {
			index = 0;
		}

		serverConfig.setMapRotationIndex(index);
		updateGameInfo(entries.get(index));

		JsonHelper.get().writeJsonFile(serverConfig, GameConfig.getGameConfigFileDirectoryLocation() + FileConstants.SERVER_CONFIG);

		stopAIThreads();
		aiThreads.clear();

		physicsWrapper.cleanup(ingameState.getRootNode());
		ingameState.cleanup();
		syncState.cleanup();
		bulletAppState.setEnabled(false);

		cleanupTeam(teamsContainer.getTeam1());
		cleanupTeam(teamsContainer.getTeam2());
	}

	private void cleanupTeam(Team team) {
		team.cleanup();
	}

	public void stopServer() {
		stopAIThreads();
	}

	protected void stopAIThreads() {
		for (AIEasyThread thread : aiThreads) {
			thread.stopThread();
		}
	}

	@Inject
	public void setEnqueueHelper(EnqueueHelper enqueueHelper) {
		this.enqueueHelper = enqueueHelper;
	}

	@Inject
	public void setStateManager(AppStateManager stateManager) {
		this.stateManager = stateManager;
	}

	@Inject
	public void setServerState(ServerState serverState) {
		this.serverState = serverState;
	}

	@Inject
	public void setServerConfig(ServerConfig serverConfig) {
		this.serverConfig = serverConfig;
	}

	@Inject
	public void setFactionConfigProvider(FactionConfigProvider factionConfigProvider) {
		this.factionConfigProvider = factionConfigProvider;
	}

	@Inject
	public void setGameInfoProvider(GameInfoProvider gameInfoProvider) {
		this.gameInfoProvider = gameInfoProvider;
	}

	@Inject
	public void setIngameState(IngameState ingameState) {
		this.ingameState = ingameState;
	}

	@Inject
	public void setSyncState(ServerSyncState syncState) {
		this.syncState = syncState;
	}

	@Inject
	public void setServerMessageListener(ServerMessageListener serverMessageListener) {
		this.serverMessageListener = serverMessageListener;
	}

	@Inject
	public void setTeamsContainer(TeamsContainer teamsContainer) {
		this.teamsContainer = teamsContainer;
	}

	@Inject
	public void setBulletAppState(BulletAppState bulletAppState) {
		this.bulletAppState = bulletAppState;
	}

	@Inject
	public void setPhysicsWrapper(PhysicsWrapper physicsWrapper) {
		this.physicsWrapper = physicsWrapper;
	}

	@Inject
	public void setGameTimeUpdateControl(GameTimeUpdateControl gameTimeUpdateControl) {
		this.gameTimeUpdateControl = gameTimeUpdateControl;
	}

	@Inject
	public void setCollisionHandler(PhysicsCollisionListener collisionHandler) {
		this.collisionHandler = collisionHandler;
	}

	@Inject
	public void setLoadingStepsBuilder(ServerLoadingStepsBuilder loadingStepsBuilder) {
		this.loadingStepsBuilder = loadingStepsBuilder;
	}
}