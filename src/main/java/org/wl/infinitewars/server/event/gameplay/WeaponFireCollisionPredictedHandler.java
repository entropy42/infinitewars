package org.wl.infinitewars.server.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface WeaponFireCollisionPredictedHandler extends EventHandler {

	void onCollisionPredicted(WeaponFireCollisionPredictedEvent event);
}