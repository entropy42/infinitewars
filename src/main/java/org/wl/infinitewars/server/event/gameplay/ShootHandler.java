package org.wl.infinitewars.server.event.gameplay;

import org.wl.infinitewars.shared.event.EventHandler;

public interface ShootHandler extends EventHandler {
	
	void onShoot(ShootEvent event);
}