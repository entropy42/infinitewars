package org.wl.infinitewars.server;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.wl.infinitewars.client.config.GameConfig;
import org.wl.infinitewars.server.ai.AILevel;
import org.wl.infinitewars.server.ai.BotFactory;
import org.wl.infinitewars.server.ai.BotFactoryImpl;
import org.wl.infinitewars.server.entity.ServerEntityFactory;
import org.wl.infinitewars.server.entity.ServerEntityIdProvider;
import org.wl.infinitewars.server.gameplay.CollisionHandler;
import org.wl.infinitewars.server.gameplay.ServerPlayerIdProvider;
import org.wl.infinitewars.server.gameplay.weapon.ServerGatlingWeaponFireFactory;
import org.wl.infinitewars.server.gameplay.weapon.ServerLightningWeaponFireFactory;
import org.wl.infinitewars.server.gameplay.weapon.ServerRailGunWeaponFireFactory;
import org.wl.infinitewars.server.gameplay.weapon.ServerRocketLauncherWeaponFireFactory;
import org.wl.infinitewars.server.gameplay.weapon.ServerStandardWeaponFireFactory;
import org.wl.infinitewars.server.loading.ServerLoadingStepsBuilder;
import org.wl.infinitewars.server.network.ServerFactory;
import org.wl.infinitewars.server.network.ServerFactoryImpl;
import org.wl.infinitewars.server.network.ServerState;
import org.wl.infinitewars.server.network.ServerStateImpl;
import org.wl.infinitewars.server.network.ServerSyncState;
import org.wl.infinitewars.server.network.messagehandler.BuyShipRequestMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.ChangePlayerSpawnLocationRequestMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.ChangeSpectatedPlayerMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.DebugConfigChangeRequestMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.FireWeaponsMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.GameStateRequestMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.MoveShipMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.PlayerNameResponseMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.ServerMessageHandler;
import org.wl.infinitewars.server.network.messagehandler.WantsRespawnMessageHandler;
import org.wl.infinitewars.server.network.restapi.GameInfoMapper;
import org.wl.infinitewars.server.network.restapi.RestApiService;
import org.wl.infinitewars.server.network.restapi.RestApiServiceImpl;
import org.wl.infinitewars.shared.AbstractGameApplication;
import org.wl.infinitewars.shared.SharedModule;
import org.wl.infinitewars.shared.entity.EntityFactory;
import org.wl.infinitewars.shared.entity.EntityIdProvider;
import org.wl.infinitewars.shared.gameplay.IngameState;
import org.wl.infinitewars.shared.gameplay.IngameStateImpl;
import org.wl.infinitewars.shared.gameplay.PhysicsWrapper;
import org.wl.infinitewars.shared.gameplay.PhysicsWrapperImpl;
import org.wl.infinitewars.shared.gameplay.player.PlayerIdProvider;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireFactory;
import org.wl.infinitewars.shared.json.GameMode;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.json.MapRotationEntry;
import org.wl.infinitewars.shared.json.ServerConfig;
import org.wl.infinitewars.shared.loading.LoadingStepsBuilder;
import org.wl.infinitewars.shared.network.sync.AbstractSyncState;
import org.wl.infinitewars.shared.util.FileConstants;

import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import com.jme3.bullet.collision.PhysicsCollisionListener;

import io.javalin.Javalin;

public class ServerModule extends SharedModule {

	private final ServerConfig serverConfig;

	public ServerModule(AbstractGameApplication app) throws Exception {
		super(app);

		serverConfig = createServerConfig();
	}

	@Override
	protected void configure() {
		super.configure();
		bind(ServerConfig.class).toInstance(serverConfig);

		bind(EntityFactory.class).to(ServerEntityFactory.class);
		bind(EntityIdProvider.class).to(ServerEntityIdProvider.class);
		bind(ServerState.class).to(ServerStateImpl.class);
		bind(IngameState.class).to(IngameStateImpl.class);
		bind(PhysicsWrapper.class).to(PhysicsWrapperImpl.class);
		bind(BotFactory.class).to(BotFactoryImpl.class);
		bind(ServerFactory.class).to(ServerFactoryImpl.class);
		bind(PlayerIdProvider.class).to(ServerPlayerIdProvider.class);
		bind(PhysicsCollisionListener.class).to(CollisionHandler.class);
		bind(AbstractSyncState.class).to(ServerSyncState.class);
		bind(LoadingStepsBuilder.class).to(ServerLoadingStepsBuilder.class);
		bind(PhysicsCollisionListener.class).to(CollisionHandler.class);
		bind(RestApiService.class).to(RestApiServiceImpl.class);

		GameInfoMapper gameInfoMapper = GameInfoMapper.INSTANCE;
		bind(GameInfoMapper.class).toInstance(gameInfoMapper);

		Javalin javalin = Javalin.create();
		javalin.start(7000);
		bind(Javalin.class).toInstance(javalin);

		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(BuyShipRequestMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(ChangePlayerSpawnLocationRequestMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(ChangeSpectatedPlayerMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(DebugConfigChangeRequestMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(FireWeaponsMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(GameStateRequestMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(MoveShipMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(PlayerNameResponseMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ServerMessageHandler.class).addBinding().to(WantsRespawnMessageHandler.class).in(Singleton.class);

		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ServerStandardWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ServerLightningWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ServerGatlingWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ServerRailGunWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ServerRocketLauncherWeaponFireFactory.class).in(Singleton.class);
	}

	private ServerConfig createServerConfig() throws Exception {
		File dir = new File(GameConfig.getGameConfigFileDirectoryLocation());

		if (!dir.exists()) {
			dir.mkdir();
		}

		ServerConfig config = JsonHelper.get().toPOJO(GameConfig.getGameConfigFileDirectoryLocation() + FileConstants.SERVER_CONFIG, ServerConfig.class, true);

		if (config == null) {
			config = createNewDefaultConfig();
		}

		return config;
	}

	private ServerConfig createNewDefaultConfig() throws Exception {
		ServerConfig c = new ServerConfig();

		List<MapRotationEntry> mapRotationEntries = new ArrayList<>(1);
		c.setMapRotationEntries(mapRotationEntries);

		MapRotationEntry rotationEntry = new MapRotationEntry();
		mapRotationEntries.add(rotationEntry);

		rotationEntry.setEndTimer(20);
		rotationEntry.setGameMode(GameMode.MOTHER_SHIP.name());
		rotationEntry.setGameTime(1200);
		rotationEntry.setInitMoney(1000);
		rotationEntry.setMapName("map1");
		rotationEntry.setSpawnDelay(5.0f);

		c.setEnableBots(true);
		c.setMaxPlayers(16);
		c.setNetworkTickRate(128);
		c.setPhysicsTickRate(128);
		c.setAiLevel(AILevel.EASY.name());

		JsonHelper.get().writeJsonFile(c, GameConfig.getGameConfigFileDirectoryLocation() + FileConstants.SERVER_CONFIG);

		return c;
	}
}