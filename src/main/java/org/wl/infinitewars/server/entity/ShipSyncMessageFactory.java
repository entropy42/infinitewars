package org.wl.infinitewars.server.entity;

import java.io.IOException;

import org.wl.infinitewars.shared.network.messages.toclient.sync.AbstractSyncMessage;
import org.wl.infinitewars.shared.network.messages.toclient.sync.ShipSyncMessage;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.scene.Spatial;

public class ShipSyncMessageFactory implements SyncMessageFactory {

	@Override
	public AbstractSyncMessage create(Spatial entity) {
		return new ShipSyncMessage(entity);
	}

	@Override
	public void write(JmeExporter ex) throws IOException {

	}

	@Override
	public void read(JmeImporter im) throws IOException {

	}
}