package org.wl.infinitewars.server.entity;

import org.wl.infinitewars.shared.entity.EntityIdProvider;

import com.google.inject.Singleton;

@Singleton
public class ServerEntityIdProvider implements EntityIdProvider {

	private long lastId;

	@Override
	public long getNextId() {
		lastId++;
		return lastId;
	}
}