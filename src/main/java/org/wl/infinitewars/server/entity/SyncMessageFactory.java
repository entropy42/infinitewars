package org.wl.infinitewars.server.entity;

import org.wl.infinitewars.shared.network.messages.toclient.sync.AbstractSyncMessage;

import com.jme3.export.Savable;
import com.jme3.scene.Spatial;

public interface SyncMessageFactory extends Savable {
	
	AbstractSyncMessage create(Spatial entity);
}