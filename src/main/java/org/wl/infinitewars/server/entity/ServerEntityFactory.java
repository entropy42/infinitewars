package org.wl.infinitewars.server.entity;

import org.wl.infinitewars.server.gameplay.DamageHistory;
import org.wl.infinitewars.server.gameplay.control.ControlPointControl;
import org.wl.infinitewars.server.gameplay.control.ShipControl;
import org.wl.infinitewars.server.gameplay.control.WeaponFireCCDControl;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityFactoryImpl;
import org.wl.infinitewars.shared.entity.EntityHelper;
import org.wl.infinitewars.shared.gameplay.GameInfoProvider;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireCreationData;
import org.wl.infinitewars.shared.json.EntityProperties;
import org.wl.infinitewars.shared.json.JsonHelper;
import org.wl.infinitewars.shared.json.ServerConfig;
import org.wl.infinitewars.shared.json.WorldObjectData;
import org.wl.infinitewars.shared.util.FileConstants;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.scene.Node;

@Singleton
public class ServerEntityFactory extends EntityFactoryImpl {

	@Inject
	private GameInfoProvider gameInfoProvider;

	@Inject
	private ServerConfig serverConfig;

	@Override
	public Node createObject(WorldObjectData object) throws Exception {
		Node node = super.createObject(object);

		String controlPointType = node.getUserData(EntityConstants.controlPointType);

		if (controlPointType != null) {
			node.addControl(new ControlPointControl(teamsContainer.getTeam1(), teamsContainer.getTeam2()));
		}

		String name = object.getName();
		String propFilePath = FileConstants.MODELS_PATH_COMPLETE + name + FileConstants.MODEL_PROPERTY_FILE;
		EntityProperties properties = JsonHelper.get().toPOJO(propFilePath, EntityProperties.class);

		if (properties != null) {
			long entityId = EntityHelper.getEntityId(node);
			gameInfoProvider.getGameInfo().getObjectIds().put(node.getLocalTranslation(), entityId);
		}

		return node;
	}

	@Override
	protected void prepareMotherShip(EntityProperties properties, Node motherShip) {
		super.prepareMotherShip(properties, motherShip);
		motherShip.addControl(new ShipControl());
	}

	@Override
	public Node createShip(String fileName) throws Exception {
		Node ship = super.createShip(fileName);

		ship.setUserData(EntityConstants.damageHistory, new DamageHistory());
		ship.setUserData(EntityConstants.syncMessageFactory, new ShipSyncMessageFactory());
		ship.addControl(new ShipControl());

		return ship;
	}

	@Override
	public Node createWeaponFire(WeaponFireCreationData data) throws Exception {
		Node weaponFire = super.createWeaponFire(data);

		weaponFire.addControl(new WeaponFireCCDControl(serverConfig.getPhysicsTickRate()));

		return weaponFire;
	}
}