package org.wl.infinitewars.server.gameplay.control;

import org.wl.infinitewars.shared.control.DelayedControl;
import org.wl.infinitewars.shared.control.MoveControl;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityHelper;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

public class BoostControl extends DelayedControl {

	public BoostControl() {
		super(0.5f);
	}

	@Override
	protected void onUpdate(float elapsedTime) {
		final MoveControl moveControl = spatial.getControl(MoveControl.class);
		final float boostEnergy = spatial.getUserData(EntityConstants.boostEnergy);
		final float maxBoostEnergy = spatial.getUserData(EntityConstants.maxBoostEnergy);

		float newBoostEnergy = boostEnergy;

		if (moveControl.getMoveInfo().isBoosting()) {
			if (boostEnergy > 0f) {
				newBoostEnergy -= delayTime * 8f;
			}

			if (newBoostEnergy < 0f) {
				newBoostEnergy = 0f;
			}
		} else if (boostEnergy < maxBoostEnergy) {
			newBoostEnergy += delayTime * 3f;

			if (newBoostEnergy > maxBoostEnergy) {
				newBoostEnergy = maxBoostEnergy;
			}
		}

		if (newBoostEnergy != boostEnergy) {
			EntityHelper.changeProperty(spatial, EntityConstants.boostEnergy, newBoostEnergy, null);
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}