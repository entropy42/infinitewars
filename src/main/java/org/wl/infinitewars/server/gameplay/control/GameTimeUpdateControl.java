package org.wl.infinitewars.server.gameplay.control;

import org.wl.infinitewars.shared.control.DelayedControl;
import org.wl.infinitewars.shared.event.main.GameTimeUpdateEvent;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

public class GameTimeUpdateControl extends DelayedControl {
	
	private float currentTime = 0f;
	
	public GameTimeUpdateControl() {
		super(1f);
	}
	
	@Override
	protected void onUpdate(float elapsedTime) {
		currentTime += elapsedTime;
		eventBus.fireEvent(new GameTimeUpdateEvent(currentTime));
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		
		currentTime = 0f;
	}
	
	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {
	
	}
}