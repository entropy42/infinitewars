package org.wl.infinitewars.server.gameplay.control;

import java.util.Comparator;

import com.jme3.scene.Spatial;

public class WeaponFireCCDResultComparator implements Comparator<WeaponFireCCDCollisionResult> {

	private final Spatial weaponFire;

	public WeaponFireCCDResultComparator(Spatial weaponFire) {
		this.weaponFire = weaponFire;
	}

	@Override
	public int compare(WeaponFireCCDCollisionResult o1, WeaponFireCCDCollisionResult o2) {
		float d1 = o1.getContactPoint().distance(weaponFire.getLocalTranslation());
		float d2 = o2.getContactPoint().distance(weaponFire.getLocalTranslation());
		return Float.compare(d1, d2);
	}
}