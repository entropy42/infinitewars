package org.wl.infinitewars.server.gameplay.control;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class WeaponFireCCDCollisionResult {

	private final Spatial object;
	private final Vector3f contactPoint;

	public WeaponFireCCDCollisionResult(Spatial object, Vector3f contactPoint) {
		this.object = object;
		this.contactPoint = contactPoint;
	}

	public Spatial getObject() {
		return object;
	}

	public Vector3f getContactPoint() {
		return contactPoint;
	}
}