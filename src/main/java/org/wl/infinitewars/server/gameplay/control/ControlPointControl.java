package org.wl.infinitewars.server.gameplay.control;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.wl.infinitewars.shared.control.DelayedControl;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.entity.EntityHelper;
import org.wl.infinitewars.shared.gameplay.player.Player;
import org.wl.infinitewars.shared.gameplay.player.Team;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class ControlPointControl extends DelayedControl {

	private final Map<Team, Float> capturePoints = new HashMap<>(2);

	private int maxCapturePoints;
	private int captureRadius;

	private Team owner;

	private final Team team1;
	private final Team team2;

	public ControlPointControl(Team team1, Team team2) {
		super(1 / 30f);
		this.team1 = team1;
		this.team2 = team2;

		capturePoints.put(team1, 0.0f);
		capturePoints.put(team2, 0.0f);
	}

	@Override
	protected void onUpdate(float elapsedTime) {
		float points1 = getAddedCapturePoints(team1, elapsedTime);
		float points2 = getAddedCapturePoints(team2, elapsedTime);

		float diff = points1 - points2;

		float currentPoints1 = capturePoints.get(team1);
		float currentPoints2 = capturePoints.get(team2);

		float newPoints1 = checkNewPoints(currentPoints1 + diff, team1);
		float newPoints2 = checkNewPoints(currentPoints2 - diff, team2);

		capturePoints.put(team1, newPoints1);
		capturePoints.put(team2, newPoints2);

		EntityHelper.changeProperty(spatial, EntityConstants.controlPointCapturePointsTeam1, newPoints1, null);
		EntityHelper.changeProperty(spatial, EntityConstants.controlPointCapturePointsTeam2, newPoints2, null);
	}

	protected float checkNewPoints(float newPoints, Team team) {
		if (newPoints <= 0f) {
			newPoints = 0f;

			if (owner == team) {
				// owner.getActiveSpawnPoints().remove(spatial);
				owner = null;
				EntityHelper.changeProperty(spatial, EntityConstants.faction, null, null);
			}
		} else if (newPoints >= maxCapturePoints) {
			newPoints = maxCapturePoints;
			owner = team;
			// owner.getActiveSpawnPoints().add(spatial);
			EntityHelper.changeProperty(spatial, EntityConstants.faction, owner.getFaction().name(), null);
		}

		return newPoints;
	}

	protected float getAddedCapturePoints(Team team, float elapsedTime) {
		List<Player> players = team.getPlayers();

		float addedCapturePoints = 0.0f;

		for (Player player : players) {
			if (player.isAlive()) {
				Node ship = player.getShip();

				if (ship != null) {
					float dist = ship.getLocalTranslation().distance(spatial.getLocalTranslation());

					if (captureRadius >= dist) {
						addedCapturePoints += (elapsedTime * 5f);
					}
				}
			}
		}

		return addedCapturePoints;
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);

		maxCapturePoints = spatial.getUserData(EntityConstants.controlPointCapturePoints);
		captureRadius = spatial.getUserData(EntityConstants.controlPointCaptureRadius);
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}