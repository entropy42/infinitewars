package org.wl.infinitewars.server.gameplay;

import org.wl.infinitewars.shared.gameplay.player.PlayerIdProvider;

import com.google.inject.Singleton;

@Singleton
public class ServerPlayerIdProvider implements PlayerIdProvider {

	private int lastId = 0;

	@Override
	public int generatePlayerId() {
		lastId++;
		return lastId;
	}
}