package org.wl.infinitewars.server.gameplay.control;

import org.wl.infinitewars.shared.control.DelayedControl;
import org.wl.infinitewars.shared.event.gameplay.RespawnPlayerEvent;
import org.wl.infinitewars.shared.gameplay.player.Player;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

public class RespawnControl extends DelayedControl {

	private final Player player;

	public RespawnControl(Player player, float respawnDelay) {
		super(respawnDelay);
		this.player = player;
	}

	@Override
	protected void onUpdate(float elapsedTime) {
		if (player.getShip() != null && !player.isAlive() && player.isWantsRespawn()) {
			eventBus.fireEvent(new RespawnPlayerEvent(player));
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}