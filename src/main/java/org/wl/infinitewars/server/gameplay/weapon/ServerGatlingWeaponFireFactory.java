package org.wl.infinitewars.server.gameplay.weapon;

import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.gameplay.weapon.SimpleSpecialWeaponFirePhysics;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireFactory;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;

public class ServerGatlingWeaponFireFactory extends WeaponFireFactory {

	@Inject
	public ServerGatlingWeaponFireFactory(AssetManager assetManager) {
		super(EntityConstants.special_weaponFirePrefix, assetManager, new SimpleSpecialWeaponFirePhysics(EntityConstants.special_weaponFirePrefix), EntityConstants.special_weaponFireType_gatling);
	}
}