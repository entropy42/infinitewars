package org.wl.infinitewars.server.gameplay.weapon;

import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.gameplay.weapon.SimpleSpecialWeaponFirePhysics;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireFactory;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;

public class ServerStandardWeaponFireFactory extends WeaponFireFactory {

	@Inject
	public ServerStandardWeaponFireFactory(AssetManager assetManager) {
		super(EntityConstants.standard_weaponFirePrefix, assetManager, new SimpleSpecialWeaponFirePhysics(EntityConstants.standard_weaponFirePrefix), EntityConstants.standard_weaponFirePrefix);
	}
}