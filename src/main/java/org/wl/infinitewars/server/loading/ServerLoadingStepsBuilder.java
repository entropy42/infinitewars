package org.wl.infinitewars.server.loading;

import java.util.List;

import org.wl.infinitewars.server.ai.BotFactory;
import org.wl.infinitewars.server.gameplay.mode.ServerMotherShipLoadingStepFactory;
import org.wl.infinitewars.shared.gameplay.GameInfo;
import org.wl.infinitewars.shared.gameplay.mode.MotherShipWinCondition;
import org.wl.infinitewars.shared.gameplay.mode.TimeWinCondition;
import org.wl.infinitewars.shared.gameplay.player.Team;
import org.wl.infinitewars.shared.loading.GameModeLoadingStepFactory;
import org.wl.infinitewars.shared.loading.LoadingStep;
import org.wl.infinitewars.shared.loading.LoadingStepBuildRequest;
import org.wl.infinitewars.shared.loading.LoadingStepsBuilder;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ServerLoadingStepsBuilder extends LoadingStepsBuilder {

	private BotFactory botFactory;

	@Override
	protected void addSteps(List<LoadingStep> steps, LoadingStepBuildRequest request) {
		if (gameInfoProvider.getGameInfo().isEnableBots()) {
			addTeamBotLoadingSteps(true, steps);
			addTeamBotLoadingSteps(false, steps);
		}

		steps.add(new LoadingStep(5f, false) {
			@Override
			public void load() throws Exception {
				createWinCondition();
			}
		});
	}

	private void addTeamBotLoadingSteps(final boolean team1, List<LoadingStep> steps) {
		int maxPlayersForTeam = gameInfoProvider.getGameInfo().getMaxPlayers() / 2;

		for (int i = 0; i < maxPlayersForTeam; i++) {
			final int a = i;

			steps.add(new LoadingStep(5f, true) {
				@Override
				public void load() throws Exception {
					Team team = team1 ? teamsContainer.getTeam1() : teamsContainer.getTeam2();
					initBot(a, team);
				}
			});
		}
	}

	private void initBot(int i, Team team) throws Exception {
		botFactory.createBot("Bot" + i, team);
	}

	private void createWinCondition() {
		GameInfo gameInfo = gameInfoProvider.getGameInfo();

		switch (gameInfo.getGameMode()) {
			case CONQUEST:
				new TimeWinCondition(teamsContainer, gameInfo);
				break;
			case MOTHER_SHIP:
				new MotherShipWinCondition();
				new TimeWinCondition(teamsContainer, gameInfo);
				break;
			case TEAM_DEATH_MATCH:
				new TimeWinCondition(teamsContainer, gameInfo);
				break;
		}
	}

	@Override
	protected GameModeLoadingStepFactory createMotherShipLoadingStepFactory(LoadingStepBuildRequest request) {
		GameModeLoadingStepFactory factory = new ServerMotherShipLoadingStepFactory(request.getMapData(), entityFactory, teamsContainer, factionConfigs, entityAttacher, gameInfoProvider.getGameInfo());
		return factory;
	}

	@Inject
	public void setBotFactory(BotFactory botFactory) {
		this.botFactory = botFactory;
	}
}