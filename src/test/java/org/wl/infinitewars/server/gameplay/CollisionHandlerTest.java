package org.wl.infinitewars.server.gameplay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.wl.infinitewars.AbstractTest;
import org.wl.infinitewars.server.network.ServerState;
import org.wl.infinitewars.server.network.ServerSyncState;
import org.wl.infinitewars.shared.entity.EntityAttacher;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeEvent;
import org.wl.infinitewars.shared.gameplay.EntityPropertyChangeCause;
import org.wl.infinitewars.shared.gameplay.WeaponFireCause;
import org.wl.infinitewars.shared.gameplay.weapon.WeaponFireControl;
import org.wl.infinitewars.shared.loading.EnqueueHelper;
import org.wl.infinitewars.shared.network.messages.toclient.gameplay.RemoveWeaponFireMessage;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class CollisionHandlerTest extends AbstractTest {

	private CollisionHandler handler;
	private ServerState serverState;
	private EntityAttacher entityAttacher;
	private ServerSyncState syncState;
	private EnqueueHelper enqueueHelper;

	@Before
	public void setup() {
		serverState = Mockito.mock(ServerState.class);
		syncState = Mockito.mock(ServerSyncState.class);
		entityAttacher = Mockito.mock(EntityAttacher.class);
		enqueueHelper = Mockito.mock(EnqueueHelper.class);

		handler = Mockito.spy(new CollisionHandler(entityAttacher, enqueueHelper, serverState, syncState));
	}

	@Test
	public void shouldOnlyReduceShields() {
		Spatial entity = new Node("test entity");
		entity.setUserData(EntityConstants.id, 1l);

		DamageHistory damageHistory = new DamageHistory();
		entity.setUserData(EntityConstants.damageHistory, damageHistory);

		entity.setUserData(EntityConstants.shields, 100);
		entity.setUserData(EntityConstants.hitPoints, 100);

		int originalDamage = 10;
		Spatial weaponFire = new Node("test weaponfire");
		EntityPropertyChangeCause cause = new WeaponFireCause(weaponFire);

		handler.receiveDamage(entity, originalDamage, cause);

		int currentShields = (int)entity.getUserData(EntityConstants.shields);
		assertEquals(90, currentShields);
		int currentHitPoints = (int)entity.getUserData(EntityConstants.hitPoints);
		assertEquals(100, currentHitPoints);

		ArgumentCaptor<EntityPropertyChangeEvent> captor = ArgumentCaptor.forClass(EntityPropertyChangeEvent.class);
		Mockito.verify(eventBus).fireEvent(captor.capture());

		EntityPropertyChangeEvent event = captor.getValue();
		assertNotNull(event);

		assertEquals(cause, event.getCause());
		assertEquals(entity, event.getEntity());
		assertEquals(1l, event.getEntityId());
		assertEquals(100, event.getOldPropertyValue());
		assertEquals(EntityConstants.shields, event.getPropertyName());
		assertEquals(90, event.getPropertyValue());
	}

	@Test
	public void shouldOnlyReduceHitPoints() {
		Spatial entity = new Node("test entity");
		entity.setUserData(EntityConstants.id, 1l);

		DamageHistory damageHistory = new DamageHistory();
		entity.setUserData(EntityConstants.damageHistory, damageHistory);

		entity.setUserData(EntityConstants.shields, 0);
		entity.setUserData(EntityConstants.hitPoints, 100);

		int originalDamage = 10;
		Spatial weaponFire = new Node("test weaponfire");
		EntityPropertyChangeCause cause = new WeaponFireCause(weaponFire);

		handler.receiveDamage(entity, originalDamage, cause);

		int currentShields = (int)entity.getUserData(EntityConstants.shields);
		assertEquals(0, currentShields);
		int currentHitPoints = (int)entity.getUserData(EntityConstants.hitPoints);
		assertEquals(90, currentHitPoints);

		ArgumentCaptor<EntityPropertyChangeEvent> captor = ArgumentCaptor.forClass(EntityPropertyChangeEvent.class);
		Mockito.verify(eventBus).fireEvent(captor.capture());

		EntityPropertyChangeEvent event = captor.getValue();
		assertNotNull(event);

		assertEquals(cause, event.getCause());
		assertEquals(entity, event.getEntity());
		assertEquals(1l, event.getEntityId());
		assertEquals(100, event.getOldPropertyValue());
		assertEquals(EntityConstants.hitPoints, event.getPropertyName());
		assertEquals(90, event.getPropertyValue());
	}

	@Test
	public void shouldReduceShieldsAndHitPoints() {
		Spatial entity = new Node("test entity");
		entity.setUserData(EntityConstants.id, 1l);

		DamageHistory damageHistory = new DamageHistory();
		entity.setUserData(EntityConstants.damageHistory, damageHistory);

		entity.setUserData(EntityConstants.shields, 100);
		entity.setUserData(EntityConstants.hitPoints, 100);

		int originalDamage = 110;
		Spatial weaponFire = new Node("test weaponfire");
		EntityPropertyChangeCause cause = new WeaponFireCause(weaponFire);

		handler.receiveDamage(entity, originalDamage, cause);

		int currentShields = (int)entity.getUserData(EntityConstants.shields);
		assertEquals(0, currentShields);
		int currentHitPoints = (int)entity.getUserData(EntityConstants.hitPoints);
		assertEquals(90, currentHitPoints);

		ArgumentCaptor<EntityPropertyChangeEvent> captor = ArgumentCaptor.forClass(EntityPropertyChangeEvent.class);
		Mockito.verify(eventBus, Mockito.times(2)).fireEvent(captor.capture());

		List<EntityPropertyChangeEvent> events = captor.getAllValues();
		assertNotNull(events);
		assertEquals(2, events.size());

		EntityPropertyChangeEvent shieldEvent = events.get(0);
		assertEquals(cause, shieldEvent.getCause());
		assertEquals(entity, shieldEvent.getEntity());
		assertEquals(1l, shieldEvent.getEntityId());
		assertEquals(100, shieldEvent.getOldPropertyValue());
		assertEquals(EntityConstants.shields, shieldEvent.getPropertyName());
		assertEquals(0, shieldEvent.getPropertyValue());

		EntityPropertyChangeEvent hpEvent = events.get(1);
		assertEquals(cause, hpEvent.getCause());
		assertEquals(entity, hpEvent.getEntity());
		assertEquals(1l, hpEvent.getEntityId());
		assertEquals(100, hpEvent.getOldPropertyValue());
		assertEquals(EntityConstants.hitPoints, hpEvent.getPropertyName());
		assertEquals(90, hpEvent.getPropertyValue());
	}

	@Test
	public void shouldZeroHitPoints() {
		Spatial entity = new Node("test entity");
		entity.setUserData(EntityConstants.id, 1l);

		DamageHistory damageHistory = new DamageHistory();
		entity.setUserData(EntityConstants.damageHistory, damageHistory);

		entity.setUserData(EntityConstants.shields, 0);
		entity.setUserData(EntityConstants.hitPoints, 100);

		int originalDamage = 111;
		Spatial weaponFire = new Node("test weaponfire");
		EntityPropertyChangeCause cause = new WeaponFireCause(weaponFire);

		handler.receiveDamage(entity, originalDamage, cause);

		int currentShields = (int)entity.getUserData(EntityConstants.shields);
		assertEquals(0, currentShields);
		int currentHitPoints = (int)entity.getUserData(EntityConstants.hitPoints);
		assertEquals(0, currentHitPoints);

		ArgumentCaptor<EntityPropertyChangeEvent> captor = ArgumentCaptor.forClass(EntityPropertyChangeEvent.class);
		Mockito.verify(eventBus).fireEvent(captor.capture());

		EntityPropertyChangeEvent event = captor.getValue();
		assertNotNull(event);

		assertEquals(cause, event.getCause());
		assertEquals(entity, event.getEntity());
		assertEquals(1l, event.getEntityId());
		assertEquals(100, event.getOldPropertyValue());
		assertEquals(EntityConstants.hitPoints, event.getPropertyName());
		assertEquals(0, event.getPropertyValue());
	}

	@Test
	public void shouldRemoveWeaponFireFromSynStateAndBroadCast() {
		Spatial weaponFire = new Node();
		handler.removeWeaponFire(1l, weaponFire, new Vector3f());

		Mockito.verify(serverState).broadcast(Mockito.any(RemoveWeaponFireMessage.class));
		Mockito.verify(syncState).removeEntity(1l);
		Mockito.verify(entityAttacher).removeEntity(weaponFire);
	}

	@Test
	public void shouldNotHandleDamage() {
		Spatial weaponFire = new Node("weaponfire test node");

		WeaponFireControl weaponFireControl = Mockito.mock(WeaponFireControl.class);
		Mockito.when(weaponFireControl.getDirection()).thenReturn(new Vector3f());
		weaponFire.addControl(weaponFireControl);

		Spatial otherEntity = new Node("other entity test node");

		RigidBodyControl rigidBodyControl = Mockito.mock(RigidBodyControl.class);
		Mockito.when(rigidBodyControl.getPhysicsLocation()).thenReturn(new Vector3f());
		otherEntity.addControl(rigidBodyControl);

		float impulseAmount = 2.0f;

		handler.handleWeaponFire(1l, weaponFire, otherEntity, impulseAmount);

		Mockito.verify(handler, Mockito.never()).receiveDamage(Mockito.any(), Mockito.anyInt(), Mockito.any());
	}

	@Test
	public void shouldHandleDamage() {
		Spatial weaponFire = new Node("weaponfire test node");
		weaponFire.setUserData(EntityConstants.weaponFireDamage, 20);

		WeaponFireControl weaponFireControl = Mockito.mock(WeaponFireControl.class);
		Mockito.when(weaponFireControl.getDirection()).thenReturn(new Vector3f());
		weaponFire.addControl(weaponFireControl);

		Spatial otherEntity = new Node("other entity test node");
		otherEntity.setUserData(EntityConstants.maxHitPoints, 300);

		RigidBodyControl rigidBodyControl = Mockito.mock(RigidBodyControl.class);
		Mockito.when(rigidBodyControl.getPhysicsLocation()).thenReturn(new Vector3f());
		otherEntity.addControl(rigidBodyControl);

		float impulseAmount = 2.0f;

		handler.handleWeaponFire(1l, weaponFire, otherEntity, impulseAmount);

		Mockito.verify(handler).queueReceiveDamage(Mockito.any(), Mockito.anyInt(), Mockito.any());
	}
}