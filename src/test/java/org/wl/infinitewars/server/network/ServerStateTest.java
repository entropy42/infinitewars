package org.wl.infinitewars.server.network;

import java.io.IOException;

import org.junit.Before;
import org.mockito.Mockito;
import org.wl.infinitewars.AbstractTest;
import org.wl.infinitewars.server.ai.BotFactory;
import org.wl.infinitewars.server.gameplay.ServerPlayersContainer;
import org.wl.infinitewars.shared.gameplay.GameInfo;
import org.wl.infinitewars.shared.gameplay.GameInfoProvider;
import org.wl.infinitewars.shared.json.ServerConfig;
import org.wl.infinitewars.shared.loading.EnqueueHelper;

import com.jme3.network.Server;
import com.jme3.scene.Node;

public class ServerStateTest extends AbstractTest {

	private ServerState serverState;

	private ServerPlayersContainer playersContainer;
	private BotFactory botFactory;
	private EnqueueHelper enqueueHelper;
	private Node appRootNode;
	private Node appGuiNode;
	private Server server;

	@Before
	public void setup() throws IOException {
		ServerConfig config = new ServerConfig();
		config.setMaxPlayers(4);
		playersContainer = new ServerPlayersContainer(config);
		botFactory = Mockito.mock(BotFactory.class);
		enqueueHelper = Mockito.mock(EnqueueHelper.class);
		appRootNode = new Node("app root node");
		appGuiNode = new Node("app gui node");
		server = Mockito.mock(Server.class);

		ServerFactory serverFactory = new ServerFactory() {
			@Override
			public Server createServer() throws IOException {
				return server;
			}
		};
		GameInfo gameInfo = new GameInfo();
		GameInfoProvider gameInfoProvider = new GameInfoProvider();
		gameInfoProvider.setGameInfo(gameInfo);

		serverState = Mockito.spy(new ServerStateImpl(gameInfoProvider, playersContainer, botFactory, enqueueHelper, serverFactory, appRootNode, appGuiNode));
	}
}