package org.wl.infinitewars.client.network;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.wl.infinitewars.shared.entity.EntityConstants;
import org.wl.infinitewars.shared.network.messages.toserver.input.FireWeaponsMessage;

import com.jme3.math.Vector3f;
import com.jme3.network.AbstractMessage;
import com.jme3.network.NetworkClient;
import com.jme3.scene.Node;

public class ClientStateTest {

	private ClientState clientState;
	private ClientFactory clientFactory;
	private NetworkClient client;

	@Before
	public void setup() {
		client = Mockito.mock(NetworkClient.class);

		clientFactory = new ClientFactory() {
			@Override
			public NetworkClient createClient() {
				return client;
			}
		};

		clientState = Mockito.spy(new ClientStateImpl(clientFactory, new Node(), new Node()));
	}

	@Test
	public void shouldSendMessageViaClient() {
		AbstractMessage message = new FireWeaponsMessage(new Vector3f(), EntityConstants.standardWeaponSlots);
		clientState.sendMessage(message);

		Mockito.verify(client).send(message);
	}
}