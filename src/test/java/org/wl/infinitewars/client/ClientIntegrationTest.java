package org.wl.infinitewars.client;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.junit.Test;
import org.wl.infinitewars.server.network.restapi.GameInfoResponseJSON;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ClientIntegrationTest {

	@Test
	public void shouldIntegrateWithLocalServer() throws Exception {
		URL url = new URL("http://localhost:7000/api/game-info");
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Content-Type", "application/json");

		ObjectMapper mapper = new ObjectMapper();

		try (InputStreamReader in = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8); BufferedReader reader = new BufferedReader(in);) {
			String responseString = "";

			while (reader.ready()) {
				responseString += reader.readLine();
			}

			GameInfoResponseJSON gameInfo = mapper.readValue(responseString, GameInfoResponseJSON.class);
			assertNotNull(gameInfo);
		}
	}
}