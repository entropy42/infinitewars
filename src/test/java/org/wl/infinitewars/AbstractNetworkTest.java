package org.wl.infinitewars;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mockito;
import org.wl.infinitewars.server.gameplay.ServerPlayerFactory;
import org.wl.infinitewars.server.gameplay.ServerPlayersContainer;
import org.wl.infinitewars.server.network.ServerState;
import org.wl.infinitewars.shared.entity.EntityAttacher;
import org.wl.infinitewars.shared.entity.EntityFactory;
import org.wl.infinitewars.shared.gameplay.GameInfo;
import org.wl.infinitewars.shared.gameplay.TeamsContainer;
import org.wl.infinitewars.shared.json.Faction;
import org.wl.infinitewars.shared.json.FactionConfig;
import org.wl.infinitewars.shared.loading.EnqueueHelper;

import com.jme3.network.HostedConnection;

public class AbstractNetworkTest extends AbstractTest {

	protected GameInfo gameInfo;
	protected ServerPlayersContainer playerManager;
	protected ServerPlayerFactory playerFactory;
	protected EntityFactory entityFactory;
	protected ServerState serverState;
	protected TeamsContainer teamsContainer;
	protected EnqueueHelper enqueueHelper;
	protected EntityAttacher entityAttacher;
	protected Map<Faction, FactionConfig> factionConfigs;

	protected void setup() {
		gameInfo = new GameInfo();
		playerManager = Mockito.mock(ServerPlayersContainer.class);
		playerFactory = Mockito.mock(ServerPlayerFactory.class);
		entityFactory = Mockito.mock(EntityFactory.class);
		serverState = Mockito.mock(ServerState.class);
		teamsContainer = new TeamsContainer();
		enqueueHelper = Mockito.mock(EnqueueHelper.class);
		entityAttacher = Mockito.mock(EntityAttacher.class);
		factionConfigs = new HashMap<>(0);
	}

	protected HostedConnection getMockedHostedConnection() {
		return Mockito.mock(HostedConnection.class);
	}
}