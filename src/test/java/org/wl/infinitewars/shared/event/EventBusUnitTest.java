package org.wl.infinitewars.shared.event;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeEvent;
import org.wl.infinitewars.shared.event.gameplay.EntityPropertyChangeHandler;

public class EventBusUnitTest {

	private EventBus eventBus;

	@Before
	public void setup() {
		eventBus = Mockito.spy(new EventBus());
	}

	@Test
	public void shouldThrowIllegalArgumentExceptions() {
		try {
			eventBus.addHandler(null, null);
			fail("IllegalArgumentException expected because type is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("type must not be null!", e.getMessage());
		}

		try {
			eventBus.addHandler(EntityPropertyChangeEvent.TYPE, null);
			fail("IllegalArgumentException expected because handler is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("handler must not be null!", e.getMessage());
		}
		try {
			eventBus.performAdd(null, null);
			fail("IllegalArgumentException expected because type is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("type must not be null!", e.getMessage());
		}

		try {
			eventBus.performAdd(EntityPropertyChangeEvent.TYPE, null);
			fail("IllegalArgumentException expected because handler is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("handler must not be null!", e.getMessage());
		}

		try {
			eventBus.fireEvent(null);
			fail("IllegalArgumentException expected because event is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("event must not be null!", e.getMessage());
		}
	}

	@Test
	public void shouldCreateNewHandlerList() {
		EventType<EntityPropertyChangeHandler> type = EntityPropertyChangeEvent.TYPE;
		EntityPropertyChangeHandler handler = Mockito.mock(EntityPropertyChangeHandler.class);

		eventBus.performAdd(type, handler);

		assertEquals(1, eventBus.registeredHandlers.size());

		List<EventHandler> handlers = eventBus.registeredHandlers.get(type);
		assertNotNull(handlers);
		assertEquals(1, handlers.size());

		EventHandler foundHandler = handlers.get(0);
		assertEquals(handler, foundHandler);
	}

	@Test
	public void shouldUseExistingHandlerList() {
		EventType<EntityPropertyChangeHandler> type = EntityPropertyChangeEvent.TYPE;
		EntityPropertyChangeHandler handler1 = Mockito.mock(EntityPropertyChangeHandler.class);

		eventBus.performAdd(type, handler1);

		EntityPropertyChangeHandler handler2 = Mockito.mock(EntityPropertyChangeHandler.class);
		eventBus.performAdd(type, handler2);

		assertEquals(1, eventBus.registeredHandlers.size());

		List<EventHandler> handlers = eventBus.registeredHandlers.get(type);
		assertNotNull(handlers);
		assertEquals(2, handlers.size());

		EventHandler foundHandler = handlers.get(0);
		assertEquals(handler1, foundHandler);

		foundHandler = handlers.get(1);
		assertEquals(handler2, foundHandler);
	}

	@Test
	public void shouldClearEverything() {
		EventType<EntityPropertyChangeHandler> type = EntityPropertyChangeEvent.TYPE;
		EntityPropertyChangeHandler handler = Mockito.mock(EntityPropertyChangeHandler.class);
		eventBus.performAdd(type, handler);

		eventBus.firingEvent.set(true);

		eventBus.addHandler(type, handler);

		eventBus.clear();

		assertFalse(eventBus.firingEvent.get());
		assertEquals(0, eventBus.actions.size());
		assertEquals(0, eventBus.registeredHandlers.size());
	}

	@Test
	public void shouldAddHandlerDirectly() {
		EventType<EntityPropertyChangeHandler> type = EntityPropertyChangeEvent.TYPE;
		EntityPropertyChangeHandler handler = Mockito.mock(EntityPropertyChangeHandler.class);
		eventBus.addHandler(type, handler);

		Mockito.verify(eventBus).performAdd(type, handler);
		assertEquals(0, eventBus.actions.size());
		assertEquals(1, eventBus.registeredHandlers.size());

		List<EventHandler> handlers = eventBus.registeredHandlers.get(type);
		assertNotNull(handlers);
		assertEquals(1, handlers.size());

		EventHandler foundHandler = handlers.get(0);
		assertEquals(handler, foundHandler);
	}

	@Test
	public void shouldNotAddHandlerDirectly() {
		eventBus.firingEvent.set(true);

		EventType<EntityPropertyChangeHandler> type = EntityPropertyChangeEvent.TYPE;
		EntityPropertyChangeHandler handler = Mockito.mock(EntityPropertyChangeHandler.class);
		eventBus.addHandler(type, handler);

		Mockito.verify(eventBus, Mockito.never()).performAdd(Mockito.anyObject(), Mockito.anyObject());
		assertEquals(1, eventBus.actions.size());
		assertEquals(0, eventBus.registeredHandlers.size());

		List<EventHandler> handlers = eventBus.registeredHandlers.get(type);
		assertNull(handlers);

		EventBusAction action = eventBus.actions.get(0);
		action.perform();

		assertEquals(1, eventBus.registeredHandlers.size());
		handlers = eventBus.registeredHandlers.get(type);
		assertNotNull(handlers);

		EventHandler foundHandler = handlers.get(0);
		assertEquals(handler, foundHandler);
	}

	@Test
	public void shouldDoNothingBecauseNoHandlerRegistered() {
		EntityPropertyChangeEvent event = Mockito.spy(new EntityPropertyChangeEvent(null, 0l, null, null, null, null));
		eventBus.fireEvent(event);

		Mockito.verify(event, Mockito.never()).callHandler(Mockito.anyObject());
	}

	@Test
	public void shouldCallAllHandlers() {
		EventType<EntityPropertyChangeHandler> type = EntityPropertyChangeEvent.TYPE;
		EntityPropertyChangeEvent event = Mockito.spy(new EntityPropertyChangeEvent(null, 0l, null, null, null, null));
		EntityPropertyChangeHandler handler1 = Mockito.mock(EntityPropertyChangeHandler.class);
		EntityPropertyChangeHandler handler2 = Mockito.mock(EntityPropertyChangeHandler.class);

		eventBus.addHandler(type, handler1);
		eventBus.addHandler(type, handler2);

		eventBus.fireEvent(event);

		Mockito.verify(event, Mockito.times(2)).callHandler(Mockito.anyObject());
		Mockito.verify(event).callHandler(handler1);
		Mockito.verify(event).callHandler(handler2);
	}

	@Test
	public void shouldPerformAllActions() {
		EventBusAction action1 = Mockito.mock(EventBusAction.class);
		EventBusAction action2 = Mockito.mock(EventBusAction.class);
		eventBus.actions.add(action1);
		eventBus.actions.add(action2);

		eventBus.fireEvent(new EntityPropertyChangeEvent(null, 0l, null, null, null, null));

		Mockito.verify(action1).perform();
		Mockito.verify(action2).perform();

		assertEquals(0, eventBus.actions.size());
	}
}