package org.wl.infinitewars.shared.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.wl.infinitewars.shared.util.CacheHelper;
import org.wl.infinitewars.shared.util.FileConstants;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class JsonHelperUnitTest {

	private JsonHelper jsonHelper;
	private ObjectMapper mapper;
	private CacheHelper cacheHelper;
	private CacheManager cacheManager;
	private Cache jsonCache;

	@Before
	public void setup() {
		mapper = Mockito.mock(ObjectMapper.class);

		cacheManager = Mockito.spy(CacheManager.create("src/test/resources/ehcache/config.xml"));
		jsonCache = Mockito.spy(cacheManager.getCache("jsonCache"));
		cacheHelper = new CacheHelper(cacheManager, jsonCache);

		jsonHelper = new JsonHelper(cacheHelper, mapper);
	}

	@Test
	public void shouldUnmarshalAndPutIntoCache() throws Exception {
		FactionConfig object = new FactionConfig();
		Mockito.when(mapper.readValue(Mockito.any(InputStream.class), Mockito.eq(FactionConfig.class))).thenReturn(object);

		String fileName = FileConstants.FACTIONS_PATH_COMPLETE + "aliens" + FileConstants.FACTION_CONFIG_FILE;
		FactionConfig result = jsonHelper.createNewPojo(fileName, FactionConfig.class, false);
		assertEquals(object, result);

		ArgumentCaptor<Element> elementArg = ArgumentCaptor.forClass(Element.class);
		Mockito.verify(jsonCache).putIfAbsent(elementArg.capture());

		Element element = elementArg.getValue();
		assertNotNull(element);
		assertEquals(fileName, element.getObjectKey());
		assertEquals(object, element.getObjectValue());
	}

	@Test
	public void shouldUseCachedValue() throws Exception {
		Object object = new Object();
		Element element = new Element("testfilename", object);
		jsonCache.put(element);

		Object result = jsonHelper.toPOJO("testfilename", Object.class);
		assertEquals(object, result);
	}

	@Test
	public void shouldCreateNewValue() throws Exception {
		Object object = new Object();
		Element element = new Element("testfilename2", object);
		jsonCache.put(element);

		Object result = jsonHelper.toPOJO("testfilename3", Object.class);
		assertFalse(object == result);
	}
}