package org.wl.infinitewars.shared.gameplay.weapon;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.wl.infinitewars.shared.entity.EntityConstants;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class WeaponControlUnitTest {

	private WeaponControl control;
	private Spatial spatial;
	private Node parent;

	@Before
	public void setup() {
		control = new WeaponControl(EntityConstants.standard_weaponFireCoolDown);

		spatial = new Node("testnode");
		control.setSpatial(spatial);

		parent = new Node("parentnode");
		parent.attachChild(spatial);
		parent.setUserData(EntityConstants.standard_weaponFireCoolDown, 20f);
	}

	@Test
	public void shouldNotFireAfterFired() {
		control.update(20f);
		assertTrue(control.canFire());

		control.weaponFired();
		assertFalse(control.canFire());
	}

	@Test
	public void shouldFireOnlyWhenCooldownReached() {
		control.update(1f);
		assertFalse(control.canFire());

		control.update(18f);
		assertFalse(control.canFire());

		control.update(2f);
		assertTrue(control.canFire());
	}
}