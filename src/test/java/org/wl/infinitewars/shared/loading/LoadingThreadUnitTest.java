package org.wl.infinitewars.shared.loading;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.wl.infinitewars.shared.loading.EnqueueHelper;
import org.wl.infinitewars.shared.loading.LoadingState;
import org.wl.infinitewars.shared.loading.LoadingStep;
import org.wl.infinitewars.shared.loading.LoadingThread;

public class LoadingThreadUnitTest {
	
	private LoadingThread thread;
	private EnqueueHelper enqueueHelper;
	private LoadingState loadingState;
	private Future<Void> future;
	
	@Before
	@SuppressWarnings("unchecked")
	public void setup() {
		enqueueHelper = Mockito.mock(EnqueueHelper.class);
		loadingState = Mockito.mock(LoadingState.class);
		
		future = Mockito.mock(Future.class);
		Mockito.when(enqueueHelper.enqueue(Mockito.anyObject())).thenReturn(future);
	}
	
	private void init() {
		List<LoadingStep> steps = createDefaultStepList();
		init(steps);
	}
	
	private List<LoadingStep> createDefaultStepList() {
		List<LoadingStep> steps = new ArrayList<>(1);
		steps.add(Mockito.mock(LoadingStep.class));
		return steps;
	}
	
	private void init(List<LoadingStep> steps) {
		setup();
		
		thread = Mockito.spy(new LoadingThread(steps, enqueueHelper, loadingState));
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptions() {
		try {
			new LoadingThread(null, null, null);
			fail("IllegalArgumentException expected because steps is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("steps must not be null!", e.getMessage());
		}
		
		try {
			List<LoadingStep> steps = new ArrayList<>(0);
			new LoadingThread(steps, null, null);
			fail("IllegalArgumentException expected because steps is empty!");
		} catch (IllegalArgumentException e) {
			assertEquals("steps must not be empty!", e.getMessage());
		}
		
		try {
			new LoadingThread(createDefaultStepList(), null, null);
			fail("IllegalArgumentException expected because enqueueHelper is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("enqueueHelper must not be null!", e.getMessage());
		}
		
		try {
			new LoadingThread(createDefaultStepList(), enqueueHelper, null);
			fail("IllegalArgumentException expected because loadingState is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("loadingState must not be null!", e.getMessage());
		}
	}
	
	@Test
	public void shouldAddupLoadingTime() {
		List<LoadingStep> steps = new ArrayList<>(2);
		steps.add(new LoadingStep(1f, false) {
			@Override
			public void load() throws Exception {
				
			}
		});
		steps.add(new LoadingStep(2f, false) {
			@Override
			public void load() throws Exception {
				
			}
		});
		init(steps);
		
		assertEquals(3f, thread.totalTime, 0.0f);
	}
	
	@Test
	public void shouldCreateLoadingStateCallable() throws Exception {
		init();
		
		Callable<Void> callable = thread.createLoadingStateCallable(2f);
		assertNotNull(callable);
		
		callable.call();
		
		Mockito.verify(loadingState).updatePercentage(2f);
	}
	
	@Test
	public void shouldEnqueueLoadingStateCallable() throws InterruptedException, ExecutionException {
		init();
		
		thread.updateLoadingState(2f);
		
		Mockito.verify(enqueueHelper).enqueue(Mockito.anyObject());
		Mockito.verify(future).get();
	}
	
	@Test
	public void shouldCreateStepCallable() throws Exception {
		init();
		
		LoadingStep step = Mockito.mock(LoadingStep.class);
		Callable<Void> callable = thread.createStepCallable(step);
		assertNotNull(callable);
		
		callable.call();
		
		Mockito.verify(step).load();
	}
	
	@Test
	public void shouldEnqueueStepCallable() throws InterruptedException, ExecutionException {
		init();
		
		LoadingStep step = Mockito.mock(LoadingStep.class);
		thread.enqueueStep(step);
		
		Mockito.verify(enqueueHelper).enqueue(Mockito.anyObject());
		Mockito.verify(future).get();
	}
	
	@Test
	public void shouldEnqueueStep() throws Exception {
		init();
		
		LoadingStep step = Mockito.mock(LoadingStep.class);
		Mockito.when(step.isEnqueue()).thenReturn(true);
		
		thread.processStep(step);
		
		Mockito.verify(step).isEnqueue();
		Mockito.verify(thread).enqueueStep(step);
		Mockito.verify(step, Mockito.never()).load();
	}
	
	@Test
	public void shouldDirectlyLoadStep() throws Exception {
		init();
		
		LoadingStep step = Mockito.mock(LoadingStep.class);
		
		thread.processStep(step);
		
		Mockito.verify(step).isEnqueue();
		Mockito.verify(thread, Mockito.never()).enqueueStep(step);
		Mockito.verify(step).load();
	}
	
	@Test
	public void shouldCatchAllExceptions() throws Exception {
		List<LoadingStep> steps = new ArrayList<>(2);
		
		LoadingStep step1 = Mockito.spy(new LoadingStep(1f, false) {
			@Override
			public void load() throws Exception {
				throw new RuntimeException();
			}
		});
		steps.add(step1);
		
		LoadingStep step2 = Mockito.spy(new LoadingStep(1f, false) {
			@Override
			public void load() throws Exception {
				throw new RuntimeException();
			}
		});
		steps.add(step2);
		
		init(steps);
		
		thread.run();
		
		Mockito.verify(step1).isEnqueue();
		Mockito.verify(step1).load();
		Mockito.verify(step2, Mockito.never()).isEnqueue();
		Mockito.verify(step2, Mockito.never()).load();
	}
	
	@Test
	public void shouldLoadAllSteps() throws Exception {
		List<LoadingStep> steps = new ArrayList<>(2);
		
		LoadingStep step1 = Mockito.spy(new LoadingStep(1f, true) {
			@Override
			public void load() throws Exception {
				
			}
		});
		steps.add(step1);
		
		LoadingStep step2 = Mockito.spy(new LoadingStep(1f, false) {
			@Override
			public void load() throws Exception {
				
			}
		});
		steps.add(step2);
		
		init(steps);
		
		thread.run();
		
		Mockito.verify(step1).isEnqueue();
		Mockito.verify(thread).enqueueStep(step1);
		Mockito.verify(step1, Mockito.never()).load();
		Mockito.verify(step2).isEnqueue();
		Mockito.verify(step2).load();
		Mockito.verify(thread, Mockito.never()).enqueueStep(step2);
	}
}